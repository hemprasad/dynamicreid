load train_test_splits_ilidsvid.mat

for n = 1:size(ls_set,1)
    Partition(n).idx_train = [ls_set(n,1:150), ls_set(n,1:150) + 300];
    Partition(n).idx_test = [ls_set(n,151:end), ls_set(n,151:end) + 300];
    Partition(n).num_trainPerson = 150;
    Partition(n).ix_test_gallery = Partition(n).idx_test > 300; % use camB as gallery
end
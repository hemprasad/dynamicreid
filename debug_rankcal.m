function [bestID, worstID] = debug_rankcal(Method,gID,Partition)

best = 3;
worst = 0.2;
bestID = [];
worstID = [];
for p = 1:numel(Method)
    tmpR = Method{p}.Ranking;
    testID = gID(Partition(p).idx_test);
    for g = 1:size(tmpR,1)
        galID = testID(Partition(p).ix_test_gallery(g,:));
        tmpBest = galID(tmpR(g,:)<=best);
        bestID = [bestID, tmpBest];
        [~,ix] = sort(tmpR(g,:),'descend');
        tmpWorst = galID(ix(1:round(numel(ix)*worst)));
        worstID = [worstID, tmpWorst];
    end
end
bestID = unique(bestID);
worstID = unique(worstID);
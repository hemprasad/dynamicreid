%% visualize the dynamic word
centerMat = cell2mat(trainCenter);
label_centerS = reshape(repmat(1:num_strip,ncenter,1),1,[]);
DFcenter = pairwiseDynamicsDistance(centerMat);
% 6 main classes: R,G,B,Y,C,M
[label_centerC,~,~] = ncutW(1+DFcenter,6);
% generate color space
lowB = 0.2;
colorCluster = zeros(size(centerMat,2),3);
for l = 1:6
    num_withinC = sum(label_centerC(:,l));
    tmpColor = repmat([l/6 0 1],num_withinC,1);
    tmpColor(:,2) = linspace(lowB,1,num_withinC);
    tmpColor = hsv2rgb(tmpColor);
    colorCluster(logical(label_centerC(:,l)),:) = tmpColor;
end
dynMat = [];
for s = 1:num_strip
    tmpTrj = trjMat(label_trajID==1 & label_strip==s,:)';
    tmplabel = label_word(label_trajID==1 & label_strip==s);
    dynMat = dynMat + dynamicVisual(tmpTrj(2:end,:),tmplabel,...
        [128 64],colorCluster(label_centerS==s,:));
end
confmat = hankelconfusionMat(trjPool',label);

trackletShow(centerMat,label_centerS,colorCluster);
Q = cell2mat(denseTrj');
Qx = Q(:,end-1);
Qy = Q(:,end);
Qx = round(Qx);
Qy = round(Qy);
Qx(Qx==0) = 1;
Qy(Qy==0) = 1;
Qx(Qx==65) = 64;
Qy(Qy==129) = 128;
heatInd = sub2ind([128,64],Qy,Qx);
heatmap = zeros(128,64);
for h = 1:numel(heatInd)
    heatmap(heatInd(h)) = heatmap(heatInd(h)) + Ndist(h);
end
for h = 1:max(heatInd)
    if sum(heatInd==h)==0
        continue;
    end
    heatmap(h) = heatmap(h)/sum(heatInd==h);
end
figure,imagesc(heatmap)
axis equal
xlim([1,64])

%% 
% visualize the dynamic word
centerMat = cell2mat(trainCenter);
label_centerS = reshape(repmat(1:num_strip,ncenter,1),1,[]);
DFcenter = pairwiseDynamicsDistance(centerMat);
% 6 main classes: R,G,B,Y,C,M
[label_centerC,~,~] = ncutW(1+DFcenter,6);
% generate color space
lowB = 0.2;
colorCluster = zeros(size(centerMat,2),3);
for l = 1:6
    num_withinC = sum(label_centerC(:,l));
    tmpColor = repmat([l/6 0 1],num_withinC,1);
    tmpColor(:,2) = linspace(lowB,1,num_withinC);
    tmpColor = hsv2rgb(tmpColor);
    colorCluster(logical(label_centerC(:,l)),:) = tmpColor;
end
dynMat = zeros([32 16 3]);
dynCount = zeros(32,16);
for s = 1:num_strip
    tmpTrj = trjMat(label_trajID==i & label_strip==s,:)';
    tmplabel = label_word(label_trajID==i & label_strip==s);
    tmpMat = dynamicVisual(tmpTrj(2:end,tmpTrj(1,:)==min(tmpTrj(1,:))),tmplabel(tmpTrj(1,:)==min(tmpTrj(1,:))),...
        [128 64],colorCluster(label_centerS==s,:));
    tmpCount = tmpMat(:,:,1)>0|tmpMat(:,:,2)>0|tmpMat(:,:,3)>0;
    [confr,confc] = find(tmpCount&logical(dynCount));
    tmpMat(confr,confc,:) = 0;
    dynCount = tmpCount|logical(dynCount);
    dynMat = dynMat + tmpMat;
end
hf = figure(1);
imagesc(dynMat);
axis equal
xlim([0,size(dynMat,2)]);
saveas(hf,['Saved_fig\dynMapPatch_per' num2str(i) '.png']);
function f = wishartPdf(G,sigma,n)
p = size(G,1); % dimension
if p >= n 
    error('Wrong DOF!');
end
determ = det(G)^((n-p-1)/2)*exp(-0.5*trace(pinv(sigma)*G));
term_g = 1;
for j = 1:p
    term_g = term_g*gamma(0.5*(n-j+1));
end
denom = 2^(n*p/2)*pi^(p*(p-1)/4)*det(sigma)^(n/2)*term_g;

f = determ/denom;
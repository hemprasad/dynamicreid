%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Warning: This script will consume a whole lot of memory (around 3GB)
% because all the datapoints used for finding optimal binedges will be
% stored in memory. Please consider a reimplemantation if you do not have
% enough RAM.
%%
clear

%% Calibration on INRIA dataset
data.Paths = {'datasets'};
expr.Name  = 'INRIA'; % CAVIAR VIPeR iLIDS INRIA

Load_DataDirs;

%% Preparations
% 3 color features channels L*a*b*
datapoints = cell(3,1);
rgb2lab = makecform('srgb2lab');
NBins = [10 10 10];

%% Data accumulation
hh = waitbar(0,'Accumulating Data');
for i=1:length(data.Files)
    waitbar(i/length(data.Files),hh);
    %% color feature channels
    img_rgb = im2double(imread([data.Dir filesep data.Files(i).name]));
    img_lab = applycform(img_rgb,rgb2lab);
    for ch=1:3
        values = img_lab(:,:,ch);
        datapoints{ch} = [datapoints{ch}; values(:)];
    end
end

if ishandle(hh)
    close(hh);
end

%% Calculate bin edges based on the accumulated data
BinEdgesLab = cell(3,1);
for i=1:3
    BinEdgesLab{i} = util.findOptimalBining(datapoints{i},NBins(i));
end

save('MAT/_BINS_/binedges_lab.mat', 'BinEdgesLab');

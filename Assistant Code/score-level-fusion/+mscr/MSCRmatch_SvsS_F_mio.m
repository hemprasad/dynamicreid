function dist = MSCRmatch_SvsS_F(Blobs,gamma)
%% MSCR distances computation


hh = waitbar(0,'Retrieving MSCR features...');

reg=makecform('srgb2lab');

for i=1:length(Blobs)
    
    %%---- MSE analysis
        mser    =   Blobs(i); 
        A = permute(mser.pvec, [3,2,1]);
        C = applycform(A, reg);
        colour = permute(C,[3,2,1]);
                        
        dataf(i).Mmvec=mser.mvec;
        dataf(i).Mpvec=colour;  
        
        waitbar(i/length(Blobs),hh);
end
close(hh);


hh = waitbar(0,'Distances computation MSCR...');
for i = 1:length(dataf)
	
	Mmvec{1} = dataf(i).Mmvec(3,:);
	Mpvec{1} = dataf(i).Mpvec;
	
	num(1)=size(Mmvec{1},2);
	
	dist_y_n	= cell(length(dataf)*2,1);
	dist_color_n= cell(length(dataf)*2,1);
	
	for j = i:length(dataf)
		
		if i == j
			
			final_dist_y(i,j)		= 0;
			final_dist_color(i,j)	= 0;
		else
			Mmvec{2} = dataf(j).Mmvec(3,:);
			Mpvec{2} = dataf(j).Mpvec;
			
			num(2)= size(Mmvec{2},2); %length(Mindeces{2});
			[thrash,max_ind(i,j)]= max([num(1),num(2)]);
			max_i = mod(max_ind(i,j),2)+1;%max_ind;
			min_i = max_ind(i,j);%mod(max_ind,2)+1;
			max_info = num(max_i);
			min_info = num(min_i);
            
% 			tic
% 			dist_y = 1000*ones(min_info, max_info);
% 			dist_color = 1000*ones(min_info, max_info);
% 			% distanze tutti i blob di i con tutti i blob di j
% 			for k=1:max_info
% 				for h=1:min_info
% 					
% 					dist_y(h,k) = abs(Mmvec{max_i}(k)-Mmvec{min_i}(h));
% 					dist_color(h,k) = sqrt(sum((Mpvec{max_i}(:,k)-Mpvec{min_i}(:,h)).^2));
% 					
% 				end
%             end
% 			toc
            % compute the distances from every blob in i to every 
            % blob in j (faster)
            dist_y= abs( Mmvec{max_i}(ones(num(min_i),1),:)' - Mmvec{min_i}(ones(num(max_i),1),:) )';
            Sxx = sum(Mpvec{max_i}.^2,1);
            Syy = sum(Mpvec{min_i}.^2,1);
            Sxy = Mpvec{max_i}' * Mpvec{min_i};
            dist_color = sqrt( Sxx(ones(num(min_i),1),:)' + Syy(ones(num(max_i),1),:) - 2*Sxy)';
%             %%%CHECK DIFFERENCES
%             disp('errors')
% 			disp(max(max(abs(dist_y-dist_y_F))))
%             disp(max(max(abs(dist_color_f-dist_color))))
%             keyboard
            
            
			%% check outliers
			
			ref_y = min(dist_y); me_ref_y = mean(ref_y); std_ref_y = std(ref_y);
			ref_color = min(dist_color); me_ref_color = mean(ref_color); std_ref_color = std(ref_color);
			good = find((ref_y<(me_ref_y+3.5*std_ref_y))&(ref_color<(me_ref_color+3.5*std_ref_color)));
			
            if isempty(good)
                good = 1:max_info;
            end
            
			max_useful_info = length(good);
			dist_y2 = dist_y(:,good);
			dist_color2 = dist_color(:,good);
			
			%%normalize
			dist_y_n = dist_y./max(dist_y2(:));
			dist_color_n = dist_color./max(dist_color2(:));
			
			%%distance computation
			totdist_n = ( gamma*dist_y_n(:,good)  + (1-gamma)*dist_color_n(:,good) ) ;
			
			%%Minimization
			[min_dist,matching] = min(totdist_n);
			
			useful_i = sub2ind(size(totdist_n),[matching]',[1:max_useful_info]');
			final_dist_y(i,j)  = sum(dist_y2(useful_i))/max_useful_info;
			final_dist_color(i,j) = sum(dist_color2(useful_i))/max_useful_info;
		end
	end
	waitbar(i*j/length(dataf)^2/2,hh);
	clear dist_y dist_color	dist_color_n dist_y_n
	
end
final_dist_y=final_dist_y+final_dist_y';
final_dist_color=final_dist_color+final_dist_color';

close(hh);


for i=1:length(dataf)
	dist.MSCR_dist_y(i,:)       = final_dist_y(i,:)./max(final_dist_y(i,:));
	dist.MSCR_dist_color(i,:)   = final_dist_color(i,:)./max(final_dist_color(i,:));    
end
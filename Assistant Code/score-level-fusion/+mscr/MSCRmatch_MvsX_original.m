function SDALF_DISTs = MSCRmatch_MvsX(SDALF,SDALF_DISTs,parGMM,gamma,reg,H,W)
%% MSCR distances computation

BlobsDyn = SDALF.multiMSCR;

hh = waitbar(0,'Retrieving MSCR features...');
for i=1:length(BlobsDyn)
    %%---- MSE analysis
    mser    =   BlobsDyn(i);
    
    [unused,BB] = blobs_inside_image(mser.mvec,[H,W]); % used to compute BB
    keep_list = 1:size(mser.mvec,2);
    
    rep = ones(1,length(keep_list));
    centroid = squeeze(BB(1:2,1,:)) ./ [W*rep; H*rep];
    colour = applycform(mser.pvec',reg)';
    
    % blobs clustering  for dynamic feature!
    data = [centroid;colour];
    [cl(i).bestk,bestpp,cl(i).bestmu,cl(i).bestcov,dl,countf] = mixtures4(data,parGMM.kmin,min(parGMM.kmax,size(data,2)),parGMM.regularize,parGMM.th,parGMM.covoption);
    num_data = size(data,2);
    cl(i).label	 = zeros(num_data,1);
    ll		 = zeros(num_data,1);
    
    MU = cl(i).bestmu'; [numcl_i,dimcl_i] = size(MU);
    if parGMM.covoption == 1
        COV = zeros(numcl_i,dimcl_i);
        for kl = 1:numcl_i, COV(kl,:) = diag(cl(i).bestcov(:,:,kl)); end
        dd = prod(COV,2);
        iCOV = 1 ./ (COV + realmin);
        normconst = ((2*pi)^(-dimcl_i/2))*((dd).^(-0.5));
    else
        COV = cl(i).bestcov;
    end
    
    if parGMM.covoption == 1
        for j = 1:num_data
            vote = normconst .* exp(-0.5 * sum(iCOV .* (data(:,j*ones(1,numcl_i))'-MU).^2,2));
            [ll(j), cl(i).label(j)] = max(vote);
        end
    else
        for j = 1:num_data
            vote = mvnpdf(data(:,j*ones(1,numcl_i))',MU,COV);
            [ll(j), cl(i).label(j)] = max(vote);
        end
    end
    
    dataf(i).Mmvec=centroid;
    dataf(i).Mpvec=colour;
    
    waitbar(i/length(BlobsDyn),hh);
    
    clear centroid colour
    
end
close(hh);


%% Compute the distances between images
lenDATAF = length(dataf);
final_dist_y = 1000 - 1000*eye(lenDATAF); % init as 0 on diag and 1 offdiag
final_dist_color = 1000 - 1000*eye(lenDATAF);

blobPos = cell(2,1);
blobCol = cell(2,1);

% hh = waitbar(0,'Calculating MSCR distances...'); lenDATAF2 = lenDATAF^2;
for i = 1:2:lenDATAF
    
    blobPos{1} = dataf(i).Mmvec; % get position of blobs
    blobCol{1} = dataf(i).Mpvec; % get colors of blobs
    
    num1 = size(blobPos{1},2); % get number of blobs
    
    MU = cl(i).bestmu'; [numcl_i,dimcl_i] = size(MU);
    if parGMM.covoption == 1
        COV = zeros(numcl_i,dimcl_i);
        for kl = 1:numcl_i, COV(kl,:) = diag(cl(i).bestcov(:,:,kl)); end
        dd = prod(COV,2);
        iCOV = 1 ./ (COV + realmin);
        normconst = ((2*pi)^(-dimcl_i/2))*((dd).^(-0.5));
    else
        COV = cl(i).bestcov;
    end
    
    dist_y_n	= cell(lenDATAF*2,1);
    dist_color_n= cell(lenDATAF*2,1);
    
    for j = 2:2:lenDATAF
        blobPos{2} = dataf(j).Mmvec; % get position of blobs
        blobCol{2} = dataf(j).Mpvec; % get colors of blobs
        num2 = size(blobPos{2},2); % get number of blobs
        
        % compute the distances from every blob in i to every
        % blob in j
        dist_y = abs( blobPos{1}(2*ones(num2,1),:)' - blobPos{2}(2*ones(num1,1),:) );
        Sxx = sum(blobCol{1}.^2,1);
        Syy = sum(blobCol{2}.^2,1);
        Sxy = blobCol{1}' * blobCol{2};
        dist_color = sqrt( Sxx(ones(num2,1),:)' + Syy(ones(num1,1),:) - 2*Sxy);
        
        if numcl_i > 1
            % with multiple clusters, every blob must be compared
            % to every blob in the nearest cluster
            % concatenate positions and color of all blobs in j
            Xall = [blobPos{2}; blobCol{2}];
            Xmat = permute(Xall(:,:,ones(1,1,numcl_i)),[3 1 2]) - MU(:,:,ones(1,1,num2));
            
            % evaluate the Gaussian pdfs of all clusters
            if parGMM.covoption == 1
                vote = normconst(:,ones(1,num2)) .* ...
                    exp(-0.5 * squeeze(sum(iCOV(:,:,ones(1,1,num2)) .* Xmat.^2,2)));
            else
                for k = 1:num2
                    vote(:,k) = mvnpdf(Xmat(:,:,k),[],COV);
                end
            end
            [unused,NNind] = max(vote); % Nearest Neighbor data association
            
            % retrieve a mask that tells us which of the blob vs
            % blob cells in the distance matrix is a valid
            % association through the clustering
            matches = cell(numcl_i,1);
            blobs = cell(numcl_i,1);
            onevec = cell(numcl_i,1);
            for kl = 1:numcl_i
                matches{kl} = (cl(i).label == kl);
                blobs{kl} = find(matches{kl});
                onevec{kl} = ones(1,length(blobs{kl}));
            end
            matchmask = [matches{NNind}];
            
            % artificially raise the distance between blobs that
            % are not associated through clustering
            dist_y = dist_y .* matchmask + 1000 * (1-matchmask);
            dist_color = dist_color .* matchmask + 1000 * (1-matchmask);
        end
                
        % remove outliers: j blobs whose min distance from i blobs
        % is too high
        ref_y = min(dist_y); me_ref_y = mean(ref_y); std_ref_y = std(ref_y);
        ref_color = min(dist_color); me_ref_color = mean(ref_color); std_ref_color = std(ref_color);
        good = find((ref_y<(me_ref_y+3.5*std_ref_y))&(ref_color<(me_ref_color+3.5*std_ref_color)));
        
        max_useful_info = length(good);
        dist_y2 = dist_y(:,good);
        dist_color2 = dist_color(:,good);
        
        %%normalize
        dist_y_n{j} = dist_y./max(dist_y2(:));
        dist_color_n{j} = dist_color./max(dist_color2(:));
        
        %%distance computation
        totdist_n = ( gamma*dist_y_n{j}(:,good)  + (1-gamma)*dist_color_n{j}(:,good) ) ;
        
        %%Minimization
        [unused,matching] = min(totdist_n);
        
        useful_i = sub2ind(size(totdist_n),matching',(1:max_useful_info)');
        final_dist_y(i,j)  = sum(dist_y2(useful_i))/max_useful_info;
        final_dist_color(i,j) = sum(dist_color2(useful_i))/max_useful_info;
    end
%     waitbar(i*j/lenDATAF2,hh);
    clear dist_y dist_color dist_color_n dist_y_n;
    
end
% close(hh);

% normalize each row of final_dist_y and final_dist_color by their max,
% avoiding to account for the initial 1000 values for unused cells
maskused = zeros(lenDATAF,lenDATAF); maskused(1:2:lenDATAF,2:2:lenDATAF) = 1;
maxvec = max(final_dist_y .* maskused,[],2);
SDALF_DISTs.MSCR_dist_y = final_dist_y ./ maxvec(:,ones(1,lenDATAF));
SDALF_DISTs.MSCR_dist_y(SDALF_DISTs.MSCR_dist_y>1) = 1;
maxvec = max(final_dist_color .* maskused,[],2);
SDALF_DISTs.MSCR_dist_color = final_dist_color ./ maxvec(:,ones(1,lenDATAF));
SDALF_DISTs.MSCR_dist_color(SDALF_DISTs.MSCR_dist_color>1) = 1;

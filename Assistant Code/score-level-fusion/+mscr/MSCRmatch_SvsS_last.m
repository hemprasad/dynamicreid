function dist = MSCRmatch_SvsS_last(Blobs,gamma,normY,normC)
% Computation of MSCR distances
% normY = normalization value for the Y position, usually height of image
% normC = normalization value for distances in CIELAB, usually 250

numImg = length(Blobs);

% convert all colors into LABCIE
reg = makecform('srgb2lab');
dataf = struct('y', cell(1,numImg));
for i = 1:numImg
    C = applycform(Blobs(i).pvec', reg)';

    dataf(i).y = Blobs(i).mvec(3,:);
    dataf(i).C = C;
end


final_dist_y = zeros(numImg);
final_dist_color = zeros(numImg);

hh = waitbar(0,'Distances computation MSCR...');
for i = 1:(numImg-1)
	
    y_i = dataf(i).y;
    C_i = dataf(i).C;
    
    % artificio di Michele per evitare un solo blob?
    if length(y_i) == 1
        y_i = [y_i y_i];
        C_i = [C_i C_i];
    end
    num_i = size(y_i,2);

    for j = (i+1):numImg

        y_j = dataf(j).y;
        C_j = dataf(j).C;
        
        % artificio di Michele per evitare un solo blob?
        if length(y_j) == 1
            y_j = [y_j y_j];
            C_j = [C_j C_j];
        end
        num_j = size(y_j,2);
        
        max_info = max(num_i,num_j);

        % compute the distances from every blob in i to every
        % blob in j (faster)
        dist_y = abs( y_j(ones(num_i,1),:)' - y_i(ones(num_j,1),:) );
        Sxx = sum(C_j.^2,1);
        Syy = sum(C_i.^2,1);
        Sxy = C_j' * C_i;
        dist_color = sqrt( Sxx(ones(num_i,1),:)' + Syy(ones(num_j,1),:) - 2*Sxy);
        % dist matrices are in size num_j X num_i
        
        % transpose matrices, if needed, to get the biggest size as columns
        if num_j > num_i
            dist_y = dist_y';
            dist_color = dist_color';
        end
        
        %% check outliers
        % reject columns that violate X25 rule 
        ref_y = min(dist_y); me_ref_y = mean(ref_y); std_ref_y = std(ref_y);
        ref_color = min(dist_color); me_ref_color = mean(ref_color); std_ref_color = std(ref_color);
        good = find((ref_y<(me_ref_y+3.5*std_ref_y))&(ref_color<(me_ref_color+3.5*std_ref_color)));
        if isempty(good)
            good = 1:max_info;
        end

        % select only the good blobs' distances, and normalize
        max_useful_info = length(good);
        dist_y2 = dist_y(:,good) / normY;
        dist_color2 = dist_color(:,good) / normC;
        dist_y2(dist_y2 > 1) = 1;
        dist_color2(dist_color2 > 1) = 1;

        % calculate the combined distance y-color
        totdist_n = gamma * dist_y2 + (1-gamma) * dist_color2;

        % find the closest blob matches for each column-blob
        [min_dist,matching] = min(totdist_n);
        
        % find out the linear indices for those matches
        useful_i = sub2ind(size(totdist_n),matching',(1:max_useful_info)');
        
        % final y and C distances are averages of the values that brought
        % best matches
        final_dist_y(i,j)  = sum(dist_y2(useful_i))/max_useful_info;
        final_dist_color(i,j) = sum(dist_color2(useful_i))/max_useful_info;

        % distance is symmetric
        final_dist_y(j,i)  = final_dist_y(i,j);
        final_dist_color(j,i) = final_dist_color(i,j);
    end
    waitbar(i/(numImg-1),hh);
    clear dist_y dist_color	dist_color_n dist_y_n

end
close(hh);

% in this version, distances are already normalized
dist.MSCR_dist_y = final_dist_y;
dist.MSCR_dist_color = final_dist_color;


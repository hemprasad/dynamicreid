function [Dir,Files] = loadDataDirs(Paths,ExprName)
%%LOAD_DATADIRS Funtion run by all experiments scripts, to setup dir names and dir
% listings based on the dataset name

%     Width = data.Width;
switch ExprName
    case 'VIPeR'
        [Dir,Files] = util.list_files(Paths,'VIPeRa','bmp');
        
    case 'iLIDS'
        [Dir,Files] = util.list_files(Paths,'i-LIDS_Pedestrian/Persons','jpg');
        
    case 'ETHZ1'
        [Dir,Files] = util.list_files(Paths,'ETHZa/seq1','png');
        
    case 'ETHZ2'
        [Dir,Files] = util.list_files(Paths,'ETHZa/seq2','png');
        
    case 'ETHZ3'
        [Dir,Files] = util.list_files(Paths,'ETHZa/seq3','png');
        
    case 'CAVIAR'
        [Dir,Files] = util.list_files(Paths,'CAVIAR4REID','jpg');
        
    case 'iLIDSMA'
        [Dir,Files] = util.list_files(Paths,'iLIDS-MA','jpg');
        
    case 'INRIA'
        [Dir,Files] = util.list_files(Paths,'INRIA','png');
        
    otherwise
        error('Cannot find images: Unknown dataset name');
        
end

% check post-conditions
if isempty(Files)
    error('No source dataset files for %s',expr.Name);
end

end

function [wgts, fusedTable] = NoGreedyfusion(Tables_Trans, Mask, Type, sortTables)
%NOGREEDYFUSION pairwise fusion scheme using method specified by Type
% argument (either 'greedy', 'forward', 'backward' or 'tree'):
% 'greedy':   for each iteration fuse pair of features with smallest
%             overlap
% 'forward':  sort all features by overlap, begin with fusion of features
%             smallest overlap (fuse best features first)
% 'backward': sort all features by overlap, begin with fusion of features
%             largest overlap (fuse best features last)
% 'tree':     use tree-based fusion sheme PROPER as proposed in our paper,
%             which is: fuse best with 2nd-best feature, 3rd with 4th, and
%             so on, then contionue with fusion of pairwise fused features.
%             Repeat until you have a single fused score-table
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 if nargin < 4
     sortTables = 0;
 end

 N = length(Tables_Trans);
 Tables = cell(1, N);
 gen = cell(1, N);
 imp = cell(1, N);
 genunsorted = cell(1, N);
 impunsorted = cell(1, N);
 
 %get genuine -, imposter-Scores and single overlaps
  overlap = ones(1, N);
  for i = 1 : N
      Tables{i} = Tables_Trans{i};
      matching = Tables{i};
      n = size(matching, 1);
      maskLearn = Mask - eye(n); % mask without main diagonal for genuine score
      genunsorted{i} = matching(maskLearn(:) == 1)';
      impunsorted{i} = matching( not(Mask(:)) )';
      gen{i} = genunsorted{i};
      imp{i} = impunsorted{i};
      overlap(i) = fusion.areaPDFOverlapAnalytically(gen{i}, imp{i});
  end
 
  % sort 
  if sortTables
      gensorted = cell(1, N);
      impsorted = cell(1, N);
      [~, idx] = sort(overlap);
      for i = 1 : N
          gensorted{i} = genunsorted{idx(i)};
          impsorted{i} = impunsorted{idx(i)};
          gen{i} = gensorted{i};
          imp{i} = impsorted{i};
          Tables{i} = Tables_Trans{idx(i)};
      end
  end
   
  switch Type
     case 'greedy'
        unusedfeatures = ones(1,N);
        bestfeat = 0;

        % 1st loop: get best feature
        minoverlapGreedy(1) = 100;
        for i = 1 : length(Tables)
            if unusedfeatures(i)
                if overlap(i) < minoverlapGreedy(1)
                    minoverlapGreedy(1) = overlap(i);
                    bestfeat(1) = i;
                end
            end
        end
        unusedfeatures(bestfeat(1)) = 0;
        wgts = zeros(1,N);
        wgts(bestfeat(1)) = 1;
        genBest = gen{bestfeat(1)}; 
        impBest = imp{bestfeat(1)};
        fusedTable = Tables{bestfeat(1)};

        %next loops for Greedy
        j = 2;
        minoverlapimprovement = 2 * eps;
        while minoverlapimprovement > eps 
            minoverlapGreedy(j) = minoverlapGreedy(j-1);
            flag = 0;
            for i = 1 : N 
                if unusedfeatures(i)
                    [wgtstmp, overlaptmp] = fusion.getOptimalWeights([genBest; gen{i}], [impBest; imp{i}]);
                    if overlaptmp < minoverlapGreedy(j)
                        minoverlapGreedy(j) = overlaptmp;
                        bestfeat(j) = i;
                        wgtsBest(:, j) = wgtstmp;
                        flag = 1;
                    end
                end
            end
    
            if flag == 0;
                disp('No improvement found!');
                break;
            end
    
            wgts(not(unusedfeatures)) = wgts(not(unusedfeatures)) .* wgtsBest(1, j);
            wgts(bestfeat(j)) = wgtsBest(2, j);
            unusedfeatures(bestfeat(j)) = 0;
            genBest = wgtsBest(1, j) * genBest + wgtsBest(2, j) * gen{beatfeat(j)};
            impBest = wgtsBest(1, j) * impBest + wgtsBest(2, j) * imp{beatfeat(j)};
            fusedTable = wgtsBest(1, j) * fusedTable + wgtsBest(2, j) * Tables{bestfeat(j)};
            minoverlapimprovement = abs(minoverlapGreedy(j) - minoverlapGreedy(j-1));
            j = j + 1;
        end
     case 'forward'
        wgts = ones(1,N);
        genFusF = gen{1}; 
        impFusF = imp{1};
        fusedTable = Tables{1};
        for i = 2 : N
            wgtstmp = fusion.getOptimalWeights([genFusF; gen{i}], [impFusF; imp{i}]);
            wgts(1 : i-1) = wgts(1 : i-1) * wgtstmp(1);
            wgts(i) = wgtstmp(2);
            genFusF = wgtstmp(1) * genFusF + wgtstmp(2) * gen{i};
            impFusF = wgtstmp(1) * impFusF + wgtstmp(2) * imp{i};
            fusedTable = wgtstmp(1) * fusedTable + wgtstmp(2) * Table{i};
        end        
     case 'backward'
        wgts = ones(1,N);
        genFusB = gen{N}; 
        impFusB = imp{N};
        fusedTable = Tables{N};
        for i = N-1 : (-1) : 1
            wgtstmp = fusion.getOptimalWeights([gen{i}; genFusB], [imp{i}; impFusB]);
            wgts(i+1 : N) = wgts(i+1 : N) * wgtstmp(2);
            wgts(i) = wgtstmp(1);
            genFusB = wgtstmp(2) * genFusB + wgtstmp(1) * gen{i};
            impFusB = wgtstmp(2) * impFusB + wgtstmp(1) * imp{i};
            fusedTable = wgtstmp(2) * fusedTable + wgtstmp(1) * Table{i};
        end
     case 'tree'
        [wgts, fusedTable] = fusion.treeFusion(Tables, Mask);
  end
 
  % sort back
  if sortTables
      for i = 1 : N
        wgtsSort(idx(i)) = wgts(i);
      end
      wgts = wgtsSort;
  end


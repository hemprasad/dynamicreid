function[Tables_Trans, genBest, impBest, bestfeat, minoverlap, paramBest] = ...
    Greedyfusion(Tables, Mask, Type, FusionType, eps)
%GREEDYFUSION perform pairwise fusion (see NoGreedyfusion.m with argument
% Type='greedy')
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get transformed Tables 
switch Type
    case 'MinMax'
        Tables_Trans = fusion.TransformationMinMax(Tables);     % quantiles possible
    case 'MuSigma'
        Tables_Trans = fusion.TransformationMuSigma(Tables);    % quantiles possible
    case 'Decimal'
        Tables_Trans = fusion.TransformationDecimal(Tables);    % quantiles possible
    case 'DoubleSigmoid'
        Tables_Trans = fusion.TransformationDoubleSigmoid(Tables, Mask);   
    case 'Tanh'
        Tables_Trans = fusion.TransformationTanh(Tables, Mask); % quantiles possible
    case 'LikelihoodRatio'
        Tables_Trans = fusion.TransformationLikelihoodRatio(Tables, Mask, 0, 0.8); % possible to evaluates best Quantiles -> needs much more time
    case 'FAR'
        Tables_Trans = fusion.TransformationFAR(Tables, Mask);
    case 'LogisticRegression'
        Tables_Trans = fusion.TransformationLogisticRegression(Tables, Mask);
    otherwise
        % open, error message?        
end

unusedfeatures = ones(1,length(Tables_Trans));
bestfeat = 0;

% 1st loop: get genuine-, imposter-Scores and best feature
minoverlap(1) = 100;
if FusionType == '*'
    expntsBest = ones(2, length(Tables_Trans));
elseif FusionType == '+'
    wgtsBest = ones(2, length(Tables_Trans));
end


for i = 1 : length(Tables_Trans)
    if unusedfeatures(i)
        matching = Tables_Trans{i};
        [m,n] = size(matching);
        maskLearn = Mask - eye(n); % mask without main diagonal for genuine score
        gen{i} = matching(maskLearn(:) == 1)';
        imp{i} = matching( not(Mask(:)) )';
        overlaptmp = fusion.areaPDFOverlap(gen{i}, imp{i}, 50);
        if overlaptmp < minoverlap(1)
            minoverlap(1) = overlaptmp;
            bestfeat(1) = i;
        end
    end
end

unusedfeatures(bestfeat(1)) = 0;
genBest = gen{bestfeat(1)}; 
impBest = imp{bestfeat(1)};
 
%next loops
j = 2;
minoverlapimprovement = 2 * eps;
while minoverlapimprovement > eps
    minoverlap(j) = minoverlap(j-1);
    flag = 0;
    for i = 1 : length(Tables) 
        if unusedfeatures(i)
            if FusionType == '*'
                [expntstmp, overlaptmp] = fusion.getOptimalExponents([genBest; gen{i}], [impBest; imp{i}]);
                if overlaptmp < minoverlap(j)
                    minoverlap(j) = overlaptmp;
                    bestfeat(j) = i;
                    expntsBest(:, j) = expntstmp;
                    flag = 1;
                end
            elseif FusionType == '+'
                [wgtstmp, overlaptmp] = fusion.getOptimalWeights([genBest; gen{i}], [impBest; imp{i}]);
                if overlaptmp < minoverlap(j)
                    minoverlap(j) = overlaptmp;
                    bestfeat(j) = i;
                    wgtsBest(:, j) = wgtstmp;
                    flag = 1;
                end
            end
        end
     end
    
    if flag == 0;
        disp('No improvement found!')
        break;
    end
           
    unusedfeatures(bestfeat(j)) = 0;
    if FusionType == '*'
        genBest = genBest .^(expntsBest(1, j)) .* gen{bestfeat(j)} .^(expntsBest(2, j));
        impBest = impBest .^(expntsBest(1, j)) .* imp{bestfeat(j)} .^(expntsBest(2, j));
    elseif FusionType == '+'
        genBest = wgtsBest(1, j) * genBest + wgtsBest(2, j) * gen{bestfeat(j)};
        impBest = wgtsBest(1, j) * impBest + wgtsBest(2, j) * imp{bestfeat(j)};
    end
    minoverlapimprovement = abs(minoverlap(j) - minoverlap(j-1));
    j = j + 1;
end

if FusionType == '*'
    paramBest = expntsBest;
elseif FusionType == '+'
    paramBest = wgtsBest;
end

    
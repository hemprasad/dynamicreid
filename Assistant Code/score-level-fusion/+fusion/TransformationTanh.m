function [transfTables, wlearnMean, wlearnStd] = TransformationTanh(learnTables, transTables,genuineMask,lowerQuantile,upperQuantile)
%TRANSFORMTANH Perform tanh-score normalization on each table.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran, M. Eisenbach                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin < 4
        lowerQuantile = 0;
        upperQuantile = 1;
    end

    % Check the input
    validateattributes(learnTables,{'cell'},{'nonempty'},mfilename,'learnTables',1);
    validateattributes(transTables,{'cell'},{'nonempty'},mfilename,'transTables',1);

    attributes = {'nonnegative','<',1};
    validateattributes(lowerQuantile,{'numeric'},attributes,mfilename,'lowerQuantile',2);

    attributes = {'nonzero','<=',1};
    validateattributes(upperQuantile,{'numeric'},attributes,mfilename,'upperQuantile',3);

    message1 = 'LearningTables and TransTables should have the same size.';
    assert(length(learnTables) == length(transTables), message1);
    
    message2 = ['The lower quantile boundary should be less than the upper'...
        ' quantile boundary.'];
    assert(lowerQuantile < upperQuantile,message2);


    % function
    nTables = length(learnTables);
    transfTables = cell(1,nTables);
    n = size(learnTables{1},1);
    realGenuineMask = logical(genuineMask-eye(n));
    realGenuineMask = realGenuineMask(1:n,1:n);
    for i=1:nTables
        % get one table of the stack
        learnTable = learnTables{i};
        learnTable = learnTable(:);

        % calculate the upper and lower boundary value
        lowerBound = quantile(learnTable,lowerQuantile); % equals min for 0
        upperBound = quantile(learnTable,upperQuantile); % equals max for 1
        
        % get the genuine scores that are within these boundaries
        realGenuines = learnTable((learnTable >= lowerBound) &...
                                  (learnTable <= upperBound) &...
                                   realGenuineMask(:));
        % clip negative values mainly caused due to numerical problems
        realGenuines(realGenuines < 0) = 0;
        
        % estimate parameters for Hampel-estimator
        m = median(realGenuines);
        distancesToMedian = abs(realGenuines-m);
        a = quantile(distancesToMedian,0.70);
        b = quantile(distancesToMedian,0.85);
        c = quantile(distancesToMedian,0.90);
        % calculate weigths
        weights = hampelestimator(realGenuines-m,a,b,c);
        wNorm = weights(:)/sum(weights(:));
        
        % calculate weighted mean and std
        correctionFactor = 1.3155963359;
        wlearnMean(i) = wNorm'*realGenuines;
        wlearnStd(i) = sqrt(wNorm'*(realGenuines - wlearnMean(i)).^2) * correctionFactor;

        % transform the table with the calculated parameters
        transfTables{i} = 0.5 * (tanh(0.01*(transTables{i} - wlearnMean(i)) / wlearnStd(i))+1);
    end

end

function w = hampelestimator(medianCentX,a,b,c)
    absMedianCentX = abs(medianCentX);
    mask1 = and((0 <= absMedianCentX),(absMedianCentX <= a));
    mask2 = and((a <  absMedianCentX),(absMedianCentX <= b));
    mask3 = and((b <  absMedianCentX),(absMedianCentX <= c));
%     mask4 = (c <= absX);

    % weights
    w = zeros(size(medianCentX));
    w(mask1) = 1;
    w(mask2) = a ./ absMedianCentX(mask2);
    w(mask3) = a .* ((c - absMedianCentX(mask3))./(c-b)) ./ absMedianCentX(mask3);

%     Y = zeros(size(X));
%     Y(mask1) = X(mask1);
%     Y(mask2) = a*sign(X(mask2));
%     Y(mask3) = a*sign(X(mask3)).*((c-abs(X(mask3)))/(c-b));
%     Y(mask4) = 0;
end

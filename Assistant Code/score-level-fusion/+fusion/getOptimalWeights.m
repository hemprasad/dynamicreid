function [wgtsBest, minOverlap] = getOptimalWeights(genuineScores, imposterScores, doPlot)
%GETOPTIMALWEIGHTS PROPER weighting for pair of score-vectors
% genuineScores(1, :) ... genuine scores from matching feature 1
% genuineScores(2, :) ... genuine scores from matching feature 2
% imposterScores(1, :) ... impostor scores from matching feature 1
% imposterScores(2, :) ... impostor scores from matching feature 2
% doPlot ... if TRUE: plot debug output, if FALSE: do not
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3
    doPlot = 0;
end

nSupportingPoints = 80;

leftBorder = 0;
rightBorder = (pi/2);

nSteps = 15;

step = (rightBorder - leftBorder) / nSteps;

if doPlot == 1
    pre = [];
    post = [];
end

while step > 0.000001
    ref = cos(leftBorder:step:rightBorder);
    wgt = [ref; sin(leftBorder:step:rightBorder)];
    wgt = wgt ./ [sum(wgt); sum(wgt)];
    minOverlap = 2; % make minOverlap dependent on input data?
    bestPos = 0;
    
    if doPlot == 1
        areaPlot = [];
    end
    
    for i = 1 : length(wgt)
        genFus = wgt(1, i) * genuineScores(1, :) + wgt(2, i) * genuineScores(2, :);
        impFus = wgt(1, i) * imposterScores(1, :) + wgt(2, i) * imposterScores(2, :);
        area = fusion.areaPDFOverlapAnalytically(genFus, impFus);
        
        if doPlot == 1
            areaPlot = [areaPlot area];
        end
        
        if area < minOverlap
            minOverlap = area;
            bestPos = i;
        end
    end
    if bestPos <= 0
        bestPos = 1;
    end
    if bestPos > length(wgt)
        bestPos = length(wgt);
    end
    wgtsBest = wgt(:, bestPos); 
    
    if doPlot == 1
        pre = [pre [wgt(1, 1:(max(1,bestPos - 1))); areaPlot(1:(max(1,bestPos - 1)))]];
        post = [[wgt(1, (min(bestPos + 1,length(ref))):length(wgt)); areaPlot((min(bestPos + 1,length(ref))):length(wgt))] post];
    end
    
    leftBorder = acos(ref(max(1,bestPos - 1)));
    rightBorder = acos(ref(min(bestPos + 1,length(ref))));
    step = (rightBorder - leftBorder) / nSteps;
    
    nSupportingPoints = nSupportingPoints * nSteps / 2;
    
    if doPlot == 1
        figure
        plot(wgt(1,:), areaPlot, 'g-');
        hold on
        plot(wgt(1,:), areaPlot, 'g.');
        hold off
    end
end

if doPlot == 1
    ges = [pre [wgt(1, :); areaPlot] post];

    figure
    plot(wgt(1,:), areaPlot, 'b-');
    hold on
    plot(wgt(1,:), areaPlot, 'b.');
    hold off

    figure
    plot(ges(1,:), ges(2,:), 'r-');
    hold on
    plot(ges(1,:), ges(2,:), 'r.');
    hold off
end

end
function[AUCBest, fBest] = maximizeAUCprop(Tables, index1, index2, eps)
% Maximize Area under ROC Curve for probability tables
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

AUC = 0;
ftmp = 0;
expo = 0;
AUCBest = AUC+10;
fBest = 0;
TransTables{index1} = ones(size(Tables{index1})) - Tables{index1};
TransTables{index2} = ones(size(Tables{index2})) - Tables{index2};

while abs(AUC-AUCBest)>eps
    AUCBest = AUC;
    for f = max(0,ftmp - 10^(-expo+1)) : 10^(-expo) : ftmp + 10^(-expo+1)
        [cmc, tAUC, ER] = util.benchmarking('datasets/i-LIDS_Pedestrian/Persons',100,50,f * TransTables{index1} .* TransTables{index2},'.jpg',0);
        if tAUC > AUC
            fBest = f;
            AUC = tAUC;
        end
    end
    ftmp = fBest;
    expo = expo+1;
end;

AUCBest = AUC;
 
% plot CMC
figure
hold on
util.benchmarking('datasets/i-LIDS_Pedestrian/Persons',100,50,TransTables{index1},'.jpg',1);
util.benchmarking('datasets/i-LIDS_Pedestrian/Persons',100,50,TransTables{index2},'.jpg',2);
util.benchmarking('datasets/i-LIDS_Pedestrian/Persons',100,50,fBest * TransTables{index1} .* TransTables{index2},'.jpg',3);
function [cmc, AUC, ER, weights, fusedMatchingTables] =...
    weightcomputation(tables, mask, parameters, partitionTest, weightmethod, datasetPath, datasetFileSuffix, type, nRuns, plotColor)
%WEIGHTCOMPUTATION compute weights with method specified by weightmethod
% argument
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fusedMatchingTables = cell(nRuns,1);
weights = cell(nRuns,1);
nTables = length(tables);

for run = 1 : nRuns
%     disp(num2str(run));
    for t = 1 : nTables
        matchingTable = tables{t};
        matchingTrain = matchingTable(not(partitionTest(run, :)), not(partitionTest(run, :)));
        matchingTest = matchingTable(partitionTest(run, :), partitionTest(run, :));
        learnTables{t} = matchingTrain;
        transTables{t} = matchingTest;
    end
    n = size(learnTables{1},1);
    learnMask = mask(not(partitionTest(run, :)), not(partitionTest(run, :)));
    maskGenuine = learnMask - eye(n);
    
    % get some characteristics
    gen = zeros(nTables,sum(maskGenuine(:) == 1));
    imp = zeros(nTables,sum(not(learnMask(:))));
    AUCsingle = zeros(1,nTables);
    EER = zeros(1, nTables);
    for i = 1 : nTables
    	[tmpcmc, tmpAUC, ~] = util.benchmarking2(datasetPath, not(partitionTest(run,:)), learnTables(i:i), datasetFileSuffix, 0); % not(part...) because learnTables
        cmcsingle(i,:) = tmpcmc;
        AUCsingle(i) = tmpAUC;
        learnTable = learnTables{i};
        learnTable = learnTable(:);
        gen(i,:) = learnTable(maskGenuine(:) == 1)';
        imp(i,:) = learnTable( not(learnMask(:)) )';
        EER(i) = fusion.getEER(gen(i,:), imp(i,:));
    end
    % get weights
    switch weightmethod
        case 'EqualWeighted'
            wgts = ones(1,nTables);
        case 'perf_AUC'
            Max = max(AUCsingle);
            Min = min(AUCsingle);
            wgts = 1 / (Max - Min) * (AUCsingle - Min);
        case 'perf_rang1'
            Max = max(cmcsingle(:,1));
            Min = min(cmcsingle(:,1));
            wgts = 1 / (Max - Min) * (cmcsingle(:,1)' - Min);
        case 'perf_rang10'
            Max = max(cmcsingle(:,10));
            Min = min(cmcsingle(:,10));
            wgts = 1 / (Max - Min) * (cmcsingle(:,10)' - Min);
        case 'perf_EER'
            wgts = 1 ./ EER;
        case 'D-Prime'
            for i = 1 : nTables
                wgts(i) = (mean(imp(i,:))-mean(gen(i,:))) ./ sqrt( std(imp(i,:))^2 + std(gen(i,:))^2);
            end
        case 'NCW'
            for i = 1 : nTables
                wgts(i) = 1 ./ (quantile(gen(i,:), 0.99) - quantile(imp(i,:), 0.01));
            end
    end
    wgts = wgts / sum(wgts);
    
    % fuse
    FusedTable = 0;
    parameter = parameters{run};
    for i = 1 : nTables
        switch type
            case 'Decimal'
                Tables_Trans{i} = log10(1+transTables{i}) / parameter(i); %paramter = normfactor
            case 'DoubleSigmoid'
                tau = parameter(1,:);
                alpha1 = parameter(2,:);
                alpha2 = parameter(3,:);
                matchingTrans = zeros(size(transTables{i}));
                transTable = transTables{i};
                isSmaller = transTable < tau(i);
                matchingTrans(isSmaller) = 1 ./ (1 + exp(-2 .* (transTable(isSmaller) - tau(i)) ./ alpha1(i)));
                matchingTrans(not(isSmaller)) = 1 ./ (1 + exp(-2 .* (transTable(not(isSmaller)) - tau(i)) ./ alpha2(i)));
                Tables_Trans{i} = matchingTrans;
            case {'LikelihoodRatio','FAR'}
                n = size(transTables, 1);
                nSupportingPoints = size(parameter{i}, 2); %parameter = Lookuptable
                LookupTable = parameter{i};
                L = zeros(n); U = nSupportingPoints * ones(n); index = zeros(n);
                transTable = transTables{i};
                tooSmall = transTable <= LookupTable(1,1);
                tooBig = transTable >= LookupTable(1,nSupportingPoints);

                maskRight = not(tooSmall) & not(tooBig);
                range = (max(LookupTable(1,:)) - min(LookupTable(1,:))) / (nSupportingPoints - 1);
                index(maskRight) = (ceil(transTable(maskRight) ./ range) * range - min(LookupTable(1,:))) / range + 1;
                U(maskRight) = max(2,min(nSupportingPoints,round(index(maskRight))));   % upper index
                L(maskRight) = U(maskRight) - 1;                                        % lower index 

                matchingTrans = transTable;
                matchingTrans(tooSmall) = LookupTable(2,1);
                matchingTrans(tooBig) = LookupTable(2, nSupportingPoints);    
                slope = (LookupTable(2,U(maskRight)) - LookupTable(2,L(maskRight))) ./ (LookupTable(1,U(maskRight)) - LookupTable(1,L(maskRight)));
                matchingTrans(maskRight) = slope .* (transTable(maskRight)' - LookupTable(1,L(maskRight))) + LookupTable(2,L(maskRight));

                Tables_Trans{i} = matchingTrans;
            case 'LogisticRegression'
                a = parameter(1,:);
                b = parameter(2,:);
                matchingRatio = exp(a(i) * transTables{i} + b(i));
                Tables_Trans{i} = 1 - matchingRatio ./ (matchingRatio + 1);
            case 'MinMax'
                Min = parameter(1,:);
                Max = parameter(2,:);
                Tables_Trans{i} = (transTables{i} - Min(i)) / (Max(i) - Min(i));
            case 'MuSigma'
                Mu = parameter(1,:);
                Sigma = parameter(2,:);
                Tables_Trans{i} = (transTables{i} - Mu(i)) / Sigma(i);
            case 'Tanh'
                parameter = reshape(parameter,[2 nTables]);
                wMu = parameter(1,:);
                wSigma = parameter(2,:);
                Tables_Trans{i} = 0.5 * (tanh(0.01 * (transTables{i} - wMu(i)) / wSigma(i)) + 1);
        end
        FusedTable = FusedTable + wgts(i) * Tables_Trans{i};
    end
    
    weights{run} = wgts;
    fusedMatchingTables{run} = FusedTable;
end

[cmc, AUC, ER] = util.benchmarking2(datasetPath, partitionTest(1:nRuns,:), fusedMatchingTables, datasetFileSuffix, plotColor);
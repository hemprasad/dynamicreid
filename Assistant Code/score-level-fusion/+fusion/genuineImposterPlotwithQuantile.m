function[t, pIsGen, polynomial5] = genuineImposterPlotwithQuantile(genuineScores, imposterScores, minValue, maxValue, nSupportingPoints, lowerquantG, upperquantG, upperquantI, doPlot)
%GENUINEIMPOSTERPLOTWITHQUANTILE estimate pGen, pImp, pIsGen with lower and upper quantile
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 8
    doPlot = 0;
end

t = linspace(minValue, maxValue, nSupportingPoints);
[d, N] = size(genuineScores);
sigma = std(genuineScores);
h = sigma * (4 / (d + 2))^(1 / (d + 4)) * N^(-1 / (d + 4)); % kernel width
hGenuine = ones(1, N) * h;
% left extreme value handling
lowerquantG = [0, lowerquantG];
fac = 2 .^ ((1:length(lowerquantG)) - 1);
for i = 1 : (length(lowerquantG) - 1)
    try
        leftBorder = quantile(genuineScores, max(0, lowerquantG(i)));
        rightBorder = quantile(genuineScores, max(0, lowerquantG(i+1)));
    catch err
        disp('error 1')
        d
        N
        i
        lowerquantG
        rethrow(err)
    end
    if leftBorder ~= rightBorder
        mask = and((genuineScores >= leftBorder), (genuineScores < rightBorder));
        if sum(mask) >= 2
            idx1 = length(lowerquantG) - i;
            idx2 = idx1 + 1;
            fac1 = fac(idx1);
            fac2 = fac(idx2);
            lowerFactor = (fac2 - fac1) / (rightBorder - leftBorder)^2;
            hGenuine(mask) = hGenuine(mask) .* (lowerFactor .* (genuineScores(mask) - rightBorder).^2 + idx1);
        end
    end
end
% right extreme value handling
upperquantG = [upperquantG, 1];
fac = 2 .^ ((1:length(upperquantG)) - 1);
for i = 1 : (length(upperquantG) - 1)
    try
        leftBorder = quantile(genuineScores, min(upperquantG(i), 1));
        rightBorder = quantile(genuineScores, min(upperquantG(i+1), 1));
    catch err
        disp('error 2')
        d
        N
        i
        upperquantG
        rethrow(err)
    end
    if leftBorder ~= rightBorder
        mask = and((genuineScores > leftBorder), (genuineScores <= rightBorder));
        if sum(mask) >= 2
            idx1 = i;
            idx2 = idx1 + 1;
            fac1 = fac(idx1);
            fac2 = fac(idx2);
            upperFactor = (fac2 - fac1) / (rightBorder - leftBorder)^2;
            hGenuine(mask) = hGenuine(mask) .* (upperFactor .* (genuineScores(mask) - leftBorder).^2 + idx1);
        end
    end
end
for i = 1:nSupportingPoints
    k = exp(-0.5 .* ((genuineScores - t(i))./hGenuine).^2) ./ hGenuine;
    p = (1 / N) * (1 / sqrt(2 * pi)) * sum(k');
    pGen(i) = p;
end
[d, N] = size(imposterScores);
sigma = std(imposterScores);
h = sigma * (4 / (d + 2))^(1 / (d + 4)) * N^(-1 / (d + 4)); % kernel width
hImposter = ones(1, N) * h;
% right extreme value handling
upperquantI = [upperquantI, 1];
fac = 2 .^ ((1:length(upperquantI)) - 1);
for i = 1 : (length(upperquantI) - 1)
    try
        leftBorder = quantile(imposterScores, min(upperquantI(i), 1));
        rightBorder = quantile(imposterScores, min(upperquantI(i+1), 1));
    catch err
        disp('error 3')
        d
        N
        i
        upperquantI
        rethrow(err)
    end
    if leftBorder ~= rightBorder
        mask = and((imposterScores > leftBorder), (imposterScores <= rightBorder));
        if sum(mask) >= 2
            idx1 = i;
            idx2 = idx1 + 1;
            fac1 = fac(idx1);
            fac2 = fac(idx2);
            upperFactor = (fac2 - fac1) / (rightBorder - leftBorder)^2;
            hImposter(mask) = hImposter(mask) .* (upperFactor .* (imposterScores(mask) - leftBorder).^2 + idx1);
        end
    end
end
for i = 1:nSupportingPoints
	k = exp(-0.5 * ((imposterScores - t(i))./hImposter).^2) ./ hImposter;
	p = (1 / N) * (1 / sqrt(2 * pi)) * sum(k');
	pImp(i) = p;
end
pIsGen = zeros(1, nSupportingPoints);
for i = 1:nSupportingPoints
    sumP = pGen(i) + pImp(i);
    if sumP > 0
        pIsGen(i) = pGen(i) / sumP;
    end
end
polynomial5 = polyfit(t, pIsGen, 9);
if doPlot
    %curve5 = polynomial5(1) * t.^5 + polynomial5(2) * t.^4 + polynomial5(3) * t.^3 + polynomial5(4) * t.^2 + polynomial5(5) * t + polynomial5(6);
    curve5 = polyval(polynomial5,t);
    mGI = max([pGen(:); pImp(:)]);
    figure
    for i = 1 : length(lowerquantG)
        lowerBoundgenuine = quantile(genuineScores, lowerquantG(i));
        plot([lowerBoundgenuine lowerBoundgenuine], [0 mGI], 'b:', 'lineWidth', 2)
        hold on
    end
    for i = 1 : length(upperquantG)
        upperBoundgenuine = quantile(genuineScores, upperquantG(i));
        plot([upperBoundgenuine upperBoundgenuine], [0 mGI], 'k:', 'lineWidth', 2)
    end
    for i = 1 : length(upperquantI)
        upperBoundimposter = quantile(imposterScores, upperquantI(i));
        plot([upperBoundimposter upperBoundimposter], [0 mGI], 'm:', 'lineWidth', 2)
    end
    plot(t, pImp, 'r-', 'lineWidth', 2)
    hold on
    plot(t, pGen, 'g-', 'lineWidth', 2)
    hold off
    figure
    plot(t, pIsGen, 'b-', 'lineWidth', 2)
    hold on
    plot(t, curve5, 'r:', 'lineWidth', 2)
    hold off
    axis([minValue maxValue 0 1]);
end
end
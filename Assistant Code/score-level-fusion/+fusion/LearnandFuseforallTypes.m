%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: J. Niebling                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% learn with iLIDS and transform VIPeR
learnTables = Tables_iLIDS;
transTables = Tables_VIPeR;
Mask = genuineMask_iLIDS;


try
    [Normfactor_iLIDS, wgtsDecimal_iLIDS, FusedTableDecimal_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'Decimal');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_Decimal', 'Normfactor_iLIDS', 'wgtsDecimal_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_Decimal', 'FusedTableDecimal_VIPeR');
catch err
    disp(err.message)
end
try
    [TauAlpha1Alpha2_iLIDS, wgtsDoubleSigmoid_iLIDS, FusedTableDoubleSigmoid_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'DoubleSigmoid');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_DoubleSigmoid', 'TauAlpha1Alpha2_iLIDS', 'wgtsDoubleSigmoid_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_DoubleSigmoid', 'FusedTableDoubleSigmoid_VIPeR');
catch err
    disp(err.message)
end
try
    [LookupTableFAR_iLIDS, wgtsFAR_iLIDS, FusedTableFAR_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'FAR');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_FAR', 'LookupTableFAR_iLIDS', 'wgtsFAR_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_FAR', 'FusedTableFAR_VIPeR');
catch err
    disp(err.message)
end
try
    [LookupTableLikelihoodRatio_iLIDS, wgtsLikelihoodRatio_iLIDS, FusedTableLikelihoodRatio_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'LikelihoodRatio');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_LikelihoodRatio', 'LookupTableLikelihoodRatio_iLIDS', 'wgtsLikelihoodRatio_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_LikelihoodRatio', 'FusedTableLikelihoodRatio_VIPeR');
catch err
    disp(err.message)
end
try
    [a_b_LogisticRegression_iLIDS, wgtsLogisticRegression_iLIDS, FusedTableLogisticRegression_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'LogisticRegression');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_LogisticRegression', 'a_b_LogisticRegression_iLIDS', 'wgtsLogisticRegression_iLIDS'),
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_LogisticRegression', 'FusedTableLogisticRegression_VIPeR');
catch err
    disp(err.message)
end
try
    [MinMax_iLIDS, wgtsMinMax_iLIDS, FusedTableMinMax_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'MinMax');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_MinMax', 'MinMax_iLIDS', 'wgtsMinMax_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_MinMax', 'FusedTableMinMax_VIPeR');
catch err
    disp(err.message)
end
try
    [MuSigma_iLIDS, wgtsMuSigma_iLIDS, FusedTableMuSigma_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'MuSigma');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_MuSigma', 'MuSigma_iLIDS', 'wgtsMuSigma_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_MuSigma', 'FusedTableMuSigma_VIPeR');
catch err
    disp(err.message)
end
try
    [wMuwSigmaTanh_iLIDS, wgtsTanh_iLIDS, FusedTableTanh_VIPeR] = fusion.LearningandFusion(learnTables, transTables, Mask, 'Tanh');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_Tanh', 'wMuSigmaTanh_iLIDS', 'wgtsTanh_iLIDS');
    save('../results/VIPeR_Exp001/transformedTables/FusionParameters_iLIDS_Tanh', 'FusedTableTanh_VIPeR');
catch err
    disp(err.message)
end

% learn with VIPeR and transform iLIDS
learnTables = Tables_VIPeR;
transTables = Tables_iLIDS;
Mask = genuineMask_VIPeR;

try
    [Normfactor_VIPeR, wgtsDecimal_VIPeR, FusedTableDecimal_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'Decimal');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_Decimal', 'Normfactor_VIPeR', 'wgtsDecimal_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_Decimal', 'FusedTableDecimal_iLIDS');
catch err
    disp(err.message)
end
try
    [TauAlpha1Alpha2_VIPeR, wgtsDoubleSigmoid_VIPeR, FusedTableDoubleSigmoid_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'DoubleSigmoid');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_DoubleSigmoid', 'TauAlpha1Alpha2_VIPeR', 'wgtsDoubleSigmoid_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_DoubleSigmoid', 'FusedTableDoubleSigmoid_iLIDS');
catch err
    disp(err.message)
end
try
    [LookupTableFAR_VIPeR, wgtsFAR_VIPeR, FusedTableFAR_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'FAR');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_FAR', 'LookupTableFAR_VIPeR', 'wgtsFAR_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_FAR', 'FusedTableFAR_iLIDS');
catch err
    disp(err.message)
end
try
    [LookupTableLikelihoodRatio_VIPeR, wgtsLikelihoodRatio_VIPeR, FusedTableLikelihoodRatio_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'LikelihoodRatio');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_LikelihoodRatio', 'LookupTableLikelihoodRatio_VIPeR', 'wgtsLikelihoodRatio_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_LikelihoodRatio', 'FusedTableLikelihoodRatio_iLIDS');
catch err
    disp(err.message)
end
try
    [a_b_LogisticRegression_VIPeR, wgtsLogisticRegression_VIPeR, FusedTableLogisticRegression_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'LogisticRegression');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_LogisticRegression', 'a_b_LogisticRegression_VIPeR', 'wgtsLogisticRegression_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_LogisticRegression', 'FusedTableLogisticRegression_iLIDS');
catch err
    disp(err.message)
end
try
    [MinMax_VIPeR, wgtsMinMax_VIPeR, FusedTableMinMax_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'MinMax');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_MinMax', 'MinMax_VIPeR', 'wgtsMinMax_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_MinMax', 'FusedTableMinMax_iLIDS');
catch err
    disp(err.message)
end
try
    [MuSigma_VIPeR, wgtsMuSigma_VIPeR, FusedTableMuSigma_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'MuSigma');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_MuSigma', 'MuSigma_VIPeR', 'wgtsMuSigma_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_MuSigma', 'FusedTableMuSigma_iLIDS');
catch err
    disp(err.message)
end
try
    [wMuwSigmaTanh_VIPeR, wgtsTanh_VIPeR, FusedTableTanh_iLIDS] = fusion.LearningandFusion(learnTables, transTables, Mask, 'Tanh');
    save('../results/VIPeR_Exp001/transformedTables/LearnVIPeR_Tanh', 'wMuSigmaTanh_VIPeR', 'wgtsTanh_VIPeR');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_VIPeR_Tanh', 'FusedTableTanh_iLIDS');
catch err
    disp(err.message)
end
toc

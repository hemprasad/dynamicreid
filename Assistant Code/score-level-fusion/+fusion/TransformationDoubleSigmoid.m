function[Tables_TransDoubleSigmoid, tau, alpha1, alpha2] = TransformationDoubleSigmoid(learnTables, transTables, Mask, alpha) 
% TRANSFORMATIONDOUBLESIGMOID Learn parmeters from learnTables and
% transform transTables using double sigmoid normalization
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: J. Niebling                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 4
    alpha = 0.05;
end

nTables = length(learnTables);
Tables_TransDoubleSigmoid = cell(1, nTables);
n = size(learnTables{1},1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
alpha1 = zeros(1,nTables);
alpha2 = zeros(1,nTables);
tau = zeros(1,nTables);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for j=1:nTables 

    learnTable = learnTables{j};
    learnTable = learnTable(:);
    maskGenuine = Mask - eye(n);
    gen = learnTable(maskGenuine(:) == 1)';
    imp = learnTable( not(Mask(:)) )';
        
    minValue = quantile([gen, imp], 0.01);
    maxValue = quantile([gen, imp], 0.99);  
    nSupportingPoints = 1000;

    t = linspace(minValue, maxValue, nSupportingPoints);
    pGen = fusion.KDE(gen, t);
    pImp = fusion.KDE(imp, t);

    pIsGen = zeros(1, nSupportingPoints);
    for i = 1:nSupportingPoints
        sumP = pGen(i) + pImp(i);
        if sumP > 0
            pIsGen(i) = pGen(i) / sumP;
        end
    end
    
   % get alpha1, tau, alpha 2 
   tau(j) = fusion.FindIntersectionpoint(gen, imp, 0.001);
   if pIsGen(1) < 1 - alpha
       alpha1(j) = minValue;
   else
       for i = 1:nSupportingPoints
           if pIsGen(i) >= 1 - alpha
               alpha1(j) = t(i+1);
           end
       end
   end

   if pIsGen(nSupportingPoints) > alpha
       alpha2(j) = maxValue;
   else
       for i = 1:nSupportingPoints
           if pIsGen(i) >= alpha
               alpha2(j) = t(i+1);
           end
       end
   end
   alpha1(j) = abs(tau(j) - alpha1(j)); % get real alpha1
   alpha2(j) = abs(tau(j) - alpha2(j)); % get real alpha2
   
            
    % transform
    transTable = transTables{j};
    isSmaller = transTable < tau(j);
    transTableDoubleSigmoid(isSmaller) = 1 ./ (1 + exp(-2 .* (transTable(isSmaller) - tau(j)) ./ alpha1(j)));
    transTableDoubleSigmoid(not(isSmaller)) = 1 ./ (1 + exp(-2 .* (transTable(not(isSmaller)) - tau(j)) ./ alpha2(j)));
   
    Tables_TransDoubleSigmoid{j} = reshape(transTableDoubleSigmoid, n, n);
end
function [lowerbestQuantile, upperbestQuantile, upperbestQuantileI, mindiff] = ...
        getQuantile(gen, imp, nSupportingPoints, eps, doPlot)
%GETQUANTILE gets best quantiles for genuineImposterPlot and best
% generates plots
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin < 5
        doPlot = 0;
    end

% gets best quantiles for genuineImposterPlot and best generates plots
  
    minValue = min([gen, imp]); 
    maxValue = quantile([gen, imp], 0.99);
    upperbestQuantile = 1;
    lowerbestQuantile = 0;
    
    % get upper quantile
    mindiffu = max([gen, imp]) * 10;
    mindiffBestu = mindiffu + 2 * eps;
    tQuantile = 0.5;
    range = 0.1;
    
    upQuant = [];
    
    maxGen = quantile(gen, 0.99);
    
    for q = 1 : 6
        if doPlot
            disp(['q = ', num2str(q)])
        end
        upQuant = [upQuant, (1 - tQuantile) / 2 + tQuantile];
        range = (1 - tQuantile) / 20;
        mindiffu = max([gen, imp]) * 10;
        mindiffBestu = mindiffu + 2 * eps;
        tQuantile = upQuant(q);
        while abs(mindiffu - mindiffBestu ) > eps
            mindiffBestu = mindiffu;
            for i = max(0.5, tQuantile - range * 10) : range : min(1, tQuantile + range * 10)
                upQuant(q) = i;
                [t, pIsGen, polynom] = fusion.genuineImposterPlotwithQuantile(gen, imp, minValue, maxValue, nSupportingPoints, 0, upQuant, [], 0);
                % calculate difference between pIsGen & polynom
                tdiff = sum(abs(pIsGen(:) - polyval(polynom,t(:))));
                % punish positive derivative
                t2 = t(ceil((length(t) / 20)) : ceil(((19 * length(t)) / 20)));
                der = polyder(polynom);
                tDeriv = polyval(der,t2(:));
                tDerivMax = max(tDeriv);
                tDerivQ95 = quantile(tDeriv, 0.95);
                tDerivQ90 = quantile(tDeriv, 0.90);
                tdiff = tdiff + 10 * (tDerivMax + tDerivQ95 + tDerivQ90);
                if tdiff < mindiffu
                    upperbestQuantile = i;
                    mindiffu = tdiff;
                    if doPlot
                        disp(['mindiffu = ',num2str(mindiffu)]);
                        disp(['tDerivMax  = ', num2str(tDerivMax)])
                        disp(['tDerivQ95  = ', num2str(tDerivQ95)])
                        disp(['tDerivQ90  = ', num2str(tDerivQ90)])
                    end
                end
            end
            tQuantile = upperbestQuantile;
            range = range * 0.1;
        end
        if tQuantile >= maxGen
            break;
        end
        upQuant(q) = tQuantile;
    end
    
    mindiffBestu = mindiffu;
    upperbestQuantile = upQuant;
    
    % get lower quantile
    mindiffl = max([gen, imp]) * 10;
    mindiffBestl = mindiffl + 2 * eps;
    tQuantile = 0.5;
    range = 0.1;
    
    while abs(mindiffl - mindiffBestl ) > eps
        mindiffBestl = mindiffl;
        for i = max(0,tQuantile - range * 10) : range : min(0.5, tQuantile + range * 10)
            [t, pIsGen, polynom] = fusion.genuineImposterPlotwithQuantile(gen, imp, minValue, maxValue, nSupportingPoints, i, upperbestQuantile, [], 0);
            % calculate difference between pIsGen & polynom
            tdiff = sum(abs(pIsGen(:) - polyval(polynom,t(:))));
            % punish positive derivative
            t2 = t(ceil((length(t) / 20)) : ceil(((19 * length(t)) / 20)));
            der = polyder(polynom);
            tDeriv = polyval(der,t2(:));
            tDerivMax = max(tDeriv);
            tDerivQ95 = quantile(tDeriv, 0.95);
            tDerivQ90 = quantile(tDeriv, 0.90);
            tdiff = tdiff + 10 * (tDerivMax + tDerivQ95 + tDerivQ90);
            if tdiff < mindiffl
                lowerbestQuantile = i;
                mindiffl = tdiff;
                if doPlot
                    disp(['mindiffl = ',num2str(mindiffl)]);
                    disp(['tDerivMax  = ', num2str(tDerivMax)])
                    disp(['tDerivQ95  = ', num2str(tDerivQ95)])
                    disp(['tDerivQ90  = ', num2str(tDerivQ90)])
                end
            end
        end
        tQuantile = lowerbestQuantile;
        range = range * 0.1;
    end
    
    mindiff = mindiffl;
    
    mindiffu = max([gen, imp]) * 10;
    mindiffBestu = mindiffu + 2 * eps;
    tQuantile = 0.5;
    range = 0.1;
    upQuantI = [];
    
    maxImp = quantile(imp, 0.99);
    
    for q = 1 : 2
        if doPlot
            disp(['q = ', num2str(q)])
        end
        upQuantI = [upQuantI, (1 - tQuantile) / 2 + tQuantile];
        range = (1 - tQuantile) / 20;
        mindiffu = max([gen, imp]) * 10;
        mindiffBestu = mindiffu + 2 * eps;
        tQuantile = upQuantI(q);
        while abs(mindiffu - mindiffBestu ) > eps
           mindiffBestu = mindiffu;
           for i = max(0.5, tQuantile - range * 10) : range : min(1, tQuantile + range * 10)
                upQuantI(q) = i;
                [t, pIsGen, polynom] = fusion.genuineImposterPlotwithQuantile(gen, imp, minValue, maxValue, nSupportingPoints, lowerbestQuantile, upperbestQuantile, upQuantI, 0);
                % calculate difference between pIsGen & polynom
                tdiff = sum(abs(pIsGen(:) - polyval(polynom,t(:))));
                % punish positive derivative
                t2 = t(ceil((length(t) / 20)) : ceil(((19 * length(t)) / 20)));
                der = polyder(polynom);
                tDeriv = polyval(der,t2(:));
                tDerivMax = max(tDeriv);
                tDerivQ95 = quantile(tDeriv, 0.95);
                tDerivQ90 = quantile(tDeriv, 0.90);
                tdiff = tdiff + 10 * (tDerivMax + tDerivQ95 + tDerivQ90);
                if tdiff < mindiffu
                    upperbestQuantileI = i;
                    mindiffu = tdiff;
                    if doPlot
                        disp(['mindiffu = ',num2str(mindiffu)]);
                        disp(['tDerivMax  = ', num2str(tDerivMax)])
                        disp(['tDerivQ95  = ', num2str(tDerivQ95)])
                        disp(['tDerivQ90  = ', num2str(tDerivQ90)])
                    end
                end
            end
            tQuantile = upperbestQuantileI;
            range = range * 0.1;
        end
        upQuantI(q) = tQuantile;
        if tQuantile >= maxImp
            break;
        end
    end
    
    upperbestQuantileI = upQuantI;
    
    if doPlot
        disp('plot');
        fusion.genuineImposterPlotwithQuantile(gen, imp, minValue, maxValue, nSupportingPoints, lowerbestQuantile, upperbestQuantile, upperbestQuantileI, 1);
    end
    
    
        
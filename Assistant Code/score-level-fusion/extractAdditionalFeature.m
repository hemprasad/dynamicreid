function feature = extractAdditionalFeature(data, bodyparts,...
    parCPS, parBVT, parELF, parLab, parMR8, parColor, featureProperties)
%EXTRACTADDITIONALFEATURE Extract (S)ELF, MR8, Lab, BVT and BiCov features.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    % Preparations for the extraction process
    switch featureProperties{1} % switch by the name of the feature
        case {'ELF', 'SELF'} % Load/recalculate bins
            BinEdgesELF = util.loadBinsFromFile('MAT/_BINS_/binedges_elf.mat', 'BinEdgesELF');
            BinEdgesELF = util.checkBinning(BinEdgesELF, parELF, data.Paths, 'ELF');
            
            BinEdgesLab = {}; %for parfor
            BinEdgesMR8 = {}; %for parfor
            gaborfilter = {}; %for parfor
        
        case 'MR8' % Load/recalculate bins
            BinEdgesMR8 = util.loadBinsFromFile('MAT/_BINS_/binedges_mr8.mat','BinEdgesMR8');
            BinEdgesMR8 = util.checkBinning(BinEdgesMR8, parMR8, data.Paths, 'MR8');
            
            BinEdgesELF = {}; %for parfor
            BinEdgesLab = {}; %for parfor
            gaborfilter = {}; %for parfor
        
        case 'Lab' 
            % Load/recalculate bins
            BinEdgesLab = util.loadBinsFromFile('MAT/_BINS_/binedges_lab.mat', 'BinEdgesLab');
            BinEdgesLab = util.checkBinning(BinEdgesLab, parLab, data.Paths, 'Lab');
            
            BinEdgesELF = {}; %for parfor
            BinEdgesMR8 = {}; %for parfor
            gaborfilter = {}; %for parfor
        
        case 'BiCov'
            BinEdgesELF = {}; %for parfor
            BinEdgesMR8 = {}; %for parfor
            BinEdgesLab = {}; %for parfor
            
            gaborfilter = util.loadFromFile('+bicov/bicovfilter.mat', 'gaborfilter'); %for parfor
        
        otherwise
            BinEdgesELF = {}; %for parfor
            BinEdgesMR8 = {}; %for parfor
            BinEdgesLab = {}; %for parfor
            gaborfilter = {}; %for parfor
    end % switch featname
    
    % transformation RGB -> Lab
    rgb2lab = makecform('srgb2lab');
    % mapping for LBP-hist
    mapping = lbp.getmapping(8,'u2');

    % number of images to process
    nImages = size(data.RGB,4);

    % load config for part dimensions from file
    conf = reidaam.load_conf(parCPS);
    part_dims = ceil(parCPS.Scale * conf.part_dims)-1;
    
    if featureProperties{3} % feature for the whole image
        % features for the whole image
        tmpFeat = cell(nImages,1); % container to hold all the features
        
        parfor id=1:nImages
            img_rgb = im2double(data.RGB(:,:,:,id));
            img_hsv = rgb2hsv(img_rgb);
            switch featureProperties{1}
                case 'BiCov' % extract BiCov features
                    try
                        tmpFeat{id,1} = ...
                            bicov.extractBiCovFeatures(util.normalizeSize(img_hsv,128,48),gaborfilter);
                    catch err
                        fprintf('BiCov: Error at Image %i: %s\n',id,err.message);
                    end
                case 'SELF' % extract SELF features (6 splits in height)
                    try
                        tmpFeat{id,1} = ...
                            elf.extractSELFFeatures(img_rgb, 1, 6, BinEdgesELF);
                    catch err
                        fprintf('SELF: Error at Image %i: %s\n',id,err.message);
                    end
            end % switch featname
        end % parfor
        feature.Full = tmpFeat;
    else
        % partwise feature extraction
        nParts = size(bodyparts,2);
        tmpFeat = cell(nImages,nParts); % container to hold all the features
        
        for id=1:nImages  % for each image
            parfor pid=1:nParts % process all parts
                part_rgb = bodyparts{id,pid};
                part_rgb = util.normalizePart(part_rgb,pid,part_dims);
                part_hsv = rgb2hsv(part_rgb);
                part_gray = rgb2gray(part_rgb);
                
                switch featureProperties{1}
                    case 'BVT' % extract the BVT histogram
                        try
                            tmpFeat{id,pid} = bvt.extractBVThist(part_hsv,...
                                parBVT.NBins, parColor.ValBlack,...
                                parColor.SatGray, parColor.WeightGrays);
                        catch err
                            fprintf('BVT: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                            rethrow(err);
                        end
                    case 'BiCov' % extract BiCov features
                        try
                            tmpFeat{id,pid} = ...
                                bicov.extractBiCovFeatures(part_hsv,gaborfilter);
                        catch err
                            fprintf('BiCov: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end
                    case 'LBP' % extract LBP histogram
                        try
                            lbphist = lbp.extractLBPFeatures(part_gray,1,8,mapping,'h');
                            tmpFeat{id,pid} = lbphist./sum(lbphist(:));
                        catch err
                            fprintf('LBP: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end

                    case 'ELF' % extract ELF histogram
                        try
                            tmpFeat{id,pid} = elf.extractELFFeatures(part_rgb,BinEdgesELF);
                        catch err
                            fprintf('ELF: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end
                    case 'MR8' % extract MR8 histogram
                        try
                            tmpFeat{id,pid} = mr8.extractMR8Features(part_gray,BinEdgesMR8);
                        catch err
                            fprintf('MR8: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end
                    case 'Lab' % extract L*a*b* histogram
                        part_lab = applycform(part_rgb,rgb2lab);
                        try
                            tmpFeat{id,pid} = lab.extractLABHist(part_lab,BinEdgesLab);
                        catch err
                            fprintf('Lab: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end
                    case 'wHSV' % extract wHSV histogram
                        try
                            tmpFeat{id,pid} = whsv.estimatewHSVhist(part_hsv);
                        catch err
                            fprintf('wHSV: Error at Image %i, Part %i: %s\n',id, pid,err.message);
                        end
                    otherwise
                        error('Unknown feature %s!',featureProperties);
                end % switch featname
            end % for each part
        end % parfor each image
        
        % Partnames for struct
        partnames = {'Torso' 'Shoulders' 'Head' 'RArm' 'RForearm' 'LArm'...
        'LForearm' 'RThigh' 'RLeg' 'LThigh' 'LLeg'};
        for pid=1:nParts
            feature.(partnames{pid}) = tmpFeat(:,pid);
        end
    end % if
end % function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Person Re-identification by Articulated Appearance Matching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Dataset: VIPeR
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% This file has been modified to feature a broader width of feature
% extraction and matching techniques. Modifications are not limited to this
% file.
%                                                                         
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:   
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based  
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),  
% Killarney, Ireland, pp. 469-476, IEEE 2015.                             
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Run_VIPeR()
%% Preliminaries
% clear; warning on all;
import reidaam.*;
clc
%% User Settings
expr.Name  = 'VIPeR'; % dataset name VIPeR iLIDS ETHZ1 ETHZ2 ETHZ3
expr.Sett  = 'SvsS';  % experimental setting SvsS MvsM
expr.Num   = '001';   % exp number (for multiple experiments)
expr.Mask  = 'PS';    % type of masks PS CPS
expr.Shots = 1;       % default number of imgs for the multi-shot case
xval.Type = 'VIPeR';  % type of cross-validation
xval.Runs = 10;       % number of cross-validation runs
expr.LoadPart = 1;    % load saved partitions (to compare results)
expr.calcGrps = 0;    % calculate CMC of parts combos instead of parts
expr.showEBar = 0;    % show error bars in the CMC plots

expr.SavePose = 0;    % Save pose images to file
expr.SaveMask = 0;    % Save mask images to file
expr.SavePart = 0;    % Save parts as single images

expr.SDALFFeat = 1;   % Calculate SDALF-Features?
expr.SDCFeat = 1;     % Calculate SDC-Features?  

% only one image for the static case
if strcmp(expr.Sett,'SvsS'), expr.Shots = 1; end

%% Setup input
data.Scale = 1; % for low-res tests, default = 1
% data.Paths = {'datasets/','../datasets/'}; % dataset paths
data.Paths = {'datasets/'};
data.Height = 128;  % normalized height and width of images
data.Width = 48;

% load the dataset directories (note: the script may modify above params)
[data.Dir,data.Files] = loadDataDirs(data.Paths,expr.Name);

% height and width of data images to be used
H = data.Height * data.Scale;
W = data.Width * data.Scale;

%% PARAMETERS
% number of bins as in "Semi-supervised Multi-feature Learning for Person 
% Re-identification" by Figueira et. al. (2013)
parMR8.NBins = 10*ones(8,1);
parMR8.ForceRecalcBinning = 0;
parLab.NBins = [10 10 10];
parLab.ForceRecalcBinning = 0;
parBVT.NBins = [14 14 10];
parBVT.ForceRecalcBinning = 0;
% number of bins as in "Person Re-Identification by Support Vector Ranking"
% by Prosser et. al. (2010)
parELF.NBins = 16*ones(29,1);
parELF.ForceRecalcBinning = 0;
parELF.FiltSize = 21;

% segmentation params
parCPS.BodyConf = 'PARSEfullprior';
parCPS.Scale = 0.9;
parCPS.Rots = -30:10:30;  % Allowed rotation angles for parts
parCPS.WindowStep = 4;    % Step in the detection windows gridding
% parCPS.Dir = ['CPS/' expr.Name '_Exp' expr.Num]; % dir to save stuff
parCPS.MinProb = 1e-10;   % min prob during normalizations
parCPS.Filters = {0.5,0.4,0.3};
% util.chkmkdir(parCPS.Dir);
    
% feature extraction params
parColor.NBINs = [20 10 10]; % number of HSV channels quantization bins
parColor.ValBlack = 0.2;     % value below this threshold makes blacks
parColor.SatGray = 0.2;      % saturation below this threshold makes grays
parColor.WeightGrays = 0.2774; % balance between grays and colors
parColor.WeightParts = ...
  [100 67 41.55 3.3 13 3.3 13 52.77 23.87 52.77 23.87]/100;
parColor.DistMeasure = 'Bhattacharyya';
parMSCR = struct('min_margin',0.003,'ainc',1.05,'min_size',15,...
  'filter_size',3,'verbosefl',0,'normY',H,'normC',200,'gamma',0.4008);
xval.beta  = 0.071;
xval.TuneParams = 0;
xval.RecalcDistances = false;

%% Run main script
xval.maskfun = @get_masks;
xval.featfun = @extract_gc;
xval.distfun = @calc_distance_gc;
xval.xvalfun = @run_crossval;

Do_MainAlgorithmSingle;


function [features] = extractSELFFeatures(Img_RGB, nSplitX, nSplitY, BinEdgesELF)
%EXTRACTSELFFEATURES Extract SELF features of RGB image.
% If you use them please also cite [Prosser et al., 2010]:
% @inproceedings{prosser2010self,
%   title={Person Re-Identification by Support Vector Ranking.},
%   author={Prosser, Bryan and Zheng, Wei-Shi and Gong, Shaogang and Xiang,
%   Tao and Mary, Q},
%   booktitle={Proc. of British Machine Vision Conference (BMVC)},
%   pages={},
%   year={2010}
% }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author:                                                                  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin < 4
        % load prepared bins from file
        load('MAT/_BINS_/binedges_elf.mat')
    end
    if nargin == 1
        nSplitX = 1;
        nSplitY = 6;
    end
    assert(nSplitX > 0,'The number of splits has to be greater zero.');
    assert(nSplitY > 0,'The number of splits has to be greater zero.');
    assert(ndims(Img_RGB) == 3,'I has to be a 3 channel RGB image of size MxNx3.');

    % split image into six stripes
    % prepare image to be split up
    Img_RGB = util.prepareForSplit(Img_RGB, nSplitX, nSplitY, 'resize');
    [H,W,~] = size(Img_RGB);
    sy = H/nSplitY;
    sx = W/nSplitX;
    %     channel = cell(D,1);
    % calculate gradient features per channel for the whole image
    imsplits = mat2cell(Img_RGB,sy*ones(nSplitY,1),sx*ones(nSplitX,1),3);
    imsplits = reshape(imsplits,[numel(imsplits) 1]);

    splitfeat = cell(nSplitX*nSplitY,1);
    for split=1:length(imsplits)
        splitfeat{split} = elf.extractELFFeatures(imsplits{split},BinEdgesELF);
    end

    features = cell2mat(splitfeat');

    features = features(:)' / sum(features(:));
    
end


function [features] = extractELFFeatures(Img_RGB, BinEdges, filtsize)
%EXTRACTELFFEATURES Extract ELF-features of a RGB image.
% @inproceedings{gray2008elf,
% title={Viewpoint Invariant Pedestrian Recognition with an Ensemble of
% Localized Features},
% author={Gray, Douglas and Tao, Hai},
% year={2008},
% booktitle={Proc. of European Cof. on Computer Vision (ECCV)},
% pages={262--275}
% }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if ~size(Img_RGB,3) == 3
        error('I has to be a 3 channel RGB image.');
    end
    
    if nargin < 2
        % load binedges from file
        BinEdges = util.loadBinsFromFile('MAT/_BINS_/binedges_elf.mat','BinEdgesELF');
    end
    
    if numel(BinEdges) ~= 29
       error(['Either to much or to less binegdes for ELF.\n'...
           'numel(BinEdges) has to be 29.']); 
    end

    N = 21;
    if nargin == 3
        N = filtsize;
    end

    if isinteger(Img_RGB)
        Img_RGB = im2double(Img_RGB);
    end
    
    img_hsv = rgb2hsv(Img_RGB);
    img_ycbcr = rgb2ycbcr(Img_RGB);

    img_hist = cat(3,Img_RGB,img_hsv(:,:,1:2),img_ycbcr);
    
    % color histograms
    color_features = [];
    for i=1:8
        channel = img_hist(:,:,i);
        bincount = histc(channel(:),BinEdges{i});
        color_features = [color_features; bincount(1:end-1)];
    end

    % extract texture information of the Luminance channel
    % 21 texture filters (8 Gabor, 13 Schmid), size 21x21
    fbGabor = elf.generateGaborFilter(N);
    fbSchmid = elf.generateSchmidFilter(N);
    fbTexture = [fbGabor; fbSchmid];

    texture_features = [];
    for i=1:21
        conv_imgY = imfilter(img_ycbcr(:,:,1),real(fbTexture{i}),'same',0);
        bincount = histc(conv_imgY(:),BinEdges{i+8});
        texture_features = [texture_features; bincount(1:end-1)];
    end

    % combine
    features = [color_features; texture_features];
    features = features(:)' / sum(abs(features(:)));
end


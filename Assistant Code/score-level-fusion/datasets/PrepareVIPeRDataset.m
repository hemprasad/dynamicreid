function PrepareVIPeRDataset(datasetsPath)
%PREPAREVIPERDATASET (Download and) unpack VIPER dataset

if nargin == 0
    datasetsPath = '.';
end

outputDir = fullfile(datasetsPath, 'VIPeR');
zipFile = fullfile(datasetsPath, 'VIPeR.v1.0.zip');
if ~exist(zipFile, 'file')
    try
        url = 'https://users.soe.ucsc.edu/~manduchi/VIPeR.v1.0.zip';
        if ~exist(outputDir, 'dir')
            mkdir(datasetsPath)
        end
        if ispc || ismac
            msgID = 'PrepareVIPeRDataset:DownloadFailed';
            msg = ['Please download VIPeR.v1.0.zip from ' url ' and put it' ...
                ' into the datasets/ directory! Then rerun this function.'];
            throw(MException(msgID,msg))
        else
            % unzip with URL does not seem to work for https:// links
            % use a workaround for linux
            system(['wget ' url]);
        end
        
        clc; fprintf('Download of VIPeR dataset succeeded!\n');
    catch ME
        throw(ME);
    end
end

try
    unzip(zipFile, datasetsPath);
    fprintf('Unpacking of VIPeR dataset succeeded!\n');
    flattenVIPeRDataset(datasetsPath);
catch ME
    fprintf('Warning: Unpacking of VIPeR dataset failed!\n');
    throw(ME);
end

end

function flattenVIPeRDataset(datasetsPath, moveFiles)
% convert image filenames from the original distribution to the convention
% accepted by SDALF, CPS, and AAM

if nargin == 1
    moveFiles = 0; % set to 0 to make copies
end

out = fullfile(datasetsPath, 'VIPeRa/');
mkdir(out);
camA = fullfile(datasetsPath, 'VIPeR', 'cam_a');
camB = fullfile(datasetsPath, 'VIPeR', 'cam_b');

filesA = dir(fullfile(camA, '*.bmp'));
filesB = dir(fullfile(camB, '*.bmp'));

for i = 1:length(filesA)
    if moveFiles
        movefile([camA filesA(i).name],[out sprintf('%04d001.bmp',i)]);
        movefile([camB filesB(i).name],[out sprintf('%04d002.bmp',i)]);
    else
        copyfile(fullfile(camA, filesA(i).name),...
            fullfile(out, sprintf('%04d001.bmp',i)));
        copyfile(fullfile(camB, filesB(i).name),...
            fullfile(out, sprintf('%04d002.bmp',i)));
    end
end

end
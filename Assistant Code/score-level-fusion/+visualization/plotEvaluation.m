function [legendEntrys,pHandles,idxSort] = plotEvaluation(dataset,expNum,method)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
hold on;
%     path = '../results/VIPeR_Exp001/transformedTablesFinal/';
%     path = ['../results/' dataset '_Exp001/transformedTablesFinal/'];
path = ['../results/' dataset '_Exp' expNum '/otherWeightmethod/'];
%     fnames = dir([path 'LearnTest_*']);
fnames = dir([path 'OtherWeights_*_' method '*']);
C = colormap(jet(length(fnames)+1));
%     C = circshift(C,[1 0]);
C(5,:) = [255,0,255]/255;
%     legendEntrys = cell(length(fnames),1);
legendEntrys = {'D-Prime','Equal Weighted','Non-Confidence Width','Area Under Curve','Equal Error Rate',...
    'Rank 1', 'Rank 10'};
pHandles = zeros(1,length(fnames)+1);

nAUC = zeros(1,length(fnames)+1);
for i=1:length(fnames)
    results = load([path fnames(i).name]);
%         plot(results.cmc*100,'Color',C(i,:),'LineWidth',2);
    pHandles(i+1) = plot(results.cmc_new*100,'Color',C(i,:),'LineWidth',2);
    nAUC(i+1) = results.AUC_new;
    legendEntrys{i} = [legendEntrys{i} ' [' num2str(results.AUC_new,'%4.3f') ']'];
%         nameParts = strsplit(fnames(i).name,'_');
%         legendEntrys{i} = nameParts{3}(1:end-4);
%         legendEntrys{i} = nameParts{end}(1:end-4);
end

path = ['../results/' dataset '_Exp' expNum '/transformedTablesFinal/'];
fname = dir([path 'LearnTest_*_' method '*']);
result = load([path fname(1).name]);
pHandles(1) = plot(result.cmc*100,'-','Color',[0,204,0]/255,'LineWidth',2);
nAUC(1) = result.AUC;

legendEntrys = [['Proposed Method [' num2str(result.AUC,'%4.3f') ']'], legendEntrys];

[~,idxSort] = sort(nAUC,'descend');

legendEntrys = legendEntrys(idxSort);
pHandles = pHandles(idxSort);

%     legend(legendEntrys,'Location','SouthEast')
grid on
%     axis([1 100 15 100])
axis([1 50 10 90])

xlabel('Rank');
ylabel('Recognition Percentage');

end
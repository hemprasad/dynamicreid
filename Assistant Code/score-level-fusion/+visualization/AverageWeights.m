function AverageWeights(weights,legendTables,titleString)
%AVERAGEWEIGHTS Bar plot of average weight distribution
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

aweights = zeros(size(weights{1}));

for i=1:10
    aweights = aweights+0.1*weights{i};
end

%%
legendTables{5} = 'wHSV_{CPS}';
legendTables{7} = 'wHSV_{72D}';
if length(legendTables) > 8
    legendTables{11} = 'PCHR_{RND}';
    legendTables{12} = 'PCHR_{Clust}';
end

%%
hF = figure;
bar(aweights)
axis([0.5 length(legendTables)+0.5 0 0.25])
title(titleString,'FontSize',24)
ylabel('Average weight')
pos = get(gca,'Position');
pos(1:2) = [0.105 0.30];
pos(3) = pos(3) + 0.05;
pos(4) = pos(4) - 0.20;
set(gca,'Position',pos)
set(gca,'XTickLabel',legendTables)
th = visualization.rotateticklabel(gca,90);
set(th,'FontSize',24);

%%
xSize = 28; ySize = 15;
% set(gcf,'PaperPositionMode','auto')
paperSizeX = 25;
paperSizeY = 16;
set(hF,'PaperSize',[paperSizeX paperSizeY]);
xLeft = (paperSizeX-xSize)/2; 
yTop = (paperSizeY-ySize)/2;
set(hF,'PaperUnits','centimeters');
set(hF,'PaperPosition',[xLeft yTop xSize ySize])
% Screen position and size
X = 1100;
Y = 100;
set(hF,'Position',[X Y xSize*20 ySize*20])

print('../images/test.pdf','-dpdf');

end
function LogRegressionVisu()
%LOGREGRESSIONVISU plot logistic regression
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, A. Vorndran                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialization
% visualization.Init;

%% get genuine and imposter scores
% genuineMask = mask;
% realGenuines = genuineMask;
% realGenuines(eye(size(mask,1)) == 1) = 0;
% 
% genuinesWCH_raw  = Tables_VIPeR{84}(realGenuines);
% genuinesWCH_raw  = genuinesWCH_raw(:)';
% impostersWCH_raw  = Tables_VIPeR{84}(~genuineMask);
% impostersWCH_raw  = impostersWCH_raw(:)';
load('CSV/genuinesWCH.csv')
load('CSV/impostersWCH.csv')

%%
minWCH = min([genuinesWCH, impostersWCH]);
maxWCH = max([genuinesWCH, impostersWCH]);
t = linspace(minWCH, maxWCH, 1000);
genKDE = fusion.KDE(genuinesWCH, t);
impKDE = fusion.KDE(impostersWCH, t);

ratio = genKDE ./ impKDE;
logRatio = log(ratio);

qI = quantile(impostersWCH, 0.08);
qG = quantile(genuinesWCH, 0.98);

tMask = and(t >= qI, t <= qG);
t2 = t(tMask);
lr2 = logRatio(tMask);

p = genKDE ./ (genKDE + impKDE);

polynomial1 = polyfit(t2, lr2, 1);
a = polynomial1(1);
b = polynomial1(2);

polyT = a * t2 + b;

tRatio = exp(a * t + b);
tNorm = tRatio ./ (tRatio + 1);

%%
red = [168 0 0]/255;
green = [17 129 30]/255;

figure(1)

subplot(3,1,1)
plot(t, genKDE, '-','LineWidth',2,'Color',green);
hold on
plot(t, impKDE, '--','LineWidth',2,'Color',red);
axis([0.1 0.6 0 8])
xlabel('s')
ylabel('Prob. Density')

subplot(3,1,2)
plot(t, p, 'b-','LineWidth',2);
hold on
plot(t, tNorm, 'm--','LineWidth',2);
axis([0.1 0.6 0 1])
plot([min(t2) min(t2)],[0 1],'k--','LineWidth',2)
plot([max(t2) max(t2)],[0 1],'k--','LineWidth',2)
xlabel('s')
ylabel('p(gen|s)')

subplot(3,1,3)
plot(t2, lr2, 'b-','LineWidth',2);
hold on
plot(t2, polyT, 'm--','LineWidth',2);
axis([0.1 0.6 -2 2])
plot([min(t2) min(t2)],[-2 2],'k--','LineWidth',2)
plot([max(t2) max(t2)],[-2 2],'k--','LineWidth',2)
xlabel('s')
ylabel('log(LR)')

%%
xSize = 20; ySize = 20;
% set(gcf,'PaperPositionMode','auto')
paperSizeX = 18;
paperSizeY = 18;
set(gcf,'PaperSize',[paperSizeX paperSizeY]);
xLeft = (paperSizeX-xSize)/2; yTop = (paperSizeY-ySize)/2;
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
% Screen position and size
X = 1100;
Y = 100;
set(1,'Position',[X Y xSize*50 ySize*50])
print(['../images/LogRegression.pdf'],'-dpdf');
% figure,imshow(imread('../images/LinearRegression.png'))
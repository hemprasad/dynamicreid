%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialization
clear%vars
close all
warning off MATLAB:polyfit:RepeatedPointsOrRescale 
% warning off MATLAB:hg:surface:CDataMustBeDoubleOrUint8
nRuns = 1;
plotColor = 0;
imageFolder = '../images/';
util.chkmkdir(imageFolder);

% type = 'LikelihoodRatio';

dataset = 'VIPeR';
datasetPath = 'datasets/VIPeRa';
datasetFileSuffix = '.bmp';
gallerySize = 316;

%% load tables
tables = util.loadFromFile(['../results/', dataset, '_Exp001/matching/Tables_', dataset, '_x001.mat'], ['Tables_', dataset]);
% get wHSV and MSCR
tables = tables([68 69]);
% load genuine mask
mask = util.loadFromFile(['../results/', dataset, '_Exp001/matching/genuineMask_', dataset, '_x001.mat'], ['genuineMask_', dataset]);


if ~exist('+visualization/results.mat','file')
    %% calculate weights
    stime = clock;
    [~, ~, ~, parametersPLR, weightsPLR, ~, partitionTest] = ...
        util.evaluation(tables, mask, datasetPath, datasetFileSuffix, 'LikelihoodRatio', nRuns, gallerySize, plotColor);
    [~, ~, ~, parametersLR, ~, ~, ~] = ...
        util.evaluation(tables, mask, datasetPath, datasetFileSuffix, 'LogisticRegression', nRuns, gallerySize, plotColor);
    
    %% transformation
    nTables = numel(tables);
    Tables_Trans = cell(nTables,1);
    n = 632;
    LookupTable = parametersPLR{1};
    for i = 1 : nTables
        nSupportingPoints = size(LookupTable{i}, 2);
        Table = LookupTable{i};
        L = zeros(n); 
        U = nSupportingPoints * ones(n); 
        index = zeros(n);
        transTable = tables{i}(partitionTest,partitionTest);

        tooSmall = transTable <= Table(1,1);
        tooBig = transTable >= Table(1,nSupportingPoints);

        maskRight = not(tooSmall) & not(tooBig);
        range = (max(Table(1,:)) - min(Table(1,:))) / (nSupportingPoints - 1);
        index(maskRight) = (ceil(transTable(maskRight) ./ range) * range - min(Table(1,:))) / range + 1;
        U(maskRight) = max(2,min(nSupportingPoints,round(index(maskRight))));   % upper index
        L(maskRight) = U(maskRight) - 1;                                        % lower index 

        matchingTrans = transTable;
        matchingTrans(tooSmall) = Table(2,1);
        matchingTrans(tooBig) = Table(2, nSupportingPoints);    
        slope = (Table(2,U(maskRight)) - Table(2,L(maskRight))) ./ (Table(1,U(maskRight)) - Table(1,L(maskRight)));
        matchingTrans(maskRight) = slope .* (transTable(maskRight)' - Table(1,L(maskRight))) + Table(2,L(maskRight));

        Tables_Trans{i} = matchingTrans;
    end
    save('+visualization/results.mat','parametersPLR','parametersLR','weightsPLR','partitionTest','Tables_Trans');
    fprintf('Calculation took %s\n',util.getDurationString(stime));
else
    load('+visualization/results.mat');
end


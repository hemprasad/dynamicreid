function jointGenuineImposterPlot(genuineScores, imposterScores, minValues, maxValues, nSupportingPoints,weights)
%JOINTGENUINEIMPOSTERPLOT plot joint-genuine-imposter-distribution
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, A. Vorndran                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t1 = linspace(minValues(1), maxValues(1), nSupportingPoints);
t2 = linspace(minValues(2), maxValues(2), nSupportingPoints);
[dGen, NGen] = size(genuineScores);
sigmaGen = std(genuineScores');
hGen = sigmaGen * (4 / (dGen + 2))^(1 / (dGen + 4)) * NGen^(-1 / (dGen + 4)); % kernel width

[T1, T2] = meshgrid(t1, t2);
T1 = T1(:);
T2 = T2(:);

genuineScores = double(genuineScores);
imposterScores = double(imposterScores);

for i = 1:length(T1);
    dT1 = (genuineScores(1, :) - T1(i)) / hGen(1);
    dT2 = (genuineScores(2, :) - T2(i)) / hGen(2);
    distSq = dT1.^2 + dT2.^2;
    k = exp(-0.5 * distSq);
    p = (1 / NGen) * (1 / (2 * pi * sqrt(hGen(1) * hGen(2)))) * sum(k');
    pGen(i) = p;
end

peakValGen = max(pGen(:));

[dImp, NImp] = size(imposterScores);
sigmaImp = std(imposterScores');
hImp = sigmaImp * (4 / (dImp + 2))^(1 / (dImp + 4)) * NImp^(-1 / (dImp + 4)); % kernel width
for i = 1:length(T1)
    dT1 = (imposterScores(1, :) - T1(i)) / hImp(1);
    dT2 = (imposterScores(2, :) - T2(i)) / hImp(2);
    distSq = dT1 .* dT1 + dT2 .* dT2;
    k = exp(-0.5 * distSq);
    p = (1 / NImp) * (1 / (2 * pi * sqrt(hImp(1) * hImp(2)))) * sum(k');
	pImp(i) = p;
end

genFus = weights(1) * genuineScores(1, :) + weights(2) * genuineScores(2, :);
impFus = weights(1) * imposterScores(1, :) + weights(2) * imposterScores(2, :);
pfGen = fusion.KDE(genFus, t1);
pfImp = fusion.KDE(impFus, t1);

% integralG = sum(pfGen)
% integralI = sum(pfImp)

peakValImp = max(pImp(:));

pIsGen = zeros(1, length(T1));
for i = 1:length(T1)
    sumP = pGen(i) + pImp(i);
    if sumP > 0
        pIsGen(i) = pGen(i) / sumP;
    end
end

T1 = reshape(T1, nSupportingPoints, nSupportingPoints);
T2 = reshape(T2, nSupportingPoints, nSupportingPoints);
pImp = reshape(pImp, nSupportingPoints, nSupportingPoints);
pGen = reshape(pGen, nSupportingPoints, nSupportingPoints);
pIsGen = reshape(pIsGen, nSupportingPoints, nSupportingPoints);
% colormap
CGen(:,:,1) = zeros(nSupportingPoints, nSupportingPoints);
% CGen(:,:,2) = (pGen / max(pGen(:))) * 0.2 + 0.8;
CGen(:,:,2) = (pGen / max(pGen(:))) * 0.3 + 0.7;
CGen(:,:,3) = zeros(nSupportingPoints, nSupportingPoints);
% CImp(:,:,1) = (pImp / max(pImp(:))) * 0.2 + 0.8;
CImp(:,:,1) = (pImp / max(pImp(:))) * 0.3 + 0.7;
CImp(:,:,2) = zeros(nSupportingPoints, nSupportingPoints);
CImp(:,:,3) = zeros(nSupportingPoints, nSupportingPoints);

% preparations for plot
impOffset = 0.007;
xEdge = weights(1) / max(weights);
yEdge = weights(2) / max(weights);

xAxes = linspace(0,1,nSupportingPoints);
yAxes = linspace(0,1,nSupportingPoints);
dummy = zeros(1,nSupportingPoints);
xAxesProj = linspace(0,xEdge,nSupportingPoints);
yAxesProj = linspace(0,yEdge,nSupportingPoints);

mx = max(pGen(:));
sg1 = sum(pGen);
sg2 = sum(pGen,2)';
sg3 = pfGen;
si1 = sum(pImp);
si2 = sum(pImp,2)';
si3 = pfImp;

% plot section
figure
hold on;
set(gcf,'renderer','opengl')

orange = [243 155 89]/255;
red = [168 0 0]/255;
green = [17 129 30]/255;

% background
xPlaneOverlap = min(sg1,si1) ./ max(sg1) .* mx;
xX = [ 0.00 xAxes 1.00];
yX = [ 0.00 dummy 0.00];
zX = [ 0.50 xPlaneOverlap 0.50];
pX = patch(xX,yX,zX,'w');
% set(pX,'facealpha',0.8)

yPlaneOverlap = min(sg2,si2) ./ max(sg2) .* mx;
xY = [ 0.00 dummy 0.00];
yY = [ 0.00 yAxes 1.00];
zY = [ 0.50 yPlaneOverlap 0.50];
pY = patch(xY,yY,zY,'w');
% set(pY,'facealpha',0.8)

% projection plane
pPlaneOverlap = min(sg3,si3) ./ max(sg3) .* mx;
x = [xAxesProj      xEdge  0.00];
y = [yAxesProj      yEdge  0.00];
z = [pPlaneOverlap  0.50   0.50];
p = patch(x,y,z,'w');
set(p,'facealpha',0.8)

% mark overlap
pOverlap = patch(xAxesProj,yAxesProj,pPlaneOverlap,'r');
set(pOverlap,'facecolor',orange);
set(pOverlap,'facealpha',0.8)

xOverlap = patch(xAxes,dummy,xPlaneOverlap,'r');
set(xOverlap,'facecolor',orange);
set(xOverlap,'facealpha',0.8)

yOverlap = patch(dummy,yAxes,yPlaneOverlap,'r');
set(yOverlap,'facecolor',orange);
set(yOverlap,'facealpha',0.8)

% projections
plot3(T1(1,:), T2(1,:), sg1 ./ max(sg1) .* mx, '-', 'lineWidth', 2,'Color',green);
plot3(T1(1,:), T2(1,:), si1 ./ max(sg1) .* mx, '--', 'lineWidth', 2,'Color',red);
plot3(T1(:,1), T2(:,1), sg2 ./ max(sg2) .* mx, '-', 'lineWidth', 2,'Color',green);
plot3(T1(:,1), T2(:,1), si2 ./ max(sg2) .* mx, '--', 'lineWidth', 2,'Color',red);
% plot3(linspace(0,0.75/0.65,nSupportingPoints), linspace(0,0.11/0.65,nSupportingPoints), sg3 ./ max(sg3) .* mx, 'g:', 'lineWidth', 2);
% plot3(linspace(0,0.75/0.65,nSupportingPoints), linspace(0,0.11/0.65,nSupportingPoints), si3 ./ max(sg3) .* mx, 'r:', 'lineWidth', 2);
plot3(xAxesProj, yAxesProj, sg3 ./ max(sg3) .* mx, '-', 'lineWidth', 2,'Color',green);
plot3(xAxesProj, yAxesProj, si3 ./ max(sg3) .* mx, '--', 'lineWidth', 2,'Color',red);

% joint distribution
hG = surf(T1, T2, pGen, CGen);
% set(hG,'facealpha',0.6)
set(hG,'edgecolor', [0 0.5 0])
% set(hG,'edgealpha',0.8)
grid off
hold on
hI = surf(T1, T2, pImp+impOffset, CImp);
% set(hI,'facealpha',0.6)
set(hI,'edgecolor', [0.5 0 0])
% set(hI,'edgealpha',0.8)
grid off
hold off
axis([minValues(1) maxValues(1) minValues(2) maxValues(2) 0 0.5])
view([maxValues(1)*0.25 maxValues(2)*2.5 0.35]);

% figure
% hG = surf(T1, T2, pGen, CGen);
% % set(hG,'facealpha',0.6)
% set(hG,'edgecolor', [0 0.5 0])
% % set(hG,'edgealpha',0.8)
% grid off
% hold on
% hI = surf(T1, T2, pImp+impOffset, CImp);
% % set(hI,'facealpha',0.6)
% set(hI,'edgecolor', [0.5 0 0])
% % set(hI,'edgealpha',0.8)
% grid off
% hold off
% axis([minValues(1) maxValues(1) minValues(2) maxValues(2) 0 0.5])
% view([maxValues(1)*0.25 maxValues(2)*2.5 0.35]);

% figure
% surf(T1, T2, pIsGen)
% grid off
%
% mnGen = mean(genuineScores');
% mnImp = mean(imposterScores');
% mdGen = median(genuineScores');
% mdImp = median(imposterScores');
% figure
% contour(T1, T2, pGen, (peakValGen/10):(peakValGen/10):peakValGen)
% hold on
% contour(T1, T2, pImp, (peakValImp/10):(peakValImp/10):peakValImp)
% plot([mnGen(1), mnImp(1)], [mnGen(2), mnImp(2)], 'b-');
% plot([mdGen(1), mdImp(1)], [mdGen(2), mdImp(2)], 'g-');
% 
% gModePath = [mnGen];
% 
% actModeG = mnGen;
% for i = 1:200
%     dT1 = (genuineScores(1, :) - actModeG(1)) / hGen(1);
%     dT2 = (genuineScores(2, :) - actModeG(2)) / hGen(2);
%     distSq = dT1.^2 + dT2.^2;
%     w = 0.5 * exp(-0.5 * distSq);
%     newModeG = sum(genuineScores' .* [w' w']) / sum(w);
%     difG = newModeG - actModeG;
%     amount = sqrt(sum(difG'.^2));
%     if amount < 0.0001
%         i
%         break;
%     else
%         if amount < 0.001
%             difG = (difG / amount) * 0.001;
%             actModeG = actModeG + difG;
%         else
%             actModeG = newModeG;
%         end
%         gModePath = [gModePath; actModeG];
%     end
% end
% 
% iModePath = [mnImp];
% 
% actModeI = mnImp;
% for i = 1:200
%     dT1 = (imposterScores(1, :) - actModeI(1)) / hImp(1);
%     dT2 = (imposterScores(2, :) - actModeI(2)) / hImp(2);
%     distSq = dT1.^2 + dT2.^2;
%     w = exp(-0.5 * distSq);
%     newModeI = sum(imposterScores' .* [w' w']) / sum(w);
%     difI = newModeI - actModeI;
%     amount = sqrt(sum(difI'.^2));
%     if amount < 0.0001
%         i
%         break;
%     else
%         if amount < 0.001
%             difI = (difI / amount) * 0.001;
%             actModeI = actModeI + difI;
%         else
%             actModeI = newModeI;
%         end
%         iModePath = [iModePath; actModeI];
%     end
% end
% 
% 
% % meanMode = 0.5 * (actModeG + actModeI);
% % jModePath = [meanMode];
% % 
% % lastP = 0;
% % actModeJ = meanMode;
% % for i = 1:500
% %     alpha = (pi/16):(pi/16):(2*pi);
% %     dx = cos(alpha) * 0.01;
% %     dy = -sin(alpha) * 0.01;
% %     pos = repmat(actModeJ, length(alpha), 1) + [dx' dy'];
% %     pGenTmp = zeros(length(alpha), 1);
% %     pImpTmp = zeros(length(alpha), 1);
% %     for i = 1:size(pos, 1);
% %         dT1 = (genuineScores(1, :) - pos(i, 1)) / h(1);
% %         dT2 = (genuineScores(2, :) - pos(i, 2)) / h(2);
% %         distSq = dT1.^2 + dT2.^2;
% %         k = exp(-0.5 * distSq);
% %         p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %         pGenTmp(i) = p;
% %         dT1 = (imposterScores(1, :) - pos(i, 1)) / hImp(1);
% %         dT2 = (imposterScores(2, :) - pos(i, 2)) / hImp(2);
% %         distSq = dT1 .* dT1 + dT2 .* dT2;
% %         k = exp(-0.5 * distSq);
% %         p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %         pImpTmp(i) = p;
% %     end
% %     pIsGenTmp = pGenTmp ./ (pGenTmp + pImpTmp);
% %     [maxVal, maxIdx] = max(pIsGenTmp);
% %     if maxVal > (lastP + 0.0001)
% %         actModeJ = pos(maxIdx, :);
% %         jModePath = [jModePath; actModeJ];
% %     else
% %         break;
% %     end
% % end
% 
% %gradientVector = [];
% 
% % for g = 1:60
% %     actModeJ = genuineScores(:,g)';
% %     tmpGradientVector = [actModeJ];
% %     for i = 1:5
% %         alpha = (pi/16):(pi/16):(2*pi);
% %         dx = cos(alpha) * 0.01;
% %         dy = -sin(alpha) * 0.01;
% %         pos = repmat(actModeJ, length(alpha), 1) + [dx' dy'];
% %         pGenTmp = zeros(length(alpha), 1);
% %         pImpTmp = zeros(length(alpha), 1);
% %         for i = 1:size(pos, 1);
% %             dT1 = (genuineScores(1, :) - pos(i, 1)) / h(1);
% %             dT2 = (genuineScores(2, :) - pos(i, 2)) / h(2);
% %             distSq = dT1.^2 + dT2.^2;
% %             k = exp(-0.5 * distSq);
% %             p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %             pGenTmp(i) = p;
% %             dT1 = (imposterScores(1, :) - pos(i, 1)) / hImp(1);
% %             dT2 = (imposterScores(2, :) - pos(i, 2)) / hImp(2);
% %             distSq = dT1 .* dT1 + dT2 .* dT2;
% %             k = exp(-0.5 * distSq);
% %             p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %             pImpTmp(i) = p;
% %         end
% %         pIsGenTmp = pGenTmp ./ (pGenTmp + pImpTmp);
% %         [maxVal, maxIdx] = max(pIsGenTmp);
% %         actModeJ = pos(maxIdx, :);
% %         tmpGradientVector = [tmpGradientVector; actModeJ];
% %     end
% %     gradientVector = [gradientVector tmpGradientVector];
% % end
% 
% %tangentVector = [];
% 
% % for g = 1:size(genuineScores, 2)
% %     actModeJ = genuineScores(:,g)';
% %     tmpTangentVector = [actModeJ];
% %     for i = 1:5
% %         dT1 = (genuineScores(1, :) - actModeJ(1)) / h(1);
% %         dT2 = (genuineScores(2, :) - actModeJ(2)) / h(2);
% %         distSq = dT1.^2 + dT2.^2;
% %         k = exp(-0.5 * distSq);
% %         p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %         pGenTmpR = p;
% %         dT1 = (imposterScores(1, :) - actModeJ(1)) / hImp(1);
% %         dT2 = (imposterScores(2, :) - actModeJ(2)) / hImp(2);
% %         distSq = dT1 .* dT1 + dT2 .* dT2;
% %         k = exp(-0.5 * distSq);
% %         p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %         refP = pGenTmpR / (p + pGenTmpR);
% %         
% %         alpha = (pi/16):(pi/16):(2*pi);
% %         dx = cos(alpha) * 0.01;
% %         dy = -sin(alpha) * 0.01;
% %         pos = repmat(actModeJ, length(alpha), 1) + [dx' dy'];
% %         pGenTmp = zeros(length(alpha), 1);
% %         pImpTmp = zeros(length(alpha), 1);
% %         for i = 1:size(pos, 1);
% %             dT1 = (genuineScores(1, :) - pos(i, 1)) / h(1);
% %             dT2 = (genuineScores(2, :) - pos(i, 2)) / h(2);
% %             distSq = dT1.^2 + dT2.^2;
% %             k = exp(-0.5 * distSq);
% %             p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %             pGenTmp(i) = p;
% %             dT1 = (imposterScores(1, :) - pos(i, 1)) / hImp(1);
% %             dT2 = (imposterScores(2, :) - pos(i, 2)) / hImp(2);
% %             distSq = dT1 .* dT1 + dT2 .* dT2;
% %             k = exp(-0.5 * distSq);
% %             p = (1 / N) * (1 / (2 * pi * sqrt(h(1) * h(2)))) * sum(k');
% %             pImpTmp(i) = p;
% %         end
% %         pIsGenTmp = pGenTmp ./ (pGenTmp + pImpTmp);
% %         pIsGenTmp = pIsGenTmp - refP;
% %         dxy = [dx' dy'];
% %         posGrad = sum(dxy(pIsGenTmp>0,:)) / sum(pIsGenTmp>0);
% %         negGrad = sum(dxy(pIsGenTmp<0,:)) / sum(pIsGenTmp<0);
% %         if sum(pIsGenTmp>0) == 0
% %             posGrad = [0 0];
% %         end
% %         if sum(pIsGenTmp<0) == 0
% %             negGrad = [0 0];
% %         end
% %         grad = 0.5 * (posGrad - negGrad);
% %         actModeJ = actModeJ + grad;
% %         tmpTangentVector = [tmpTangentVector; actModeJ];
% %     end
% %     tangentVector = [tangentVector tmpTangentVector];
% % end
% 
% tangentVector = [];
% weightVector = [];
% weightVector2 = [];
% 
% stepWidth = 0.1;
% 
% for g = 1:size(genuineScores, 2)
%     actModeJ = genuineScores(:,g)';
%     
%     dT1 = (genuineScores(1, :) - actModeJ(1)) / hGen(1);
%     dT2 = (genuineScores(2, :) - actModeJ(2)) / hGen(2);
%     distSq = dT1.^2 + dT2.^2;
%     k = exp(-0.5 * distSq);
%     p = (1 / NGen) * (1 / (2 * pi * sqrt(hGen(1) * hGen(2)))) * sum(k');
%     pGenTmpR = p;
%     dT1 = (imposterScores(1, :) - actModeJ(1)) / hImp(1);
%     dT2 = (imposterScores(2, :) - actModeJ(2)) / hImp(2);
%     distSq = dT1 .* dT1 + dT2 .* dT2;
%     k = exp(-0.5 * distSq);
%     p = (1 / NImp) * (1 / (2 * pi * sqrt(hImp(1) * hImp(2)))) * sum(k');
%     refP = pGenTmpR / (p + pGenTmpR);
% 
%     alpha = (pi/16):(pi/16):(2*pi);
%     dx = cos(alpha) * stepWidth;
%     dy = -sin(alpha) * stepWidth;
%     pos = repmat(actModeJ, length(alpha), 1) + [dx' dy'];
%     pGenTmp = zeros(length(alpha), 1);
%     pImpTmp = zeros(length(alpha), 1);
%     for i = 1:size(pos, 1);
%         dT1 = (genuineScores(1, :) - pos(i, 1)) / hGen(1);
%         dT2 = (genuineScores(2, :) - pos(i, 2)) / hGen(2);
%         distSq = dT1.^2 + dT2.^2;
%         k = exp(-0.5 * distSq);
%         p = (1 / NGen) * (1 / (2 * pi * sqrt(hGen(1) * hGen(2)))) * sum(k');
%         pGenTmp(i) = p;
%         dT1 = (imposterScores(1, :) - pos(i, 1)) / hImp(1);
%         dT2 = (imposterScores(2, :) - pos(i, 2)) / hImp(2);
%         distSq = dT1 .* dT1 + dT2 .* dT2;
%         k = exp(-0.5 * distSq);
%         p = (1 / NImp) * (1 / (2 * pi * sqrt(hImp(1) * hImp(2)))) * sum(k');
%         pImpTmp(i) = p;
%     end
%     pIsGenTmp = pGenTmp ./ (pGenTmp + pImpTmp);
%     pIsGenTmpMR = pIsGenTmp - refP;
%     dxy = [dx' dy'];
%     posGrad = sum(dxy(pIsGenTmpMR>0,:)) / sum(pIsGenTmpMR>0);
%     negGrad = sum(dxy(pIsGenTmpMR<0,:)) / sum(pIsGenTmpMR<0);
%     if sum(pIsGenTmpMR>0) == 0
%         posGrad = [0 0];
%     end
%     if sum(pIsGenTmpMR<0) == 0
%         negGrad = [0 0];
%     end
%     grad = 0.5 * (posGrad - negGrad);
%     w = max(pIsGenTmp) - min(pIsGenTmp);
%         
%     tangentVector = [tangentVector; grad];
%     weightVector = [weightVector; [w w]];
%     weightVector2 = [weightVector2; [pGenTmpR pGenTmpR]];
% end
% 
% sortWeightVector2 = sort(weightVector2(:,1));
% proc95 = round(length(sortWeightVector2) * 0.05);
% thresh = sortWeightVector2(proc95, 1);
% weightVector2(weightVector2(:,1) < thresh, :) = repmat([0 0], sum(sortWeightVector2(:,1) < thresh), 1);
% weightVector2(weightVector2(:,1) >= thresh, :) = repmat([1 1], sum(sortWeightVector2(:,1) >= thresh), 1);
% 
% meanWeightedGradient = sum(tangentVector .* weightVector)
% meanWeighted95Gradient = sum(tangentVector .* weightVector .* weightVector2)
% meanGradient = sum(tangentVector)
% meanGradient95 = sum(tangentVector .* weightVector2)
% 
% within95Proz = sum(weightVector2(:, 1)) / length(weightVector2(:, 1)')
% 
% actModeG
% actModeI
% plot([actModeG(1), actModeI(1)], [actModeG(2), actModeI(2)], 'r-');
% 
% hold off
% 
% figure
% contour(T1, T2, pIsGen, (1:8)./(2:9))
% hold on
% plot([actModeG(1), actModeI(1)], [actModeG(2), actModeI(2)], 'k-');
% plot(iModePath(:,1)', iModePath(:,2)', 'r-');
% plot(gModePath(:,1)', gModePath(:,2)', 'g-');
% %plot(jModePath(:,1)', jModePath(:,2)', 'c-');
% % for i = 1:(size(gradientVector,2)/2)
% %     plot(gradientVector(:,2*i-1)', gradientVector(:,2*i)', 'm-');
% %     plot(gradientVector(1,2*i-1)', gradientVector(1,2*i)', 'm.');
% % end
% % for i = 1:(size(tangentVector,2)/2)
% %     plot(tangentVector(:,2*i-1)', tangentVector(:,2*i)', 'b-');
% %     plot(tangentVector(1,2*i-1)', tangentVector(1,2*i)', 'b.');
% % end
% plot([actModeG(1)-meanWeightedGradient(1)*10, actModeG(1)+meanWeightedGradient(1)*10], [actModeG(2)-meanWeightedGradient(2)*10, actModeG(2)+meanWeightedGradient(2)*10], 'b-');
% plot([actModeG(1)-meanGradient(1)*10, actModeG(1)+meanGradient(1)*10], [actModeG(2)-meanGradient(2)*10, actModeG(2)+meanGradient(2)*10], 'g-');
% plot([actModeG(1)-meanGradient95(1)*10, actModeG(1)+meanGradient95(1)*10], [actModeG(2)-meanGradient95(2)*10, actModeG(2)+meanGradient95(2)*10], 'c-');
% plot([actModeG(1)-meanWeighted95Gradient(1)*10, actModeG(1)+meanWeighted95Gradient(1)*10], [actModeG(2)-meanWeighted95Gradient(2)*10, actModeG(2)+meanWeighted95Gradient(2)*10], 'r-');
% hold off
% 
% figure
% contour(T1, T2, pIsGen, 1-((1:8)./(2:9)))
% hold on
% plot([actModeG(1), actModeI(1)], [actModeG(2), actModeI(2)], 'k-');
% plot(iModePath(:,1)', iModePath(:,2)', 'r-');
% plot(gModePath(:,1)', gModePath(:,2)', 'g-');
% %plot(jModePath(:,1)', jModePath(:,2)', 'c-');
% % for i = 1:(size(gradientVector,2)/2)
% %     plot(gradientVector(:,2*i-1)', gradientVector(:,2*i)', 'm-');
% %     plot(gradientVector(1,2*i-1)', gradientVector(1,2*i)', 'm.');
% % end
% % for i = 1:(size(tangentVector,2)/2)
% %     plot(tangentVector(:,2*i-1)', tangentVector(:,2*i)', 'b-');
% %     plot(tangentVector(1,2*i-1)', tangentVector(1,2*i)', 'b.');
% % end
% plot([actModeG(1)-meanWeightedGradient(1)*10, actModeG(1)+meanWeightedGradient(1)*10], [actModeG(2)-meanWeightedGradient(2)*10, actModeG(2)+meanWeightedGradient(2)*10], 'b-');
% plot([actModeG(1)-meanGradient(1)*10, actModeG(1)+meanGradient(1)*10], [actModeG(2)-meanGradient(2)*10, actModeG(2)+meanGradient(2)*10], 'g-');
% plot([actModeG(1)-meanGradient95(1)*10, actModeG(1)+meanGradient95(1)*10], [actModeG(2)-meanGradient95(2)*10, actModeG(2)+meanGradient95(2)*10], 'c-');
% plot([actModeG(1)-meanWeighted95Gradient(1)*10, actModeG(1)+meanWeighted95Gradient(1)*10], [actModeG(2)-meanWeighted95Gradient(2)*10, actModeG(2)+meanWeighted95Gradient(2)*10], 'r-');
% hold off
% 
% figure
% plot3(genuineScores(1, weightVector2(:, 1) == 1), genuineScores(2, weightVector2(:, 1) == 1), weightVector(weightVector2(:,1) == 1, 1)', 'g.');
% hold on
% plot3(genuineScores(1, weightVector2(:, 1) == 0), genuineScores(2, weightVector2(:, 1) == 0), weightVector(weightVector2(:,1) == 0, 1)', 'r.');
% hold off

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load tables
dataset = 'VIPeR';
expNum = '001';
Tables = util.loadFromFile(['../results/' dataset '_Exp' expNum '/matching/Tables_' dataset '_x' expNum '.mat'],['Tables_' dataset]);%,'legendTables')

%%
[legendEntrys,pHandles,order] = visualization.plotEvaluation(dataset,expNum,'LikelihoodRatio');

%%
hold on;
RankTable = zeros(length(Tables),5);
for i=1:length(Tables)
    cmc = util.benchmarking('datasets/VIPeRa',50,316,Tables{i},'.bmp',0,1);
%     cmc = util.benchmarking('datasets/i-LIDS_Pedestrian/Persons',100,50,Tables{i},'.jpg',0);
    RankTable(i,:) = cmc([1 5 10 20 50]);
    hSingle = plot(100*cmc,'Color',[168 168 168]/255);
end
axis([1 50 0 100])

%%
legendEntrys = [legendEntrys,'Single features'];    
legend([pHandles, hSingle],legendEntrys,'Location','SouthEast')
title(['Cumulative Match Characteristic - ' dataset ' (p=316)']);

%%
xSize = 20; ySize = 19;
% set(gcf,'PaperPositionMode','auto')
paperSizeX = 18;
paperSizeY = 18;
set(gcf,'PaperSize',[paperSizeX paperSizeY]);
xLeft = (paperSizeX-xSize)/2; yTop = (paperSizeY-ySize)/2;
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
% Screen position and size
X = 1100;
Y = 100;
set(gcf,'Position',[X Y xSize*50 ySize*50])
% print(['../images/Weighting_vs_SingleFeatures.pdf'],'-dpdf');
% figure,imshow(imread('../images/LinearRegression.png'))

% %%
% [~,idx] = sort(RankTable(:,4),'descend');
% 
% %%
% disp('Top 5 Features by Rank 20')
% for i=1:5
%     disp([legendTables{idx(i)} ': ' num2str(RankTable(idx(i),:)*100,' %4.2f ')])
% end
function InformationFusion_with_MetricLearning(Tables_KISSME,Tables_kLFDA)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% KISSME
% kissmeFAR = load('../results/VIPeR_Exp001/transformedTablesML/LearnTest_VIPeR_FAR_KISSME.mat','cmc');
% kissmeConcat = load('+visualization/CMC_KISSME_ConcatFeat.mat','cmc');
% kissmePure = load('../results/VIPeR_Exp001/transformedTables_KISSME_pure/LearnTest_VIPeR_FAR_KISSME_pure.mat','cmc');
% % Tables_KISSME = util.loadFromFile('../results/VIPeR_Exp001/matching/Tables_VIPeR_KISSME.mat','Tables_VIPeR');
% 
% partitionTest = util.loadFromFile('../results/VIPeR_Exp001/transformedTablesML/LearnTest_VIPeR_FAR_KISSME.mat','partitionTest');
% 
% cmcKISSME = zeros(length(Tables_KISSME),316);
% for i=1:length(Tables_KISSME)
%     cmcKISSME(i,:) = util.benchmarking2('datasets/VIPeRa',partitionTest,Tables_KISSME(:,i),'.bmp');
% end

%% Kernel LFDA
klfdaFAR = load('../results/VIPeR_Exp001/transformedTablesML/_LearnTest_VIPeR_FAR_kLFDA.mat','cmc');
klfdaConcat = load('+visualization/CMC_kLFDA_ConcatFeat.mat','cmc');
% Tables_kLFDA = util.loadFromFile('../results/VIPeR_Exp001/matching/Tables_VIPeR_kLFDA.mat','Tables_VIPeR');

partitionTest = util.loadFromFile('../results/VIPeR_Exp001/transformedTablesML/LearnTest_VIPeR_FAR_kLFDA.mat','partitionTest');

cmckLFDA = zeros(length(Tables_kLFDA),316);
for i=1:length(Tables_KISSME)
    cmckLFDA(i,:) = util.benchmarking2('datasets/VIPeRa',partitionTest,Tables_kLFDA(:,i),'.bmp');
end

% %%
% figure
% hold on;
% % features not suited for metric learning
% hAdditional = plot(100*cmcKISSME(9,:),'-','Color',[0.7 0.7 0.7],'LineWidth',2);
% for i=10:12
%     plot(100*cmcKISSME(i,:),'-','Color',[0.7 0.7 0.7],'LineWidth',2)
% end
% % features from metric learning
% hMetric = plot(100*cmcKISSME(1,:),'--','Color',[0.2 0.2 0.8],'LineWidth',2);
% for i=2:8
%     plot(100*cmcKISSME(i,:),'--','Color',[0.2 0.2 0.8],'LineWidth',2)
% end
% % only metric learning fused
% hFusionOnlyMetric = plot(100*kissmePure.cmc,'--','Color',[0.8 0.0 0.0],'LineWidth',2);
% % KISSME on concatenated feature vector
% hConcat = plot(100*kissmeConcat.cmc,'-','Color',[0.8 0.0 0.0],'LineWidth',2);
% % proposed method
% hProposed = plot(100*kissmeFAR.cmc,'Color',[0 0.8 0],'LineWidth',2);
% 
% xlabel('Rank');
% ylabel('Recognition Percentage');
% title('Cumulative Match Characteristic VIPeR (p=316)');
% legend([hProposed hFusionOnlyMetric hConcat hMetric hAdditional],{'Proposed Method','KISSME fusion','KISSME on concatenated feature','Features after KISSME','Additional Features'},'Location','SouthEast');
% axis([1 50 0 100])
% grid on
% 
% %%
% xSize = 20; ySize = 19;
% xScale = 50; yScale = 50;
% % set(gcf,'PaperPositionMode','auto')
% paperSizeX = 18;
% paperSizeY = 18;
% set(gcf,'PaperSize',[paperSizeX paperSizeY]);
% xLeft = (paperSizeX-xSize)/2; yTop = (paperSizeY-ySize)/2;
% set(gcf,'PaperUnits','centimeters');
% set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
% % Screen position and size
% X = 1100;
% Y = 100;
% set(gcf,'Position',[X Y xSize*xScale ySize*yScale])
% % print(['../images/KISSME_FAR_Fusion.pdf'],'-dpdf');

%%
figure
hProposed = plot(100*klfdaFAR.cmc,'Color',[0 0.8 0],'LineWidth',2);
hold on;
hConcat = plot(100*klfdaConcat.cmc,'Color',[0.8 0.0 0.0],'LineWidth',2);
% features not suited for metric learning
hAdditional = plot(100*cmckLFDA(9,:),'-','Color',[0.7 0.7 0.7],'LineWidth',2);
for i=10:12
    plot(100*cmckLFDA(i,:),'-','Color',[0.7 0.7 0.7],'LineWidth',2)
end
% features from metric learning
hMetric = plot(100*cmckLFDA(1,:),'--','Color',[0.2 0.2 0.8],'LineWidth',2);
for i=2:8
    plot(100*cmckLFDA(i,:),'--','Color',[0.2 0.2 0.8],'LineWidth',2)
end

xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic VIPeR (p=316)');
legend([hProposed hConcat hMetric hAdditional],{'Proposed Method','KISSME on concatenated features','Features after KISSME','Additional Features'},'Location','SouthEast');
axis([1 50 0 100])
grid on

%%
xSize = 20; ySize = 19;
xScale = 50; yScale = 50;
% set(gcf,'PaperPositionMode','auto')
paperSizeX = 18;
paperSizeY = 18;
set(gcf,'PaperSize',[paperSizeX paperSizeY]);
xLeft = (paperSizeX-xSize)/2; yTop = (paperSizeY-ySize)/2;
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
% Screen position and size
X = 1100;
Y = 100;
set(gcf,'Position',[X Y xSize*xScale ySize*yScale])
% print(['../images/kLFDA_FAR_Fusion.pdf'],'-dpdf');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialization
visualization.Init;

%% Get genuines and imposters
genuineMask = mask(partitionTest,partitionTest);
realGenuines = genuineMask;
realGenuines(eye(size(genuineMask,1)) == 1) = 0;

genuinesMSCR = Tables_Trans{1}(realGenuines);
genuinesMSCR = genuinesMSCR(:)';
impostersMSCR = Tables_Trans{1}(~genuineMask);
impostersMSCR = impostersMSCR(:)';

genuinesWCH = Tables_Trans{2}(realGenuines);
genuinesWCH = genuinesWCH(:)';
impostersWCH = Tables_Trans{2}(~genuineMask);
impostersWCH = impostersWCH(:)';

%% draw joint genuine-imposter-plot
visualization.jointGenuineImposterPlot([genuinesMSCR; genuinesWCH],[impostersMSCR; impostersWCH], [0, 0], [1, 1], 75,weightsPLR{1});
view(110,30)
set(gcf,'color','white')

%%
% set(gcf,'PaperUnits','centimeters')
% % Paper position and size
xSize = 24; ySize = 16;
xLeft = (21-xSize)/2; yTop = (30-ySize)/2;
% set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
set(gcf,'PaperPositionMode','auto')
% % Screen position and size
X = 100;
Y = 100;
set(gcf,'Position',[X Y xSize*50 ySize*50])
print([imageFolder 'JointGenImp3D_2.png'],'-dpng');
figure,imshow(imread([imageFolder 'JointGenImp3D_2.png']))
% print([imageFolder 'JointGenImp3D_final.png'],'-dpng');
% figure,imshow(imread([imageFolder 'JointGenImp3D_final.png']))
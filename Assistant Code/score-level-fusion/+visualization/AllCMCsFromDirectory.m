%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% Copyright (c) 2015,                                                     %
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% All rights reserved.                                                    %
%                                                                         %
% Copying, resale, or redistribution, with or without modification, is    %
% strictly prohibited.                                                    %
%                                                                         %
% The academic use of this MATLAB code is free of charge. Any commercial  %
% distribution or act related to the commercial usage of this source code %
% is strictly prohibited. The distribution of this source code to any     %
% parties that have not read and agreed to the terms and conditions of    %
% usage is strictly prohibited.                                           %
%                                                                         %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     %
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       %
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   %
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    %
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   %
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE    %
% POSSIBILITY OF SUCH DAMAGE.                                             %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% This code is is publicly available at                                   %
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/      %
% for academic use only.                                                  %
%                                                                         %
% When you use this code, you must cite the following paper:              %
%                                                                         %
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:   %
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based  %
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),  %
% Killarney, Ireland, pp. 469-476, IEEE 2015.                             %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% Author: A. Vorndran                                                     %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

root_dir = '../results/VIPeR_Exp001/transformedTablesML/';
fnames = dir([root_dir 'LearnTest_*.mat']);
fnames = {fnames(:).name};

%%
figure
hold on
legendEntries = {};
C = colormap(hsv(length(fnames)));
% C = colormap(lines(2));
for i=1:length(fnames)
    result = load([root_dir fnames{i}],'cmc');
    plot(100*result.cmc,'Color',C(i,:),'LineWidth',2);
%     if strfind(fnames{i},'kLFDA')
%         plot(100*result.cmc,'Color',C(1,:));
%     else
%         plot(100*result.cmc,'Color',C(2,:));
%     end
    legendEntries = [legendEntries fnames{i}(17:end-4)];
end

h = legend(legendEntries);
% Interpreter_type: 'latex' | {'tex'} | 'none'
set(h, 'Interpreter', 'none'); 
axis([1 50 30 100])
grid on
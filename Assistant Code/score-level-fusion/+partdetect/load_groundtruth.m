function GT = load_groundtruth(train,pdims)
%
% Load the ground truth annotation labels.
%
% Input is contained in the file train.Labels, which should contain a
% matrix ptsAll of size 14x2xnum, where 14 are the body points (see
% following explanation of the PARSE database) and num is the number of
% images in the dataset.
%
% Output is:
% GT.Parts - number of parts
% GT.Names - names of parts
% GT.X - x coordinates of each part
% GT.Y - y coordinates of each part
% GT.L - length/height of each part
% GT.W - width of each part
% GT.R - rotation angle (in radiants) of each part, with 0 = vertical
% GT.FS - foreshortening wrt canonical size
% GT.S - scale of the entire sample wrt average size
%
% In the PARSE database, the ground truth data is provided as the positions
% of certain body points:
% 1. 'lank' = left ankle
% 2. 'lkne' = left knee
% 3. 'lhip' = left hip
% 4. 'rhip' = right hip
% 5. 'rkne' = right knee
% 6. 'rank' = right ankle
% 7. 'lwr'  = left wrist
% 8. 'lelb' = left elbow
% 9. 'lsho' = left shoulder
% 10.'rsho' = right shoulder
% 11.'relb' = right elbow
% 12.'rwr'  = right wrist
% 13.'hbot' = head bottom
% 14.'htop' = head top
%
% From this information, we need to extract a body configuration of 11
% parts with a rectangular shape.
% 
% The configuration has these 10 body parts: torso head 2xarms 2xforearms
% 2xthighs 2xlegs, numbered as follows:
%
%      3
%   4  2  6
%  5   1   7
%     8 10
%    9   11
%
% The length of each part is given by the difference between its
% extremities, but the width is left to choice, except for the shoulders
% parts which is the opposite.
%
% 24 Feb 2013: Decided to change the way torso/shoulders/head are
%              calculated so that it is possible to remove the shoulders
%              without leaving a hole between torso and head.
%              Added estimation of scale based on foreshortening wrt
%              canonical size.

%% Load input, setup output
% load the labels
load([train.Dir '/' train.Labels]);
num = size(ptsAll,3);

% setup output
GT.Parts = 11;
GT.Names = {'Torso','Shoulders','Head','LArm','LForearm','RArm',...
  'RForearm','LThigh','LLeg','RThigh','RLeg'};
GT.X = zeros(num,GT.Parts); % x position
GT.Y = zeros(num,GT.Parts); % y position
GT.L = zeros(num,GT.Parts); % length/height of the part
GT.W = zeros(num,GT.Parts); % width of the part
GT.R = zeros(num,GT.Parts); % rot = 0 if the part is upright vertical
GT.FS = zeros(num,GT.Parts); % foreshortening of each part
% GT.S = zeros(num,1); % scale of the entire training case

% add these body points to the database:
% 15.'tbot' = torso bottom is between lhip and rhip
% 16.'ttop' = torso top is between lsho and rsho
ptsAll(15,:,:) = 0.5 * (ptsAll(3,:,:) + ptsAll(4,:,:));
ptsAll(16,:,:) = 0.5 * (ptsAll(9,:,:) + ptsAll(10,:,:));

%% Torso
% torso goes from tbot to ttop
dx = ptsAll(16,1,:) - ptsAll(15,1,:);
dy = ptsAll(16,2,:) - ptsAll(15,2,:);
GT.X(:,1) = ptsAll(15,1,:) + 0.5 * dx;
GT.Y(:,1) = ptsAll(15,2,:) + 0.5 * dy;
% length and rotation, calculated to have 0 = vertical
GT.L(:,1) = sqrt( dx.^2 + dy.^2 );
GT.W(:,1) = pdims(1,1);
GT.R(:,1) = atan2(dx,-dy);
% calculate foreshortening
GT.FS(:,1) = GT.L(:,1) / pdims(1,2);

%% Head
% middle is between htop and hbot. Head height is extended by 20% to grab
% some context over the head and part of the neck.
GT.X(:,3) = 0.5 * ptsAll(13,1,:) + 0.5 * ptsAll(14,1,:);
GT.Y(:,3) = 0.5 * ptsAll(13,2,:) + 0.5 * ptsAll(14,2,:);
% length and rotation, calculated to have 0 = vertical
dx = 1.2* (ptsAll(14,1,:) - ptsAll(13,1,:));
dy = 1.2* (ptsAll(13,2,:) - ptsAll(14,2,:));
GT.L(:,3) = sqrt( dx.^2 + dy.^2 );
GT.W(:,3) = pdims(3,1);
GT.R(:,3) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,3) = GT.L(:,3) / pdims(3,2);

%% Shoulders
% the shoulders is a horizontal part above the torso connecting the two
% shoulder points and raising orthogonally up and down
GT.X(:,2) = ptsAll(16,1,:);
GT.Y(:,2) = ptsAll(16,2,:);
dx = ptsAll(10,1,:) - ptsAll(9,1,:);
dy = ptsAll(10,2,:) - ptsAll(9,2,:);
GT.L(:,2) = pdims(2,2);
GT.W(:,2) = sqrt( dx.^2 + dy.^2 );

% orientation of the part is based on the neck direction
width = sqrt( dx.^2 + dy.^2 );
neckx = ptsAll(16,1,:) + 7 * dy ./ width;
necky = ptsAll(16,2,:) - 7 * dx ./ width;
dx = 2*(neckx - ptsAll(16,1,:));
dy = 2*(ptsAll(16,2,:) - necky);
GT.R(:,2) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,2) = GT.W(:,2) / pdims(2,1);

%% Arms
% middle is between sho and elb
GT.X(:,4) = 0.5 * (ptsAll(8,1,:) + ptsAll(9,1,:));
GT.Y(:,4) = 0.5 * (ptsAll(8,2,:) + ptsAll(9,2,:));
GT.X(:,6) = 0.5 * (ptsAll(10,1,:) + ptsAll(11,1,:));
GT.Y(:,6) = 0.5 * (ptsAll(10,2,:) + ptsAll(11,2,:));
% length and rotation, calculated to have 0 = vertical
dx = ptsAll(9,1,:) - ptsAll(8,1,:);
dy = ptsAll(8,2,:) - ptsAll(9,2,:);
GT.L(:,4) = sqrt( dx.^2 + dy.^2 );
GT.W(:,4) = pdims(4,1);
GT.R(:,4) = atan2(dx,dy);
dx = ptsAll(10,1,:) - ptsAll(11,1,:);
dy = ptsAll(11,2,:) - ptsAll(10,2,:);
GT.L(:,6) = sqrt( dx.^2 + dy.^2 );
GT.W(:,6) = pdims(6,1);
GT.R(:,6) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,4) = GT.L(:,4) / pdims(4,2);
GT.FS(:,6) = GT.L(:,6) / pdims(6,2);

%% Forearms
% middle is between elb and wr
GT.X(:,5) = 0.5 * (ptsAll(7,1,:) + ptsAll(8,1,:));
GT.Y(:,5) = 0.5 * (ptsAll(7,2,:) + ptsAll(8,2,:));
GT.X(:,7) = 0.5 * (ptsAll(11,1,:) + ptsAll(12,1,:));
GT.Y(:,7) = 0.5 * (ptsAll(11,2,:) + ptsAll(12,2,:));
% length and rotation, calculated to have 0 = vertical
dx = ptsAll(8,1,:) - ptsAll(7,1,:);
dy = ptsAll(7,2,:) - ptsAll(8,2,:);
GT.L(:,5) = sqrt( dx.^2 + dy.^2 );
GT.W(:,5) = pdims(5,1);
GT.R(:,5) = atan2(dx,dy);
dx = ptsAll(11,1,:) - ptsAll(12,1,:);
dy = ptsAll(12,2,:) - ptsAll(11,2,:);
GT.L(:,7) = sqrt( dx.^2 + dy.^2 );
GT.W(:,7) = pdims(7,1);
GT.R(:,7) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,5) = GT.L(:,5) / pdims(5,2);
GT.FS(:,7) = GT.L(:,7) / pdims(7,2);

%% Thighs
% middle is between hip and kne
GT.X(:,8) = 0.5 * (ptsAll(2,1,:) + ptsAll(3,1,:));
GT.Y(:,8) = 0.5 * (ptsAll(2,2,:) + ptsAll(3,2,:));
GT.X(:,10) = 0.5 * (ptsAll(4,1,:) + ptsAll(5,1,:));
GT.Y(:,10) = 0.5 * (ptsAll(4,2,:) + ptsAll(5,2,:));
% length and rotation, calculated to have 0 = vertical
dx = ptsAll(3,1,:) - ptsAll(2,1,:);
dy = ptsAll(2,2,:) - ptsAll(3,2,:);
GT.L(:,8) = sqrt( dx.^2 + dy.^2 );
GT.W(:,8) = pdims(8,1);
GT.R(:,8) = atan2(dx,dy);
dx = ptsAll(4,1,:) - ptsAll(5,1,:);
dy = ptsAll(5,2,:) - ptsAll(4,2,:);
GT.L(:,10) = sqrt( dx.^2 + dy.^2 );
GT.W(:,10) = pdims(10,1);
GT.R(:,10) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,8) = GT.L(:,8) / pdims(8,2);
GT.FS(:,10) = GT.L(:,10) / pdims(10,2);

%% Legs
% middle is between hip and kne
GT.X(:,9) = 0.5 * (ptsAll(1,1,:) + ptsAll(2,1,:));
GT.Y(:,9) = 0.5 * (ptsAll(1,2,:) + ptsAll(2,2,:));
GT.X(:,11) = 0.5 * (ptsAll(5,1,:) + ptsAll(6,1,:));
GT.Y(:,11) = 0.5 * (ptsAll(5,2,:) + ptsAll(6,2,:));
% length and rotation, calculated to have 0 = vertical
dx = ptsAll(2,1,:) - ptsAll(1,1,:);
dy = ptsAll(1,2,:) - ptsAll(2,2,:);
GT.L(:,9) = sqrt( dx.^2 + dy.^2 );
GT.W(:,9) = pdims(9,1);
GT.R(:,9) = atan2(dx,dy);
dx = ptsAll(5,1,:) - ptsAll(6,1,:);
dy = ptsAll(6,2,:) - ptsAll(5,2,:);
GT.L(:,11) = sqrt( dx.^2 + dy.^2 );
GT.W(:,11) = pdims(11,1);
GT.R(:,11) = atan2(dx,dy);
% calculate foreshortening
GT.FS(:,9) = GT.L(:,9) / pdims(9,2);
GT.FS(:,11) = GT.L(:,11) / pdims(11,2);


%% Estimate the global scale
% if all parts are foreshortened by a certain amount, it is conceivable
% that the entire image is a little out of scale, so the fixed dimension of
% the parts should be adjusted.

% calculate average length of all parts
avglen = mean(GT.L);

% average left/right parts
avglen([4 6]) = (avglen(4) + avglen(6))/2;
avglen([5 7]) = (avglen(5) + avglen(7))/2;
avglen([8 10]) = (avglen(8) + avglen(10))/2;
avglen([9 11]) = (avglen(9) + avglen(11))/2;

% calculate foreshortening wrt the average size instead of the canonical
% size, to avoid bias due to the canonical size being defined in terms of
% full cells
FS = GT.L ./ avglen(ones(num,1),:);

% calculate the scale of the entire training case as average foreshortening
GT.S = mean(FS,2);

% adjust the fixed part dimensions to account for global scale factor
for pp = [1 3 4 5 6 7 8 9 10 11]
  GT.W(:,pp) = GT.W(:,pp) .* GT.S;
end
GT.L(:,2) = GT.L(:,2) .* GT.S;

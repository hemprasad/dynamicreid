function montage_maps(M,parts)

% prepare the montage image
cmap = [0 0 0; jet(255)];
[H,~,numr,~] = size(M);

vbar = zeros(H,1);
MI = scaleImage(M(:,:,1,parts(1)),0,1);
for ridx = 2:numr
  MI = [MI vbar scaleImage(M(:,:,ridx,parts(1)),0,1)];
end
hbar = zeros(1,size(MI,2));

% finish the montage image
for pidx = parts(2:end)
  T = scaleImage(M(:,:,1,pidx),0,1);
  for ridx = 2:numr
    T = [T vbar scaleImage(M(:,:,ridx,pidx),0,1)];
  end
  MI = [MI; hbar; T];
end
imshow(MI,cmap);


function I = scaleImage(A,minv,maxv)

A(A < minv) = minv;
A(A > maxv) = maxv;
rangev = maxv - minv;
I = uint8( (A - minv) * 255/rangev );
I(I == 0) = 1;

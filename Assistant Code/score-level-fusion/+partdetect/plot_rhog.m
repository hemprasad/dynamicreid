function IMG = plot_rhog(H,par,csize)
% This function assumes each feature vector is a contrast normalized vector
% concatenating 2x2 cells for each block

[veclen hei wid] = size(H);
BH = reshape(H,[veclen/4 4 hei wid]);
numbins = par.Orientations;

bandwidth = 180 / numbins;
bins = 0:bandwidth:180;

if nargin < 3, csize = 9; end
cmid = ceil(csize/2);
aorsize = [csize csize numbins];

% create each bin archetype edge orientation (not gradient!)
aor = zeros(csize,csize,numbins);
for k = 1:numbins
  if bins(k) == 0
    aor(:,cmid,k) = 1;
  elseif bins(k) == 90
    aor(cmid,:,k) = 1;
  else
    m = tand(bins(k)+90);
    if abs(m) <= 1
      cstart = round((cmid-1)*sqrt(1/(m^2+1)));
      x = -cstart:cstart;
      y = round(cmid + m*x);
      aor(sub2ind(aorsize,y,x+cmid,k*ones(1,length(x)))) = 1;
    else
      cstart = round((cmid-1)*sqrt(m^2/(m^2+1)));
      y = -cstart:cstart;
      x = round(cmid + y/m);
      aor(sub2ind(aorsize,y+cmid,x,k*ones(1,length(x)))) = 1;
    end
  end  
end

% figure;
% for k = 1:numbins
%   bigsubplot(1,numbins,1,k);
%   imagesc(aor(:,:,k));
% end

% create the composite image
iheight = hei*csize*2;
iwidth = wid*csize*2;
IMG = zeros(iheight,iwidth);

CH = permute(BH,[3 4 1 2]);

for row = 1:hei
  for col = 1:wid
    r1 = (row-1)*csize*2; c1 = (col-1)*csize*2;
    cellhist = aor .* CH(row*ones(csize,1,1),col*ones(1,csize,1),:,1);
    IMG(r1+1:r1+csize,c1+1:c1+csize) = max(cellhist,[],3);
    cellhist = aor .* CH(row*ones(csize,1,1),col*ones(1,csize,1),:,2);
    IMG(r1+1+csize:r1+csize*2,c1+1:c1+csize) = max(cellhist,[],3);
    cellhist = aor .* CH(row*ones(csize,1,1),col*ones(1,csize,1),:,3);
    IMG(r1+1:r1+csize,c1+csize+1:c1+csize*2) = max(cellhist,[],3);
    cellhist = aor .* CH(row*ones(csize,1,1),col*ones(1,csize,1),:,4);
    IMG(r1+csize+1:r1+csize*2,c1+csize+1:c1+csize*2) = max(cellhist,[],3);
  end
end

imagesc(IMG);

function F = extract_hog(I,conf,cells,par)
%
% Extract the HOG feature vector F from the given image I for a body part
% of configuration conf, dimensions dims and size in blocks
%
% This version uses extended blocks, meaning that the border cells in a ROI
% are centered at the boundary pixels and extend for half cell beyond the
% ROI.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% PARTDETECT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% PARTDETECT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PARTDETECT.  If not, see <http://www.gnu.org/licenses/>.

import partdetect.*;

%% Preliminaries
% image size
[ihei,iwid,~] = size(I);

% cell size (must be an odd number) and half size
csize = par.CellSize;
chalf = (csize - 1) / 2;

% resize the cell size based on scale
scale = conf(6);
chalf = round(chalf * scale);
csize = 2 * chalf + 1;

% configuration of the body part
xc = conf(1);
yc = conf(2);
rot = conf(5) * 180/pi;
height = conf(3);
width = conf(4);

% configuration of cells and blocks
blocks = cells;
cells = blocks + 1;
numbins = par.Orientations;
numcells = prod(cells);

%% Retrieve the body part image
% calculate the four centers of the pixels inside the corners of the
% bounding box
xrect = [-1 1 1 -1] * (width-1)/2;
yrect = [-1 -1 1 1] * (height-1)/2;
xy = util.rototrasl([xrect;yrect],xc,yc,rot);

% calculate the bounding box of the part, with half cell padding
xymin = round(min(xy,[],2)) - chalf;
xymax = round(max(xy,[],2)) + chalf;

% check if we need padding before extraction
pad = max([1-xymin xymax-[iwid;ihei] [0;0]],[],2);
if any(pad > 0)
  I = padarray(I,[pad(2) pad(1) 0],'symmetric','both');
  xymin = xymin + pad;
  xymax = xymax + pad;
  xyadj = pad + 1;
else
  xyadj = [1; 1];
end

% extract the bounding box
B = I(xymin(2):xymax(2),xymin(1):xymax(1),:);

% if DEBUG
%   figure(101);clf;
%   bigsubplot(1,2,1,1);imagesc(I);axis equal off;hold on;
%   plot([xy(1,:) xy(1,1)],[xy(2,:) xy(2,1)],'r-');
%   bigsubplot(1,2,1,2);imagesc(B);axis equal off;
% end


%% Retrieve the bin-rotated HOG integral image
% calculate the HOG integral image
IH = calc_inthog(B,par);

% quantize the rotation of the part into the given orientations to estimate
% the shift needed in the gradient histograms
rstep = 180 / numbins;
rshift = -round(rot / rstep);

% shift the gradient histograms
H = circshift(IH,[0 0 rshift]);

% if DEBUG
%   figure(102);clf;
%   for rr = 1:numbins
%     temp = IH(1:end-1,1:end-1,rr) + IH(2:end,2:end,rr) ...
%          - IH(2:end,1:end-1,rr) - IH(1:end-1,2:end,rr);
%     bigsubplot(2,numbins,1,rr);imagesc(temp);axis equal off;
%     temp = H(1:end-1,1:end-1,rr) + H(2:end,2:end,rr) ...
%          - H(2:end,1:end-1,rr) - H(1:end-1,2:end,rr);
%     bigsubplot(2,numbins,2,rr);imagesc(temp);axis equal off;
%   end
% end


%% Retrieve the vectors for each cell
% calculate the coordinates of the meshgrid of cell centers wrt the middle
% of the body part
xp = linspace(1,width,cells(1)) - (width-1)/2 - 1;
yp = linspace(1,height,cells(2)) - (height-1)/2 - 1;
[XP,YP] = meshgrid(xp,yp);
XYP = util.rototrasl([XP(:)'; YP(:)'],xc,yc,rot);

% if DEBUG
%   figure(101);
%   bigsubplot(1,2,1,1);hold on;
%   scatter(XYP(1,:),XYP(2,:),'g+');
% end

% adjust the meshgrid to the coordinates of the roi
XYP(1,:) = round(XYP(1,:)) - xymin(1) + xyadj(1);
XYP(2,:) = round(XYP(2,:)) - xymin(2) + xyadj(2);

% if DEBUG
%   figure(101);
%   bigsubplot(1,2,1,2);hold on;
%   scatter(XYP(1,:),XYP(2,:),'g+');
%   for bb = 1:size(XYP,2)
%     row = XYP(2,bb); col = XYP(1,bb);
%     ycell = [row-chalf row-chalf row+chalf+1 row+chalf+1 row-chalf] -0.5;
%     xcell = [col-chalf col+chalf+1 col+chalf+1 col-chalf col-chalf] -0.5;
%     plot(xcell,ycell,'b-');
%   end
% end

% calculate the sums of gradient histograms using the integral image trick
C = zeros(numbins,numcells,'single');
r1 = XYP(2,:) - chalf; r2 = r1 + csize;
c1 = XYP(1,:) - chalf; c2 = c1 + csize;
for bb = 1:numcells
  C(:,bb) = H(r2(bb),c2(bb),:) + H(r1(bb),c1(bb),:) ...
          - H(r1(bb),c2(bb),:) - H(r2(bb),c1(bb),:);
end

% if DEBUG
%   CH = reshape(C',[cells(2) cells(1) 18]);
%   figure(103);clf;bigsubplot(1,2,1,1);
%   Plot_HOG(CH,par);axis equal off;colormap gray;
% end

%% Retrieve the vectors for each block
% calculate the features for each 2x2 block of cells, normalize the vectors
% by using L1-sqrt. Sometimes the values calculated from the integral image
% will be negative because of rounding errors.
R = zeros(numbins*4,blocks(2),blocks(1),'single');

if strcmp(par.BlockNorm,'L1-sqrt')
  for row = 1:blocks(2)
    for col = 1:blocks(1)
      bb = ([col col col+1 col+1] - 1) * cells(2) + [row row+1 row row+1];
      vset = abs(C(:,bb));
      R(:,row,col) = sqrt( vset(:) / (sum(vset(:)) + par.Epsilon) );
    end
  end
elseif strcmp(par.BlockNorm,'L2-hys')
  % L2-Hys normalization
  for row = 1:blocks(2)
    for col = 1:blocks(1)
      bb = ([col col col+1 col+1] - 1) * cells(2) + [row row+1 row row+1];
      vset = abs(C(:,bb));
      vnorm = sqrt(sum(vset(:).^2) + par.Epsilon);
      R(:,row,col) = min(vset(:) / vnorm,par.Cutoff);
    end
  end
end

% if DEBUG
%   figure(103);bigsubplot(1,2,1,2);
%   Plot_RHOG(R,par);axis equal off;colormap gray;
% end

%% Return the complete HOG feature vector
F = R(:);

% if DEBUG
%   TH = Calc_Integral_HOG(I,par);
%   TH = circshift(TH,[0 0 rshift]);
% 
%   figure(104);clf;
%   stemp = 0;
%   for rr = 1:numbins
%     temp = TH(1:end-1,1:end-1,rr) + TH(2:end,2:end,rr) ...
%          - TH(2:end,1:end-1,rr) - TH(1:end-1,2:end,rr);
%     bigsubplot(1,numbins,1,rr);imagesc(temp);axis equal off;
%     stemp = stemp + temp;
%   end
%   figure(105);imagesc(stemp);axis equal off;
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOOTNOTES
%
% Feb 24 - Found major error, the circshift was operating at reverse.
%

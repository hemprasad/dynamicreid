function MAP = detect_parts(I,conf)
%
% This function performs body part detections using the HOG features and
% the LDA classifier.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% PARTDETECT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% PARTDETECT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PARTDETECT.  If not, see <http://www.gnu.org/licenses/>.

import partdetect.*;

%% Preliminaries
% retrieve some info
[imheight,imwidth,~] = size(I); % image size
Rots = conf.parHOG.Rots;        % rotations to detect
nRots = conf.parHOG.nRots;      % number of rotations
nParts = length(conf.prior.names); % number of parts
nBins = conf.parHOG.Orientations;

csize = conf.parHOG.CellSize; % HOG cell size
chalf = (csize - 1) / 2;

% resize the cell size based on scale
scale = conf.parHOG.Scale;
chalf = round(chalf * scale);
csize = 2 * chalf + 1;

% resize the part dimensions based on scale
part_dims = round(conf.part_dims * scale);

% setup the sampling grid locations
gstep = conf.parHOG.WindowStep;
Gx = 1:gstep:imwidth;
Gy = 1:gstep:imheight;
grwidth = length(Gx);
grheight = length(Gy);
normeps = conf.parHOG.Epsilon;
normcutoff = conf.parHOG.Cutoff;

%% Pad the image
% calculate the biggest part diagonal and use it to pad the image
pad = ceil( 0.5*sqrt(sum(max(part_dims).^2)) ) + chalf;

% pad the image by replicating
B = padarray(I,[pad pad],'both','symmetric');


%% Retrieve the bin-rotated HOG integral image
% calculate the HOG integral image
IH = permute(calc_inthog(B,conf.parHOG),[3 1 2]);

% quantize the rotation of the part into the given orientations to estimate
% the shift needed in the gradient histograms
rstep = 180 / nBins;
rshift = round(Rots ./ rstep);

% shift the gradient histograms
H = zeros([size(IH) nRots],'single');
for rr = 1:nRots
  if rshift(rr)
    H(:,:,:,rr) = circshift(IH,[-rshift(rr) 0 0]);
  else
    H(:,:,:,rr) = IH;
  end
end


%% Detect each part
% prepare output storage for the detection maps
MAP = zeros(imheight,imwidth,nRots,nParts);
for pp = 1:nParts
  % check body part
  width = part_dims(pp,1);
  height = part_dims(pp,2);
  
  % size of cells and blocks
  blocks = conf.part_cells(pp,:);
  cells = blocks + 1;
  numcells = prod(cells);
  numblocks = prod(blocks);

  
  %% Retrieve the vectors for each cell
  % for each rotation angle to evaluate, get the cells integrals
  C = zeros(nBins,numcells,grheight,grwidth,nRots,'single');
  xp = linspace(1,width,blocks(1)+1) - (width-1)/2 - 1;
  yp = linspace(1,height,blocks(2)+1) - (height-1)/2 - 1;
  [XP,YP] = meshgrid(xp,yp);
  for rr = 1:nRots
    % calculate the coordinates of the meshgrid of cell centers wrt the first
    % detection window centered at pad+1
    if Rots(rr)
      XYP = util.rototrasl([XP(:)'; YP(:)'],pad+1,pad+1,Rots(rr));
    else
      XYP = [XP(:)' + pad+1; YP(:)' + pad+1];
    end
    XYP = round(XYP);
    
    % calculate the cell sums using the integral image trick for all
    % detection windows in the entire image
    r1 = XYP(2,:) - chalf; r2 = r1 + csize;
    c1 = XYP(1,:) - chalf; c2 = c1 + csize;
    for bb = 1:numcells
      r1grid = r1(bb)-1 + Gy;
      c1grid = c1(bb)-1 + Gx;
      r2grid = r2(bb)-1 + Gy;
      c2grid = c2(bb)-1 + Gx;
      C(:,bb,:,:,rr) = H(:,r2grid,c2grid,rr) + H(:,r1grid,c1grid,rr)...
        - H(:,r1grid,c2grid,rr) - H(:,r2grid,c1grid,rr);
    end
  end
  
  
  %% Retrieve the vectors for each block
  % for each rotation angle to evaluate, get the cells integrals
  vlen = nBins*4;
  vvec = 1:vlen;
  F = zeros(vlen*numblocks,grheight,grwidth,nRots,'single');
  for rr = 1:nRots
    % calculate the features for each 2x2 block of cells, normalize the vectors
    % by using L1-sqrt. Sometimes the values calculated from the integral image
    % will be negative because of rounding errors, so use abs.
    if strcmp(conf.parHOG.BlockNorm,'L1-sqrt')
      for row = 1:blocks(2)
        for col = 1:blocks(1)
          bb = ([col col col+1 col+1] - 1) * cells(2) + [row row+1 row row+1];
          vset = abs(reshape(C(:,bb,:,:,rr),[vlen grheight grwidth]));
          vnorm = sum(vset,1) + normeps;
          r1 = (col-1) * blocks(2)*vlen + (row-1) * vlen;
          F(r1 + vvec,:,:,rr) = sqrt(vset ./ vnorm(ones(vlen,1,1),:,:));
        end
      end
    elseif strcmp(conf.parHOG.BlockNorm,'L2-hys')
      % L2-Hys normalization
      for row = 1:blocks(2)
        for col = 1:blocks(1)
          bb = ([col col col+1 col+1] - 1) * cells(2) + [row row+1 row row+1];
          vset = abs(reshape(C(:,bb,:,:,rr),[vlen grheight grwidth]));
          vnorm = sqrt(sum(vset.^2,1) + normeps);
          r1 = (col-1) * blocks(2)*vlen + (row-1) * vlen;
          F(r1 + vvec,:,:,rr) = ...
            min(vset ./ vnorm(ones(vlen,1,1),:,:),normcutoff);
        end
      end
    end
  end
  
  
  %% Retrieve the detection probability maps
  % reshape the features for classification
  TEST = reshape(F,[vlen*numblocks grheight*grwidth*nRots]);

  % LDA classify
  Platt = conf.parLDA(pp).Platt;
  SCORE = conf.parLDA(pp).W' * TEST - conf.parLDA(pp).B;
  PROB = 1 ./ (1 + exp(Platt(1) * SCORE + Platt(2)));

  % retrieve the detection values
  MAP(:,:,:,pp) = ...
    imresize(reshape(PROB,[grheight grwidth nRots]),...
    [imheight imwidth],'bilinear');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOOTNOTES
%
% Feb 22 - Integrating code from Extract_Dense_HOG and the outer shell for
%          better optimization (speed) and migrating from a simple setup to
%          the standard in purePIMO
% Feb 24 - Corrected an error in the circshift of HOG bins, wrong dim was
%          being shifted

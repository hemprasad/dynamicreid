function cmc = getCMCOnAllDatasets(path)
%GETCMCONALLDATASETS
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

caviar = load([path 'CAVIAR4REID.csv']);
ethz1 = load([path 'ETHZa1.csv']);
ethz2 = load([path 'ETHZa2.csv']);
ethz3 = load([path 'ETHZa3.csv']);
ilids = load([path 'i-LIDS.csv']);
viper = load([path 'VIPeRa.csv']);
cmc.ethz1 = [];
cmc.ethz2 = [];
cmc.ethz3 = [];
cmc.viper = [];
cmc.viper1 = [];
cmc.ilids = [];
cmc.caviar = [];
[cmc.ethz3, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/ETHZa/seq3', 1000, 28, ethz3, '.png');
[cmc.ethz2, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/ETHZa/seq2', 1000, 35, ethz2, '.png');
[cmc.ethz1, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/ETHZa/seq1', 1000, 83, ethz1, '.png');
[cmc.viper1, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/VIPeRa', 1000, 316, viper, '.bmp', 1);
[cmc.viper, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/VIPeRa', 1000, 316, viper, '.bmp');
[cmc.ilids, AUC, ER] = benchmarking('F:/Dataset_Reidentification/Datasets_REID/i-LIDS_Pedestrian/Persons', 1000, 50, ilids, '.jpg');
[cmc.caviar, AUC, ER] = benchmarking('F:/Dataset_Reidentification/CAVIAR4REID', 1000, 72, caviar, '.jpg');
end
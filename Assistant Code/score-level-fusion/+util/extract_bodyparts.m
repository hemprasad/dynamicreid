function bodyparts = extract_bodyparts(data,dset,parCPS)
%EXCTRACT_BODYPARTS Extract all bodyparts as single images.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    conf = reidaam.load_conf(parCPS);

    if nargin < 4
        % single global dataset run
        load([parCPS.Dir '/pose_estimations/MAPestimates.mat']);

        % output directory
        imdir = [parCPS.Dir '/pose_images'];
        util.chkmkdir(imdir);
    else
        % partition run
        testid = rundata(1);  tstr = num2str(testid,'%03d');
        numiter = rundata(2); nstr = num2str(numiter);

        % load this partition run #numiter estimates
        load([conf.PoseDir '/MAPestimates' nstr '_' tstr '.mat']);

        % output directory
        imdir = [parCPS.Dir '/pose_images_it' nstr '_' tstr '/'];
        util.chkmkdir(imdir);
    end
    
    numped = length(dset);
    [height,width,~,~] = size(data.RGB);

    % scale the part dimensions
    scale = parCPS.Scale;
    scaledSize = ceil(scale * conf.part_dims)-1;
    scaledCorner = -scaledSize / 2;

    % for each image store all eleven bodyparts
    nFiles = length(data.Files);
    bodyparts = cell(nFiles,11);

    % display progress
    hh = waitbar(0,['Processing image 0 of ' length(data.Files)]);
    
    % process all images
    for pedidx = 1:numped
        globals = dset(pedidx).global;
        numimg = length(globals);
        if isfield(dset,'run')
            runnums = dset(pedidx).run;
        else
            runnums = globals;
        end

        for jj = 1:numimg
            waitbar(globals(jj)/nFiles,hh,...
                sprintf('Processing image %i of %i',globals(jj),nFiles));
            I = data.RGB(:,:,:,globals(jj));
            for pidx = 1:conf.nParts
                map = zeros(height,width);
                % get position and rotation of the current part
                iy = MAP(1,pidx,runnums(jj));
                ix = MAP(2,pidx,runnums(jj));
                ridx = MAP(3,pidx,runnums(jj));
                rotAngle = parCPS.Rots(ridx);

                % create a rectangle
                FM = ones(scaledSize(pidx,[2 1])+1);
                
                % fold the part segmentation into the whole image
                if rotAngle
                    FM = util.imrot_fftbased(FM,-rotAngle,0);
                    FM = FM .* (FM > 0.4);

                    % approximate coordinates, problem is with the exact
                    % transformation during rotation
                    rect(3) = size(FM,2)-1;
                    rect(4) = size(FM,1)-1;
                    rect(1) = ix - round(rect(3)/2);
                    rect(2) = iy - round(rect(4)/2);
                else
                    rect = [[ix iy-1]+scaledCorner(pidx,:) scaledSize(pidx,:)];
                    rect = round(rect);
                end
                % check that paste rect is within image boundaries
                [phei,pwid] = size(FM); prect = [1 1 pwid-1 phei-1];
                if rect(1) < 1
                    offset = 1 - rect(1);
                    prect(1) = prect(1) + offset;
                    prect(3) = prect(3) - offset;
                    rect(1) = 1;
                    rect(3) = rect(3) - offset;
                end
                if rect(2) < 1
                    offset = 1 - rect(2);
                    prect(2) = prect(2) + offset;
                    prect(4) = prect(4) - offset;
                    rect(2) = 1;
                    rect(4) = rect(4) - offset;
                end
                if rect(1)+rect(3) > width
                    prect(3) = prect(3) + width - (rect(1)+rect(3));
                    rect(3) = width - rect(1);
                end
                if rect(2)+rect(4) > height
                    prect(4) = prect(4) + height - (rect(2)+rect(4));
                    rect(4) = height - rect(2);
                end
                rows = rect(2):rect(2)+rect(4);
                cols = rect(1):rect(1)+rect(3);
                prows = prect(2):prect(2)+prect(4);
                pcols = prect(1):prect(1)+prect(3);
                % create mask for the current part
                map(rows,cols) = max(map(rows,cols),FM(prows,pcols));
                
                Ir = im2double(I);
                if rotAngle
                    % rotate the image so that the part will be a proper
                    % square
                    map = imrotate(map,rotAngle);
                    Ir = imrotate(Ir,rotAngle);
                end
                % soften artefacts of rotation
                map = bwmorph(bwmorph(bwmorph(map>0.2,'spur'),'clean'),'close');
%                 J = Ir.*repmat(map,[1 1 3]);
                
                % find and store extracted bodypart
                [my,mx] = find(map);
                part = Ir(min(my):max(my),min(mx):max(mx),:);
                if numel(part) < 150
                   error('Failed to extract part %i of image %i!',pidx,globals(jj));
                end
                bodyparts{globals(jj),pidx} = max(min(part,1),0);
            end
        end
    end
    
    if ishandle(hh)
       close(hh); 
    end

end
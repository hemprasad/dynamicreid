function chkdataset(datadir)

if ~exist(datadir,'dir')
  error('train:missingData',['Dataset "' datadir '" is required.']);
end

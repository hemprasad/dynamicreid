function [pathdir,files] = list_files(paths,subdir,extension)
% search among all the paths the given subdir containing the files with the
% given extension
%
% Copyright (C) 2013 Cheng Dong Seon

pathdir = [];
files = [];
for parent = paths
  pathdir = [parent{1} subdir];
  if exist(pathdir,'dir')
    files = dir([pathdir '/*.' extension]);
    break;
  end
end

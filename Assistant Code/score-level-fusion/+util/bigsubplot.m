function h = bigsubplot(rows,cols,rr,cc,hgap,vgap)
%
% create subplots bigger than standard
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 5, hgap = 0.01; end
if nargin < 6, vgap = hgap; end

wid = (1.0-hgap)/cols;
pos(1) = (cc-1)*wid+hgap;
pos(3) = wid-hgap;

if length(vgap) == 1
  % the plot is centered vertically
  hei = (1.0-vgap)/rows;
  pos(2) = (rows-rr)*hei+vgap;
  pos(4) = hei-vgap;
else
  % the plot is not centered: vgap = [bottomgap topgap]
  hei = 1.0/rows;
  pos(2) = (rows-rr)*hei + vgap(1);
  pos(4) = hei - sum(vgap);
end
h = subplot('position',pos);

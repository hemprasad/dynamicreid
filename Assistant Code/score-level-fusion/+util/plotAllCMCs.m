function plotAllCMCs(cmcStructures, names)
%PLOTALLCMCS
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = size(cmcStructures, 2);

colors{1} = 'b-';
colors{2} = 'g-';
colors{3} = 'r-';
colors{4} = 'c-';
colors{5} = 'm-';
colors{6} = 'k-';

% viper
figure;
plot(1 : 316, cmcStructures{1}.viper * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - VIPeR');
axis([1 316   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 316, cmcStructures{i}.viper * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% viper'
figure;
plot(1 : 316, cmcStructures{1}.viper1 * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - VIPeR (manipulated)');
axis([1 316   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 316, cmcStructures{i}.viper1 * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% i-LIDS
figure;
plot(1 : 50, cmcStructures{1}.ilids * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - i-LIDS');
axis([1 50   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 50, cmcStructures{i}.ilids * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% caviar
figure;
plot(1 : 72, cmcStructures{1}.caviar * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - CAVIAR4REID');
axis([1 72   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 72, cmcStructures{i}.caviar * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% ethz 1
figure;
plot(1 : 83, cmcStructures{1}.ethz1 * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - ETHZ Seq. 1');
axis([1 83   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 83, cmcStructures{i}.ethz1 * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% ethz 2
figure;
plot(1 : 35, cmcStructures{1}.ethz2 * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - ETHZ Seq. 2');
axis([1 35   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 35, cmcStructures{i}.ethz2 * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

% ethz 3
figure;
plot(1 : 28, cmcStructures{1}.ethz3 * 100, colors{1});
grid on;
xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic (CMC) - ETHZ Seq. 3');
axis([1 28   0 100]);
if n > 1
    hold on
    for i = 2 : n
        plot(1 : 28, cmcStructures{i}.ethz3 * 100, colors{i});
    end
    hold off
end
legend(names,'Location','SouthEast');

end
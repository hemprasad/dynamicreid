function [h,s,v] = multirgb2hsv(RGB)
%
% Converts multiple color images from RGB to HSV.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program  is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program .  If not, see <http://www.gnu.org/licenses/>.

% convert to floating point
X = single(RGB) / 255;
r = X(:,:,1,:);
g = X(:,:,2,:);
b = X(:,:,3,:);


[H,W,~,num] = size(X);
h = zeros(H,W,1,num,'single');

v = max(X,[],3);
m = min(X,[],3);
s = v - m;
z = ~s;
s = s + z;

k = find(r == v);
h(k) = (g(k) - b(k)) ./ s(k);
k = find(g == v);
h(k) = 2 + (b(k) - r(k)) ./ s(k);
k = find(b == v);
h(k) = 4 + (r(k) - g(k)) ./ s(k);
h = h/6;
k = find(h < 0);
h(k) = h(k) + 1;
h = (~z) .* h;

k = find(v);
s(k) = (~z(k)) .* s(k) ./ v(k);
s(~v) = 0;

% if there is only one output requested, concatenate hsv
if nargout == 1
  h = cat(3,h,s,v);
end

function tab = extractTablesForExperiments(parCPS,expr,nImages)
%EXTRACTTABLESFOREXPERIMENTS Create a stack of the selected matching tables
% for further computations
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

featprops = util.loadFromFile([parCPS.Dir '/features/features' expr.MAT],'featprops');

% See the matching section to see to which of the matching functions 
% these numbers belong to. Please note the difference between the number
% of different functions that are computed for the different types of
% features (vector vs. histogram)
matchFunIdx = [1 3 5 1 1 3 1 5 1];

tname = genvarname(['Tables_' expr.Name]);
tab.(tname) = {};
tab.legendTables = {};
pos = 1;
for featidx=1:length(featprops)
    % load the name of the feature
    featname = featprops{featidx}{1};

    if strcmp(featname,'BiCov') % BiCov tables are excluded from the final experiment
        fprintf('Skipping %s\n',featname);
    else
        if featprops{featidx}{3} % feature for whole image
            featname = [featname '_full'];
            partnames = {'Full'};
        else
            partnames = {'Torso','Shoulders','Head','RArm','RForearm',...
                'LArm','LForearm','RThigh','RLeg','LThigh','LLeg'};
        end

        fname = [parCPS.Dir '/matching/matching_' featname expr.MAT];
        if exist(fname,'file')
            % load the whole matching table from file
            matching = util.loadFromFile(fname,'matching');
            % for each, select only the tables chosen by the user
            for partidx=1:length(partnames)
                if (length(featprops{featidx}) < 4) && (size(matching,4) > 1)
                    tab.(tname){pos} = matching(:,:,partidx,matchFunIdx(featidx));
                elseif (length(featprops{featidx}) == 4) && (size(matching,4) > 1)
                    tab.(tname){pos} = matching(:,:,partidx,featprops{featidx}{4});
                else
                    tab.(tname){pos} = squeeze(matching(:,:,partidx,1));
                end
                tab.legendTables{pos} = [featname '->' partnames{partidx}];
                pos = pos +1;
            end
        else
            fprintf('Cannot load %s!\n',featname);
        end

    end
end

clear matching;

%% Add matching tables of ReIdAAM
fname = [parCPS.Dir '/matching/matching_ReIdAAM' expr.MAT];
if exist(fname,'file')
    ReIdAAM = load(fname);

%     % ETHZ1 sampling
%     if strcmp(expr.Name,'ETHZ1')
%         ReIdAAM.BVT = ReIdAAM.BVT(1:2:nImages,1:2:nImages);
%     end
%     % ETHZ1 sampling (End)
%     tab.(tname){pos} = single(ReIdAAM.BVT);
%     tab.legendTables{pos} = 'BVT_full';
%     pos = pos +1;
%     
%     clear ReIdAAM;

    % ETHZ1 sampling
    if strcmp(expr.Name,'ETHZ1')
        ReIdAAM.MSCR = ReIdAAM.MSCR(1:2:nImages,1:2:nImages);
    end
    % ETHZ1 sampling (End)
    tab.(tname){pos} = single(ReIdAAM.MSCR);
    tab.legendTables{pos} = 'MSCR';
    pos = pos +1;
else
    fprintf('Cannot load %s!\n',fname);
end

%% Add matching tables of SDALF
if isfield(expr,'SDALFFeat') && expr.SDALFFeat
    SDALF = load([parCPS.Dir '/matching/matching_SDALF' expr.MAT]);
%         tab.(tname){pos} = single(SDALF.RHSP);
%         tab.legendTables{pos} = 'SDALF RHSP';
%         pos = pos +1;
    % ETHZ1 sampling
    if strcmp(expr.Name,'ETHZ1')
        SDALF.wHSV = SDALF.wHSV(1:2:nImages,1:2:nImages);
    end
    % ETHZ1 sampling (End)
    tab.(tname){pos} = single(SDALF.wHSV);
    tab.legendTables{pos} = 'SDALF wHSV';
    pos = pos + 1;
end

clear SDALF;

%% Add matching tables of tracker
trackerDir = [parCPS.Dir '/matching/tracker/'];
if exist(trackerDir,'dir')
    filesInTrackerDir = dir([trackerDir '*.csv']);
    nTrackerTables = length(filesInTrackerDir);

    hh = waitbar(0,'Reading tracker tables from file...');
    for i=1:nTrackerTables
        fname = filesInTrackerDir(i).name;
        trackerTable = csvread([trackerDir fname]);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ETHZ1 sampling
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if strcmp(expr.Name,'ETHZ1')
            trackerTable = trackerTable(1:2:nImages,1:2:nImages);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ETHZ1 sampling (End)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        tab.(tname){pos} = single(trackerTable);
        tab.legendTables{pos} = fname;
        pos = pos + 1;
        waitbar(i/nTrackerTables,hh);
    end

    clear trackerTable

    if ishandle(hh)
        close(hh);
    end
else
    disp(['No directory found at "' trackerDir '"!'])
end

%% Add matching tables of SDC
fname = [parCPS.Dir '/matching/matching_SDC_full' expr.MAT];
if exist(fname,'file')
    SDC = util.loadFromFile(fname,'matching');
    SDC = single(SDC - min(SDC(:)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ETHZ1 sampling
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(expr.Name,'ETHZ1')
        SDC = SDC(1:2:nImages,1:2:nImages);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ETHZ1 sampling (End)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tab.(tname){pos} = SDC;
    tab.legendTables{pos} = 'SDC';
else
    fprintf('Cannot load %s\n',fname);
end

end
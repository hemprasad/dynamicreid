function [BinEdges] = ...
    checkBinning(BinEdges,par,dataPath,featname)
%CHECKBINNING Check if loaded binning complies with chosen parameters.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    switch featname
        case 'ELF'
            loadedNBinsELF = cellfun(@numel,BinEdges);
            if ~(length(par.NBins(:)) == length(loadedNBinsELF(:))) || ...
                    ~all(par.NBins(:) == (loadedNBinsELF(:)-1)) || ...
                        (isfield(par,'ForceRecalcBinning') && par.ForceRecalcBinning)
                warning('Need to recalculate binning for ELF!')
                BinEdges = recalcBinningELF(par,dataPath);
            end
            
        case 'Lab'
            loadedNBinsLab = cellfun(@numel,BinEdges);
            if ~(length(par.NBins(:)) == length(loadedNBinsLab(:))) || ...
                    ~all(par.NBins(:) == (loadedNBinsLab(:)-1)) || ...
                        (isfield(par,'ForceRecalcBinning') && par.ForceRecalcBinning)
                warning('Need to recalculate binning for Lab!')
                BinEdges = recalcBinningLab(par,dataPath);
            end
            
        case 'MR8'
            loadedNBinsMR8 = cellfun(@numel,BinEdges);
            if ~(length(par.NBins(:)) == length(loadedNBinsMR8(:))) || ...
                    ~all(par.NBins(:) == (loadedNBinsMR8(:)-1)) || ...
                        (isfield(par,'ForceRecalcBinning') && par.ForceRecalcBinning)
                warning('Need to recalculate binning for MR8!')
                BinEdges = recalcBinningMR8(par,dataPath);
            end
            
        otherwise
            error('Unknown featname for checkBinning(...)!');
    end
end


function BinEdgesELF = recalcBinningELF(parELF,dataPath)
    
    INRIADataset = collectINRIA(dataPath);
    
    %% Preparations
    % 8 color feature channels and 21 texture filters applied to Y
    % texture filterbank
    N = parELF.FiltSize;
    fbGabor = elf.generateGaborFilter(N);
    fbSchmid = elf.generateSchmidFilter(N);
    fbTexture = [fbGabor; fbSchmid];

    NBins = parELF.NBins;
    assert(length(NBins) == 29);

    %% Data accumulation
    hh = waitbar(0,'Accumulating Data');
    BinEdgesELF = cell(29,1);
    for ch=1:29
        datapoints = zeros(numel(INRIADataset)/3,1);
        for i=1:size(INRIADataset,4)
            waitbar(i/size(INRIADataset,4),hh,...
                sprintf('Accumulating Data %d of 29',ch));
            
            img_rgb = squeeze(INRIADataset(:,:,:,i));
            img_ycbcr = rgb2ycbcr(img_rgb);
            sidx = (i-1)*128*48+1;
            eidx = sidx+128*48-1;
            if ch <= 8
                %% color feature channels
                img_hsv = rgb2hsv(img_rgb);
                imgstack = cat(3,img_rgb,img_hsv(:,:,1:2),img_ycbcr);
                values = imgstack(:,:,ch);
                datapoints(sidx:eidx) = values(:);
            else
                conv_imgY = imfilter(img_ycbcr(:,:,1),real(fbTexture{ch-8}),'same',0);
                datapoints(sidx:eidx) = conv_imgY(:);
            end
        end
        BinEdgesELF{ch} = util.findOptimalBining(datapoints,NBins(ch));
    end

    if ishandle(hh)
        close(hh);
    end

    %% Calculate bin edges based on the accumulated data
%     fprintf('Calculating new bin-edges...');
%     parfor i=1:29
%         BinEdgesELF{i} = util.findOptimalBining(datapoints{i},NBins(i));
%     end
%     fprintf('OK\n');
    
    fprintf('Save ELF-bins to file...');
    save('MAT/_BINS_/binedges_elf.mat','BinEdgesELF');
    fprintf('OK\n');
    
end

function BinEdgesLab = recalcBinningLab(parLab,dataPath)
    
    INRIADataset = collectINRIA(dataPath);
    
    datapoints = cell(3,1);
    rgb2lab = makecform('srgb2lab');
    
    NBins = parLab.NBins;
    assert(length(NBins) == 3);

    %% Data accumulation
    hh = waitbar(0,'Accumulating Data');
    for i=1:lsize(INRIADataset,4)
        waitbar(i/size(INRIADataset,4),hh);
        %% color feature channels
        img_rgb = squeeze(INRIADataset(:,:,:,i));
        img_lab = applycform(img_rgb,rgb2lab);
        parfor ch=1:3
            values = img_lab(:,:,ch);
            datapoints{ch} = [datapoints{ch}; values(:)];
        end
    end

    if ishandle(hh)
        close(hh);
    end
    
    %% Calculate bin edges based on the accumulated data
    BinEdgesLab = cell(3,1);
    fprintf('Calculating new bin-edges...');
    parfor i=1:3
        BinEdgesLab{i} = util.findOptimalBining(datapoints{i},NBins(i));
    end
    fprintf('OK\n');
    
    fprintf('Save Lab-bins to file...');
    save('MAT/_BINS_/binedges_lab.mat','BinEdgesLab');
    fprintf('OK\n');
end

function BinEdgesMR8 = recalcBinningMR8(parMR8,dataPath)
    
    INRIADataset = collectINRIA(dataPath);
    
    datapoints = repmat({zeros(numel(INRIADataset)/3,1)},8,1);
    
    NBins = parMR8.NBins;
    assert(length(NBins) == 8);
    
    hh = waitbar(0,'Accumulating Data');
    for i=1:size(INRIADataset,4)
        waitbar(i/size(INRIADataset,4),hh);
    	img_gray = rgb2gray(INRIADataset(:,:,:,i));
    	features = mr8.MR8fast(img_gray);
        
        sidx = (i-1)*128*48+1;
        eidx = sidx+128*48-1;
    	for j=1:8
    		feature = features(j,:);
    		datapoints{j}(sidx:eidx) = feature(:);
    	end
    end
    
    if ishandle(hh)
        close(hh);
    end
    
    %% Calculate bin edges based on the accumulated data
    BinEdgesMR8 = cell(size(datapoints));
    fprintf('Calculating new bin-edges...');
    for i=1:size(BinEdgesMR8,1)
        BinEdgesMR8{i} = util.findOptimalBining(datapoints{i},NBins(i));
    end
    fprintf('OK\n');
    
    fprintf('Save MR8-bins to file...');
    save('MAT/_BINS_/binedges_mr8.mat','BinEdgesMR8');
    fprintf('OK\n');
end

function INRIADataset = collectINRIA(dataPath)

    % try to load INRIA-dataset from file
    fname = 'MAT/_DATASETS_/dataset_INRIA_48x128_binning.mat';
    if exist(fname,'file')
        load(fname,'INRIADataset');
    else
        % load filenames
        [Dir, Files] = loadDataDirs(dataPath,'INRIA');
        INRIADataset = zeros(128,48,3,length(Files),'double');

        hh = waitbar(0,'Loading Dataset');
        for i=1:length(Files)
            waitbar(i/length(Files),hh);
            INRIADataset(:,:,:,i) = im2double(imread([Dir filesep Files(i).name]));
        end
        
        if ishandle(hh)
            close(hh)
        end
        
        fprintf('Save INRIA to file...');
        save(fname,'INRIADataset');
        fprintf('OK\n');
    end
end

% BinEdgesELF = util.loadBinsFromFile('MAT/_BINS_/binedges_elf.mat','bins_elf');
% BinEdgesLAB = util.loadBinsFromFile('MAT/_BINS_/binedges_lab.mat','bins_lab');
% BinEdgesMR8 = util.loadBinsFromFile('MAT/_BINS_/binedges_mr8.mat','bins_mr8');
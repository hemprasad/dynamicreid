function [ matching ] = partwiseMatching(features,distfun)
%PARTWISEMATCHING Compute Matching Score per (CPS-)bodypart
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    matching = struct();
    nImages = length(features);
    % get the names of the features 
    % this only works if all features were computed for all parts
    partnames = fieldnames(features{1});
    featurenames = fieldnames(features{1}.(partnames{1})); 
    
    match_tables = cell(length(partnames),length(featurenames));
    ix = cellfun('isempty',match_tables);
    match_tables(ix) = {zeros(nImages)};
    
    for i=1:length(partnames)
        for j=1:length(featurenames)
            matching.(partnames{i}).(featurenames{j}) = zeros(nImages);
        end
    end
    
    hh = waitbar(0);
    for i=1:length(partnames)
        message = sprintf('Matching features of part %s',partnames{i});
        waitbar(i/length(partnames),hh,message);
        parfor j=1:length(featurenames)
            for galid=1:nImages
                gal = features{galid};
                for probid=1:galid
                    prob = features{probid};
                    
                    dist = distfun.(featurenames{j})(...
                        gal.(partnames{i}).(featurenames{j}),...
                        prob.(partnames{i}).(featurenames{j}));
                    match_tables{i,j}(galid,probid) = dist;
%                     matching.(partnames{i}).(featurenames{j})(galid,probid) = dist;
%                     matching.(partnames{i}).(featurenames{j})(probid,galid) = dist;
                    
                end
%                 message = sprintf('Matching feature %s of part %s',...
%                     featurenames{j},partnames{i});
%                 waitbar(galid/nImages,hh,message);
            end
        end
    end
    
    for i=1:length(partnames)
        for j=1:length(featurenames)
            upperTable = triu(match_tables{i,j},1);
            matching.(partnames{i}).(featurenames{j}) = upperTable + upperTable';
        end
    end
    
    if ishandle(hh)
        close(hh)
    end

end


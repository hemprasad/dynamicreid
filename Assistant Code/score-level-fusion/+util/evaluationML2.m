function [cmc, AUC, ER, parameters, weights, fusedMatchingTables, partitionTest] = evaluationML2(tables, mask, datasetPath, datasetFileSuffix, type, nRuns, gallerySize, plotColor)
% evaluation
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling, A. Vorndran                           
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% [train, test] = util.getPartitioningSDALF(datasetPath, nRuns, gallerySize, datasetFileSuffix);
[train,test] = util.getPartitioningML('datasets/VIPeRa',10,316,'.bmp','PartitionFromML_3366.mat');

fusedMatchingTables = cell(nRuns,1);
parameters = cell(nRuns,1);
weights = cell(nRuns,1);
hwait = waitbar(0);
for run = 1 : nRuns
    stime = clock;
    for t = 1 : size(tables,2)
        matchingTable = tables{run,t};
        matchingTrain = matchingTable(train(run, :), train(run, :));
        matchingTest = matchingTable(test(run, :), test(run, :));
        learnTables{t} = matchingTrain;
        transTables{t} = matchingTest;
    end
    
    learnMask = mask(train(run, :), train(run, :));

%     [parameter, weight, fusedMatchingTable] = fusion.LearningandFusion(learnTables, transTables, learnMask, type);
    [parameters{run}, weights{run}, fusedMatchingTables{run}] = fusion.LearningandFusion(learnTables, transTables, learnMask, type);

%     fusedMatchingTables{run} = fusedMatchingTable;
%     parameters{run} = parameter;
%     weights{run} = weight;
    if hasInfNaN(fusedMatchingTables{run})
        warning(['Inf or NaN using ' type ' at run ' num2str(run)]);
    end
    
    waitbar(run/nRuns,hwait);
    disp(['Run ' num2str(run) ' took ' util.getDurationString(stime)]);
end

if ishandle(hwait)
    close(hwait);
end

partitionTest = test;

[cmc, AUC, ER] = util.benchmarking2(datasetPath, test, fusedMatchingTables, datasetFileSuffix, plotColor);

% [train, test] = util.getPartitioning(...);

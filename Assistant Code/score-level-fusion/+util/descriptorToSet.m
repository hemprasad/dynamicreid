function [ set ] = descriptorToSet( descriptor )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    if ndims(descriptor) ~= 3
       error(['Input is expected to be 3-dimensional with size MxNxF,\n'...
           'with MxN being the original image dimension and F being\n'...
           'the feature dimension.']); 
    end
    
    nelem = size(descriptor,1)*size(descriptor,2);
    set = reshape(descriptor,[nelem size(descriptor,3)]);

end


% function rotrect = rototrasl(XY,xm,ym,rotm)
function rotrect = rototrasl(rectindex,xm,ym,rotm)
rotrect=[cosd(rotm) -sind(rotm); sind(rotm) cosd(rotm)]*rectindex;
rotrect=rotrect+repmat([xm;ym],1,size(rectindex,2));
function chkparallel(num)
% check if the current computer is capable of parallel processing (multiple
% cores) and if the user wants to activate it

if exist('matlabpool','file')
    
    isOpen = matlabpool('size') > 0;
    numcores = feature('numCores');
    if ~isOpen && numcores > 1
        u = input('You have multiple cores. Do you want parallel processing enabled? ','s');
        if ~isempty(u) && u(1) == 'y'
            num = min(numcores,num);
            matlabpool('open',num);
        end
    end
    
else
    disp('No parallel processing toolbox: expect slower performance.');
end

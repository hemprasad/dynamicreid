function h = handlesubplot(num,numset,gap)
% automatically handle figure division into subplots
% if length(numset) < 4, then single row
% otherwise as square as possible

total = length(numset);
numone = numset(1);
numlast = numset(end);

if total < 4
  % form a row of bigsubplots
  h = util.bigsubplot(1,total,1,num-numone+1,gap);
else
  
end

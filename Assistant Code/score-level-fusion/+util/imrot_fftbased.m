function [Y,Tvec] = imrot_fftbased(X,angle,padval)
% Image rotation using convolution-based interpolation.
% Returns the rotated image and the translation vector used to pad the
% original size.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Reduce angle to a 1st-quadrant rotation.
if angle == 0
    Y = X; Tvec = [0; 0]; return;
elseif angle>-45 && angle<=45
    qcase = 0; % default case
else
    angle = mod(angle,360);
    if angle>45 && angle<=135
        X = permute(X,[2 1 3]); X = X(end:-1:1,:,:); % 90 degrees rotation
        angle = angle - 90; qcase = 1;
    elseif angle>135 && angle<=225
        X = X(end:-1:1,end:-1:1,:); % 180 degrees rotation
        angle = angle - 180; qcase = 2;
    elseif angle>225 && angle<=315
        X = permute(X,[2 1 3]); X = X(:,end:-1:1,:); % 270 degrees rotation
        angle = angle - 270; qcase = 3;
    end
end
if nargin < 3, padval = 'symmetric'; end

%% Calculate angle in radiants and shearing coefficients.
theta = pi*angle/180;
tanth2 = tan(theta/2);
sinth = sin(theta);

%% First pass: shearing along x-axis.
[Nr,Nc,Dims] = size(X);
if Dims > 1, padvec = zeros(1,3); else padvec = zeros(1,2); end
D = ( (0:Nr-1) - Nr/2 ) * tanth2;
padvec(2) = ceil(abs(D(1)));
X = padarray(X,padvec,padval,'both'); Nx = Nc + 2*padvec(2);

S = fft(X,[],2);

center = floor(Nx/2)+1;
K = ( (1:Nx)-center ) / (Nx/2);
H = exp(-pi*i* D'*K);
iH = ifftshift(H,2);
if Dims > 1, iH = iH(:,:,ones(1,1,Dims)); end

X = real(ifft(S.*iH,[],2));
X(X<0) = 0; X(X>255) = 255;

%% Second pass: shearing along y-axis.
xC = Nc/2-abs(D(1));
padvec(1) = ceil(abs(sinth*xC)); padvec(2) = 0;
X = padarray(X,padvec,padval,'both'); Ny = Nr + 2*padvec(1);

S = fft(X,[],1);

center = floor(Ny/2)+1;
K = ( (1:Ny)-center ) / (Ny/2);
D = -( (0:Nx-1) - Nx/2 ) * sinth;
H = exp(-pi*i* K'*D);
iH = ifftshift(H,1);
if Dims > 1, iH = iH(:,:,ones(1,1,Dims)); end

X = real(ifft(S.*iH,[],1));
X(X<0) = 0; X(X>255) = 255;

%% Third pass: shearing along x-axis.
xB = xC-Nc;
yE = Nr/2-abs(xB*sinth);
padvec(2) = ceil(abs(tanth2*yE)); padvec(1) = 0;
X = padarray(X,padvec,padval,'both'); Nx = Nx + 2*padvec(2);

S = fft(X,[],2);

center = floor(Nx/2)+1;
K = ( (1:Nx)-center ) / (Nx/2);
D = ( (0:Ny-1) - Ny/2 ) * tanth2;
H = exp(-pi*i* D'*K);
iH = ifftshift(H,2);
if Dims > 1, iH = iH(:,:,ones(1,1,Dims)); end

Y = real(ifft(S.*iH,[],2));
% Y(Y<0) = 0;
% Y(Y>255) = 255;

% retrieve the translation vector implicit in the padding processes used to
% guarantee the original image is completely retained in the rotated image
% space
if nargout < 2, return; end
% Rmat = [cos(theta) sinth; -sinth cos(theta)];
switch qcase
    case 0
        if angle > 0
%             corner = Rmat * [-Nc; 0];
%             pixel  = Rmat * [-Nc+0.5; 0.5];
%             Tvec = [pixel(1)-corner(1)-0.5; pixel(2)-0.5];
            %Tvec = [0; Nc*sinth];
            Tvec = [0.5; 1+(Nc+0.5)*sinth];
        else
            Tvec = [1.5-(Nr+0.5)*sinth; 0.25];
        end
%     case 1
%         if angle > 0
%             Tvec = [Nr*sinth; Nr*cos(theta) + Nc*sinth];
%         else
%             Tvec = [0; Nr*cos(theta)];
%         end
%     case 2
%         if angle > 0
%             Tvec = [Nc*cos(theta) + Nr*sinth; Nr*cos(theta)];
%         else
%             Tvec = [Nc*cos(theta); Nr*cos(theta) - Nc*sinth];
%         end
%     case 3
%         if angle > 0
%             Tvec = [Nc*cos(theta); 0];
%         else
%             Tvec = [Nc*cos(theta) - Nr*sinth; -Nc*sinth];
%         end
    otherwise
        error('myrotate: unsupported angles for Tvec');
end

% SvsS-Benchmarking
%
% Input
% -----
% imageFolder:  path for dataset images (CAVIAR4REID filen naming convention)
% nRuns:        repeat the experiment <nRuns> times (standard value in state-of-the-art = 10)
% gallerySize:  number of persons in gallery
% matching:     matching table (result for comparing each image with each other; gallery = rows, probe = cols)
% fileExtension: image file extension ('.jpg', '.png', or '.bmp')
%
% Return
% ------
% cmc:          values for CMC-Curve
% AUC:          area under CMC-Curve
% ER:           expected rank
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                     
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cmc, AUC, ER] = benchmarking2(imageFolder, galleryMask, matchings, fileExtension, plotColor)
% default parameters
if nargin < 5
    plotColor = 0; % do not plot
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Index the dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% currentFolder = pwd;                        % remember current directory
% cd(imageFolder);                            % go to dataset directory
if ~exist(imageFolder,'dir')
    error(['Image folder "' imageFolder '" could not be found.']);
end
fileList = dir(imageFolder);                % get files of directory
nFiles = numel(fileList);                   % get number of files in directory
label = [];                                 % initialize person labels
fIDs = [];                                  % initialize per person frame IDs
for i = 1 : nFiles                          % for each file in directory
    if fileList(i).isdir == 0               % test if it is a file
        [~,name,ext] = fileparts(fileList(i).name); % separate filename in path, name and extension
        if ext == fileExtension             % test if file is an image
            personID = str2num(name(1:4));	% get person ID from filename
            frameID = str2num(name(5:7));	% get frame ID from filename
            label = [label personID];       % add person ID to list of label
            fIDs = [fIDs frameID];          % add frame ID to list of per person frame IDs
        end % if
    end % if
end % for
% cd(currentFolder);                          % go back to working directory

nRuns = size(galleryMask, 1);
labelGallery = label(galleryMask(1, :));
personIDs = unique(labelGallery);
gallerySize = length(personIDs);

rankHist = zeros(nRuns, gallerySize);
for r = 1 : nRuns
    labelGallery = label(galleryMask(r, :));
    fIDsGallery = fIDs(galleryMask(r, :));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute random samples for gallery and probe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    personIDs = unique(labelGallery);           % get unique labels
    nPersons = length(personIDs);               % count number of persons in dataset from unique labels
    gallery = zeros(nPersons, 1);               % initialize gallery set
    probe = zeros(nPersons, 1);                 % initialize probe set
    for i = 1 : nPersons                        % for each person
        nSamples(i) = max(fIDsGallery(labelGallery == personIDs(i))); % count number of samples for this person
        refStartIndex(i) = sum(nSamples(1:i)) - nSamples(i) + 1; % get index of first image for this person
        g = floor(rand(1, 1) * nSamples(i));    % choose rondom samples for gallery
        p = floor(rand(1, 1) * (nSamples(i) - 1)); % choose rondom samples for probe (not identical with gallery)
        p(p >= g) = p(p >= g) + 1;              % coorection for non-identical-condition
        gallery(i, :) = g + refStartIndex(i);   % save random chosen indexes for gallary images
        probe(i, :) = p + refStartIndex(i);     % save random chosen indexes for probe images
    end % for
    if max(nSamples(:)) == 2
        gallery = (1:2:(2*nPersons-1))';
        probe = (2:2:(2*nPersons))';
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start benchmarking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gallerySet = gallery;
    probeSet = probe;
    matching = matchings{r};
    scoreTable = matching(gallerySet, probeSet);
    for p = 1 : gallerySize
        rank = sum(scoreTable(p, p) >= scoreTable(:, p)); % compute rank for person p: comparison for distance score (use <= for similarity score)
        rankHist(r, rank) = rankHist(r, rank) + 1; % increment bin for this rank
    end
end % for
perRunCMC = cumsum(rankHist, 2) / gallerySize;
rankTimesCount = repmat(1 : gallerySize, nRuns, 1) .* rankHist;
ER = sum(rankTimesCount(:)) / (nRuns * gallerySize);
if size(perRunCMC, 1) == 1
    cmc = perRunCMC;
else
    cmc = sum(perRunCMC) / nRuns;
end
AUC = sum(cmc) / gallerySize;

if plotColor > 0 % output on
    disp(['ER = ', num2str(ER)]);
    disp(['AUC = ', num2str(AUC)]);

    cmap = colormap(lines);
    plc = mod(plotColor, size(cmap, 1));
    if plc == 0
        plc = size(cmap, 1);
    end
    plot(1 : gallerySize, cmc * 100, 'Color', cmap(plc, :),'LineWidth',2);
    grid on;
    xlabel('Rank');
    ylabel('Recognition Percentage');
    title(['Cumulative Match Characteristic (CMC) (p = ', num2str(gallerySize), ')']);
    axis([1 gallerySize   0 100]);
end

end
function features = extractBiCovFeatures(Img_HSV, gaborfilter)
%BICOV Implementation of BiCov-Band by Ma et al.
%   bicovFeatures = bicov(IMG_HSV)
% @inproceedings{ma2012bicov,
%   title={Bicov: a novel image representation for person re-identification
%   and face verification},
%   author={Ma, Bingpeng and Su, Yu and Jurie, Fr{\'e}d{\'e}ric},
%   booktitle={British Machine Vision Conference},
%   year={2012}
% }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if size(Img_HSV,3) ~= 3
        error('I has to be a 3-channel HSV-image')
    end

    sizeFilter1 = [11 15 19 23 27 31 35 39];
    sizeFilter2 = [13 17 21 25 29 33 37 41];

    u = 16;
    v = 8;

    % Seperate Channels
    H.img = Img_HSV(:,:,1);
    S.img = Img_HSV(:,:,2);
    V.img = Img_HSV(:,:,3);

    if nargin == 1
        if exist('+bicov/bicovfilter.mat','file') && 0
            load('+bicov/bicovfilter.mat','gaborfilter');
        else
            % generate filter
            gaborfilter = cell(16,1);
            gfb1 = cell(length(sizeFilter1));
            gfb2 = cell(length(sizeFilter2));
            for sci=1:length(sizeFilter1)
                gfb1{sci} = bicov.getGarborFilter(u,v,(2*sci-1),sizeFilter1(sci));
                gfb2{sci} = bicov.getGarborFilter(u,v,(2*sci)  ,sizeFilter2(sci));
                % average of the orientations
                averagedfb1 = 1/v*gfb1{sci}{1};
                averagedfb2 = 1/v*gfb2{sci}{1};
                for j=2:v
                    averagedfb1 = averagedfb1+1/v*gfb1{sci}{j};
                    averagedfb2 = averagedfb2+1/v*gfb2{sci}{j};
                end
                gaborfilter{2*sci-1,1} = averagedfb1;
                gaborfilter{2*sci,1} = averagedfb2;
            end
            save('+bicov/bicovfilter.mat','gaborfilter');
        end
    end
    %

    % Compute gabor features for each channel
    H.Band = cell(8,1);
    S.Band = cell(8,1);
    V.Band = cell(8,1);
  
    % extract gabor features
    GH1 = bicov.extractGaborFeatures(H.img,gaborfilter);
    GS1 = bicov.extractGaborFeatures(S.img,gaborfilter);
    GV1 = bicov.extractGaborFeatures(V.img,gaborfilter);
    
    for i=1:8
        % form bio-inspired feature magnitude images seperately
        % for each channel
        HBIFM = max(abs(GH1{2*i-1}),abs(GH1{2*i}));
        SBIFM = max(abs(GS1{2*i-1}),abs(GS1{2*i}));
        VBIFM = max(abs(GV1{2*i-1}),abs(GV1{2*i}));
        % Form final feature vector based on BIFM
        % FeatVec = [x, y, Bi(x,y), Bix(x,y), Biy(x,y), Bixx(x,y), Biyy(x,y)]
        % For channel H
        H.Band{i} = bicov.extractGradientFeatures(HBIFM);
        % For channel S
        S.Band{i} = bicov.extractGradientFeatures(SBIFM);
        % For channel V
        V.Band{i} = bicov.extractGradientFeatures(VBIFM);
    end

%     bicov.FilterresponseVisualisation;
%     bicov.BIFMagnitudeImageVisualisation;
%     bicov.FilterVisualisation;
    %% Calculate covariance descriptor for overlapping regions
    % size of sliding window: 8x8
    % shift: 4 4
    dx = 8;
    dy = 8;
    sx = 4;
    sy = 4;
    dH = [];
    dS = [];
    dV = [];
    for i=1:4
        % load bands
        RepH1 = H.Band{2*i-1};
        RepH2 = H.Band{2*i};
        RepS1 = S.Band{2*i-1};
        RepS2 = S.Band{2*i};
        RepV1 = V.Band{2*i-1};
        RepV2 = V.Band{2*i};
        idx = 1;
        for x=1:sx:size(H.img,2)-sx
            idy = 1;
            for y=1:sy:size(H.img,1)-sy
                % channel H
                % select a part of the feature map
                Window1 = RepH1(y:(y+dy-1),x:(x+dx-1),:);
                Window2 = RepH2(y:(y+dy-1),x:(x+dx-1),:);
                % calculate cov-descriptor
                reshapedWindow1 = reshape(Window1,[dx*dy 7]);
                reshapedWindow2 = reshape(Window2,[dx*dy 7]);
                % difference of cov-descriptors
                dH(i*idy,idx) = diffCovDesc(reshapedWindow1,reshapedWindow2);
                
                % channel S
                % select a part of the feature map
                Window1 = RepS1(y:(y+dy-1),x:(x+dx-1),:);
                Window2 = RepS2(y:(y+dy-1),x:(x+dx-1),:);
                % calculate cov-descriptor
                reshapedWindow1 = reshape(Window1,[dx*dy 7]);
                reshapedWindow2 = reshape(Window2,[dx*dy 7]);
                % difference of cov-descriptors
                dS(i*idy,idx) = diffCovDesc(reshapedWindow1,reshapedWindow2);
                
                % channel V
                % select a part of the feature map
                Window1 = RepV1(y:(y+dy-1),x:(x+dx-1),:);
                Window2 = RepV2(y:(y+dy-1),x:(x+dx-1),:);
                % calculate cov-descriptor
                reshapedWindow1 = reshape(Window1,[dx*dy 7]);
                reshapedWindow2 = reshape(Window2,[dx*dy 7]);
                % difference of cov-descriptors
                dV(i*idy,idx) = diffCovDesc(reshapedWindow1,reshapedWindow2);

                idy = idy + 1;
            end
            idx = idx + 1;
        end
    end

    features = cat(1,dH,dS,dV);
end

function d = diffCovDesc(reshapedWindow1,reshapedWindow2)
    C1 = cov(reshapedWindow1);
    C2 = cov(reshapedWindow2);
    lambda = eig(C1,C2);
    d = sqrt(sum(log(lambda(~isnan(lambda))).^2));
    d(isinf(d)) = 0;
end

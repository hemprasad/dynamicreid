function dist = calc_distance_mscr(dist,par)
%
% Computation of MSCR distances
% normY = normalization value for the Y position, usually height of image
% normC = normalization value for distances in CIELAB, usually 250
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

normY = par.normY;
normC = par.normC;
gamma = par.gamma;
numImg = length(dist.MSCR);
reg = makecform('srgb2lab');

skipDisplay = isfield(par,'NoDisplay') && par.NoDisplay;
if ~skipDisplay,
  hh = waitbar(0,'Computing MSCR distances...'); drawnow;
end

% retrieve the Y position of the blobs and convert all colors into LABCIE
dataf = struct('y', cell(1,numImg));
for i = 1:numImg
  dataf(i).y = dist.MSCR(i).mvec(3,:);
  dataf(i).C = applycform(dist.MSCR(i).pvec', reg)';
end

% compute every distance from image i to image j
dist.M = zeros(numImg);
for i = 1:(numImg-1)
	
  y_i = dataf(i).y; % position of blobs (row vector)
  C_i = dataf(i).C; % color of blobs (3xnumblobs)
  num_i = length(y_i); % number of blobs in image i
  Syy = sum(C_i.^2,1);
  
  % compare against remaining images
  for j = (i+1):numImg
    
    y_j = dataf(j).y; % position of blobs (row vector)
    C_j = dataf(j).C; % color of blobs (3xnum)
    num_j = length(y_j); % number of blobs in image j
    %dims = [num_j num_i]; % dist matrices are in size num_j X num_i
    
    % calc the normalized distances from every blob in i to every blob in j
    dist_y = abs( bsxfun(@minus,y_j',y_i) );
    Sxx = sum(C_j.^2,1);
    Sxy = C_j' * C_i;
    dist_color = sqrt(bsxfun(@plus,Sxx',bsxfun(@minus,Syy,2*Sxy)));
        
    % normalize the distances and cap at 1
    dist_y = dist_y / normY;
    dist_color = dist_color / normC;
    dist_y(dist_y > 1) = 1;
    dist_color(dist_color > 1) = 1;
    
    % calculate the combined distance y-color
    totdist_n = gamma * dist_y + (1-gamma) * dist_color;
    
    % find the closest blob matches row-wise and column-wise
    [~,match_j] = min(totdist_n,[],2);
    [~,match_i] = min(totdist_n,[],1);
    
    % find the matrix coordinates of the matches (sub2ind is slooooow)
    %indic_i = sub2ind(dims,match_i,1:num_i);
    %indic_j = sort(sub2ind(dims,1:num_j,match_j'));
    indic_j = sort((1:num_j) + (match_j'-1) * num_j);
    indic_i = match_i + (0:num_i-1) * num_j;
    
    % join the two list and remove duplicates
    matches = util.union_sorted(indic_j,indic_i);
    
    % final distance is the average of the best matches
    dist.M(i,j) = sum(totdist_n(matches)) / length(matches);
    dist.M(j,i) =  dist.M(i,j);
  end
  if ~skipDisplay, waitbar(i/(numImg-1),hh); end
end
if ~skipDisplay, close(hh); end;

function alpha = conv_gaussian_map(dims,MAP,Tmap,Gfilter,minp)
%
% This version receives only the MAP location of the starting limb.
%
% Perform these steps:
% - apply Tmap's first shift vector to the MAP estimate
% - perform the Gaussian convolution with Gfilter
% - apply Tmap's second map
% - normalize the output map
%
% This version uses non-clipping mappings
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
fdims = 2*dims;
xdims = [dims(1); dims(2); fdims(3)];
minds = [1;1;1];
crop = [Gfilter.halfy Gfilter.halfx Gfilter.halfr];

% Move the MAP location to the coordinate system of the joint
MAP = MAP + Tmap.V1(:,MAP(3)) + [0; 0; Gfilter.halfr];

% Check if the shift dropped the estimate outside the bounds, and, if so,
% move it back within the bounds
MAP = min(max(MAP,minds),xdims);

% Copy the filter cube so that its center is positioned at the MAP estimate
% location
ideal1 = MAP - Gfilter.Cmid;
ideal2 = MAP + Gfilter.Cmid;
mnw = max(ideal1,minds); % "northwest" corner of the maps
mse = min(ideal2,xdims); % "southeast" corner of the maps
cnw = mnw - ideal1 + minds; % "northwest" corner in the cube
cse = Gfilter.Cdim - ideal2 + mse; % "southeast" corner in the cube

% Copy (part/all) the filter cube in the correct position
Gamma = zeros(xdims');
Gamma(mnw(1):mse(1),mnw(2):mse(2),mnw(3):mse(3)) = ...
  Gfilter.Cube(cnw(1):cse(1),cnw(2):cse(2),cnw(3):cse(3));

% Calculate the upward message
alpha = zeros(dims);
alpha(Tmap.S2) = Gamma(Tmap.R2);

% normalize the output maps
alpha(alpha<minp) = minp;
alpha = alpha / sum(alpha(:));

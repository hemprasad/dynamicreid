function info = get_nodes(E)
% Parse the edge information to extract:
% R = root
% L = leaves
% P = parents
% C = children
%
% E = edges
%
% Copyright (C) 2013 Cheng Dong Seon

info.list = unique(E(:))';
info.L = setdiff(info.list,E(:,1)');
info.R = setdiff(info.list,E(:,2)');
info.P(E(:,2)) = E(:,1);
for n = info.list
  rows = E(:,1) == n;
  if any(rows)
    info.C{n} = E(rows,2)';
  else
    info.C{n} = [];
  end
end
info.num = length(info.list);

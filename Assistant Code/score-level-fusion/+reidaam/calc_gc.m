function data = calc_gc(data,par)
%
% This function calculates some auxiliary info to facilitate future color
% feature extraction.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

[H,W,~,num] = size(data.RGB); area = H*W;

% retrieve the quantization levels
qlevels = par.NBINs;

% convert the images to HSV
[hue,sat,val] = util.multirgb2hsv(data.RGB);
hue = reshape(hue,[area num]);
sat = reshape(sat,[area num]);
val = reshape(val,[area num]);

% correct the saturation of black pixels, so that it is never higher than
% the brightness level, to avoid weird dark pixels strongly hued
B = (val < par.ValBlack);
sat(B) = min(sat(B),val(B));

% retrieve the map of pixels considered to be graylevel: the black pixels
% + those with saturation less than the given threshold
data.G = B | (sat < par.SatGray);

% store the rescaled HSV values
data.H = hue * qlevels(1) - 0.5;
data.S = sat * qlevels(2) - 0.5;
data.V = val * qlevels(3);

% pre-calculate certain weights based on the given cutoff threshold:
% WpropSat is proportional to the saturation values with SatGray = 1, and
% it is meant to be used only for values < SatGray
data.WpropSat = sat / par.SatGray;

%% Create the GC histogram distance matrix
% generate the distance matrix for the graylevel histogram (it's a
% circulant matrix)
Dgrays = toeplitz([0 1 2*ones(1,qlevels(3)-2)]);

% the distance matrix for the colors is block-circulant with circulant
% blocks
Dcolor = 2*ones(qlevels(1)*qlevels(2));
block1_1 = toeplitz([0 1 2*ones(1,qlevels(1)-3) 1]);
block1_2 = toeplitz([1 sqrt(2) 2*ones(1,qlevels(1)-3) sqrt(2)]);
for cc = 1:qlevels(2)-1
  indic = (cc-1) * qlevels(1) + (1:qlevels(1));
  Dcolor(indic,indic) = block1_1;
  Dcolor(indic,indic + qlevels(1)) = block1_2;
  Dcolor(indic + qlevels(1),indic) = block1_2;
end
indic = (qlevels(2)-1) * qlevels(1) + (1:qlevels(1));
Dcolor(indic,indic) = block1_1;

% compute the bin-similarity matrices and combine them
Agrays = 1 - Dgrays / 2; % 2 is max(Dgrays(:))
Acolor = 1 - Dcolor / 2;
data.BinSim = blkdiag(Agrays,Acolor);

% calculate a scaling factor for the modified Bhattacharyya distance based
% on the self-similarity of a flat vector
vec = ones(1,size(data.BinSim,1));
vec = sqrt(vec / sum(vec));
data.BhattaNorm = vec * sqrt(data.BinSim) * vec';


function [mask,pmask] = calc_masks(BP,height,width,conf)
%
% Create masks for each body position estimate in BP
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
import reidaam.*;

[~,numparts,numimg] = size(BP);
scale = conf.parHOG.Scale;
scaledSize = ceil(scale * conf.part_dims)-1;
scaledCorner = -scaledSize / 2;

mask = cell(numimg,1);
pmask = cell(numimg,numparts);

for jj = 1:numimg
  
  F = zeros(height,width); % full segm area
    
  for pidx = 1:numparts
    iy = BP(1,pidx,jj);
    ix = BP(2,pidx,jj);
    ridx = BP(3,pidx,jj);
    rotAngle = conf.parHOG.Rots(ridx);
    
    FM = ones(scaledSize(pidx,[2 1])+1);
    
    % fold the part segmentation into the whole image
    partM = zeros(height,width);
    if rotAngle
      FBM = util.imrot_fftbased(FM,-rotAngle,0);
      FM = (FBM > 0.5);
      
      % approximate coordinates, problem is with the exact
      % transformation during rotation
      %rect = ([ix; iy]-1) + Rmat' * scaledCorner(pidx,:)' - iTvec - [2;1];
      rect(3) = size(FBM,2)-1;
      rect(4) = size(FBM,1)-1;
      rect(1) = ix - round(rect(3)/2);
      rect(2) = iy - round(rect(4)/2);
    else
      rect = [[ix iy-1]+scaledCorner(pidx,:) scaledSize(pidx,:)];
      rect = round(rect);
    end
    
    % check that paste rect is within image boundaries
    [phei,pwid] = size(FM); prect = [1 1 pwid-1 phei-1];
    if rect(1) < 1
      offset = 1 - rect(1);
      prect(1) = prect(1) + offset;
      prect(3) = prect(3) - offset;
      rect(1) = 1;
      rect(3) = rect(3) - offset;
    end
    if rect(2) < 1
      offset = 1 - rect(2);
      prect(2) = prect(2) + offset;
      prect(4) = prect(4) - offset;
      rect(2) = 1;
      rect(4) = rect(4) - offset;
    end
    if rect(1)+rect(3) > width
      prect(3) = prect(3) + width - (rect(1)+rect(3));
      rect(3) = width - rect(1);
    end
    if rect(2)+rect(4) > height
      prect(4) = prect(4) + height - (rect(2)+rect(4));
      rect(4) = height - rect(2);
    end
    rows = rect(2):rect(2)+rect(4);
    cols = rect(1):rect(1)+rect(3);
    prows = prect(2):prect(2)+prect(4);
    pcols = prect(1):prect(1)+prect(3);
    
    F(rows,cols) = F(rows,cols) | FM(prows,pcols);
    
    partM(rows,cols) = FM(prows,pcols);
    
    pmask{jj,pidx} = logical(partM);
  end

  mask{jj} = logical(F);
end

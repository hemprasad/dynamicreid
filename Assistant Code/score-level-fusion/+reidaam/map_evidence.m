function map_evidence(rgb,Mu,Cv,ped,conf,par)
% Calculates evidence maps from Gaussian appearance models
% - Mu{p,s} : pixel means
% - Cv{p,s} : pixel variances
% - conf : configuration info for the experiment
% - parts : configuration info for parts
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import reidaam.*;

[H,W,chans,~] = size(rgb);
numped = length(ped);
numparts = conf.nParts;
numrots = conf.parHOG.nRots;

% preparate data for parallel computing
PARC = struct([]);
for pedidx = 1:numped
  idxprob = ped(pedidx).probes;
  idxgall = ped(pedidx).gallery;
  probes = ped(pedidx).global(idxprob);
  gallery = ped(pedidx).global(idxgall);
  
  % retrieve the separate gallery and probe images
  PARC(pedidx).GALL = single(rgb(:,:,:,gallery));
  PARC(pedidx).PROB = single(rgb(:,:,:,probes));
  PARC(pedidx).GMu = Mu{pedidx,1};
  PARC(pedidx).GCv = Cv{pedidx,1};
  PARC(pedidx).PMu = Mu{pedidx,2};
  PARC(pedidx).PCv = Cv{pedidx,2};
end

% pre-calculate masks for rotated parts
rots = conf.parHOG.Rots;
pdims = round(conf.part_dims * conf.parHOG.Scale);
MASK = cell(numparts,numrots);
flipMASK = cell(numparts,numrots);
for pidx = 1:numparts
  for ridx = 1:numrots
    rotAngle = rots(ridx);
    
    M = ones(pdims(pidx,2),pdims(pidx,1));
    if rotAngle
      % create a rotated mask to select only valid pixels
      M = util.imrot_fftbased(M,-rotAngle,0);
      M = (M > 0.5);
    end
    MASK{pidx,ridx} = M(:,:,ones(1,1,chans));
    flipMASK{pidx,ridx} = M(end:-1:1,end:-1:1,ones(1,1,chans));
  end
end


% parallel computation
parfor pedidx = 1:numped
  numimg = length(ped(pedidx).global);
  idxprob = ped(pedidx).probes;
  idxgall = ped(pedidx).gallery;
  
  % calculate the evidence maps separately on gallery and probe set
  EVI = zeros(H,W,numrots,numparts,numimg,'single');
  EVI(:,:,:,:,idxgall) = ...
    calc_evidence(PARC(pedidx).GALL,PARC(pedidx).GMu,PARC(pedidx).GCv,...
                  MASK,flipMASK,conf,par);
  EVI(:,:,:,:,idxprob) = ...
    calc_evidence(PARC(pedidx).PROB,PARC(pedidx).PMu,PARC(pedidx).PCv,...
                  MASK,flipMASK,conf,par);
    
  % save the evidence maps
  pedpid = ped(pedidx).pid;
  fname = [conf.AppearanceDir '/' num2str(pedpid,'%04d') '-evidence.mat'];
  parsave(fname,EVI);
end
end

%%
% dummy function to allow saving data
function parsave(fname,EVI)
save(fname, 'EVI');
end
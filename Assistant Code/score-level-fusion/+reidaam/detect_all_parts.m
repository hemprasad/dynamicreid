function detect_all_parts(rgb,ped,conf,par)
%
% This function performs the detection of the individual parts, and applies
% any ROI filtering specified in the parameters.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
import partdetect.*;

%% Preliminaries
[H,W,~,~] = size(rgb);
nPed = length(ped);
nRots = conf.parHOG.nRots;
nParts = conf.nParts;
dirname = conf.DetectDir;
mindet = par.MinProb;

%% Run the part detectors
if ~isfield(par,'SkipDetect') || ~par.SkipDetect
  util.chkmkdir(conf.DetectDir);
  
  % splice the image data for parallel computing
  img = cell(1,nPed);
  for pedidx = 1:nPed
    globals = ped(pedidx).global;
    numimg = length(globals);
    img{pedidx} = zeros(H,W,3,numimg);
    for jj = 1:numimg
      img{pedidx}(:,:,:,jj) = im2single(rgb(:,:,:,globals(jj)));
    end
  end

  % process each pedestrian separately
  parfor pedidx = 1:nPed
    globals = ped(pedidx).global;
    numimg = length(globals);
    
    % process all the images for this pedestrian
    SCORE = zeros(H,W,nRots,nParts,numimg,'single');
    for jj = 1:numimg
      SCORE(:,:,:,:,jj) = detect_parts(img{pedidx}(:,:,:,jj),conf);
    end
    
    % save the probability maps
    fname = [dirname num2str(pedidx,'%04d') '-scores.mat'];
    util.savevars(fname,SCORE);
  end
end
fprintf('\nPre-Detection finished.\nGenerating masks...\n');

%% Filter the score grids
% The filters are universal in that they are meant to filter out detections
% outside certain ROIs, which are fixed and assume a fixed image size.
%
% First, by default all the detections that position a part too much (more
% than 10%) outside the image bounds are filtered out.
%
% Second, the user may provide additional constraints.
maxout = 10/100;
if isfield(par,'Filters')
  numfilters = length(par.Filters);
else
  numfilters = 0;
end

% create the filters
filterout = false(H,W,nRots,nParts);
dims = par.Scale * conf.part_dims;
border = round(dims * (0.5 - maxout));
for pidx = 1:nParts
  % filter out the border
  filterout(1:border(pidx,2),:,:,pidx) = true;
  filterout(end-border(pidx,2)+1:end,:,:,pidx) = true;
  filterout(:,1:border(pidx,1),:,pidx) = true;
  filterout(:,end-border(pidx,1)+1:end,:,pidx) = true;

  % filter out the user requests (as percentages in height after which we
  % should stop looking for those detections)
  if pidx <= numfilters
    stopheight = round(H * par.Filters{pidx});
    filterout(stopheight:end,:,:,pidx) = true;
  end
end

% process all the pedestrians independently to avoid overloading memory
for pedidx = 1:nPed
  if isfield(ped(pedidx),'run')
    allimgidx = ped(pedidx).run;
  else
    allimgidx = ped(pedidx).global;
  end
  numimg = length(allimgidx);

  % load all the detections for this pedestrian
  fname = [dirname num2str(pedidx,'%04d') '-scores.mat'];
  load(fname);
  
  for jj = 1:numimg
    % apply the filters
    scores = SCORE(:,:,:,:,jj);
    scores(filterout) = mindet;
    SCORE(:,:,:,:,jj) = scores;
  end
  
  % save the filtered scores
  save(fname,'SCORE');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOOTNOTES
%
% 23 Feb - Using the AAM HOG-LDA detectors

function MAP = run_pictorial(ny,nx,gr,Mj,nodes,Tmaps,Gfilters,par)
%
% Optimized version of the MATLAB-based Pictorial Structures algorithm.
% Basically, it is a message passing algorithm on the kinematic prior tree
% diagram of parts, with sum-product used on the forward (leaves-to-root)
% leg and max-product (root-to-leaves) used on the backward leg. For speed
% reasons.
%
% There are several speed-up optimizations probably not fully described.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import reidaam.*;

%% Preliminaries
% Retrieve the grid of configurations:
nr = length(gr);
minp = par.MinProb;

%% Main algorithm (optimized steps)
% Initialize the storage for all the messages
Beta = cell(nodes.num);
Alpha = cell(nodes.num);
dims = [ny nx nr];
MAP = zeros(3,nodes.num);

% normalize the Mj
% for j = nodes.list
%   mj = Mj(:,:,:,j);
%   Mj(:,:,:,j) = max(mj / sum(mj(:)),minp);
% end

%% 1. Leaves
% Process all leaves j, store all parents i in a queue, create all the
% upward messages $\alpha_{ji}$.
Que = [];
for j = nodes.L
  % Get parent $i\rightarrow j$ and store it in the queue:
  i = nodes.P(j);
  Que = [i Que];
  
  % Perform the Gaussian convolution in the joint coordinate system:
  Alpha{j,i} = conv_gaussian(Mj(:,:,:,j),Tmaps(j,i),Gfilters{i,j},minp);  
end

%% 2. Non-leaves, non-root
% Process all non-leaves, non-root nodes j ordered from leaves to root,
% with parent i and children k such that $i\rightarrow j\rightarrow k$.
Que = setdiff(Que,nodes.R);
while ~isempty(Que)
  % Deque the current node and get its parent node.
  % Stop when we reach the root of the tree.
  % Que the parent node, if not already present
  j = Que(1); i = nodes.P(j);
  Que = Que(2:end);
  if i ~= nodes.R && ~any(ismember(Que,i)), Que = [Que i]; end
  
  % If the current node has children in the queue, then re-que at the end
  % and restart, since we first need the data from its children
  if any(ismember(Que,nodes.C{j}))
    Que = [Que j];
    continue;
  end
  
  % Calculate the first upward message:
  rp = log(Mj(:,:,:,j));
  for k = nodes.C{j}
    rp = rp + log(Alpha{k,j});
  end
  Beta{j,i} = exp(rp - max(rp(:)));
  
  % Perform the Gaussian convolution in the joint coordinate system:
  Alpha{j,i} = conv_gaussian(Beta{j,i},Tmaps(j,i),Gfilters{i,j},minp);
end

%% 3. Root
% Calculate the marginal distribution at the root and its downward going
% messages $\beta_{1j}$ and $\alpha_{1j}$.
rp = log(Mj(:,:,:,nodes.R));
for j = nodes.C{nodes.R}
  rp = rp + log(Alpha{j,nodes.R});
end

% keep only the max peak
[~,maxind] = max(rp(:));
[i1,i2,i3] = ind2sub([ny nx nr],maxind);
MAP(:,nodes.R) = [i1 i2 i3];

for j = nodes.C{nodes.R}
  % Perform the Gaussian convolution in the joint coordinate system:
  Alpha{nodes.R,j} = conv_gaussian_map(dims,MAP(:,nodes.R),...
    Tmaps(nodes.R,j),Gfilters{nodes.R,j},minp);
end

%% 4. Non-leaves, non-root
% Process all non-leaves, non-root nodes j ordered from root to leaves,
% with parent i and children k such that $i\rightarrow j\rightarrow k$.
Que = setdiff(nodes.C{nodes.R},nodes.L);
while ~isempty(Que)
  % Deque the current node and get its parent node:
  j = Que(1); i = nodes.P(j);
  Que = Que(2:end);
  
  % Calculate the marginal distribution:
  BA = Beta{j,i} .* Alpha{i,j};
  
  % keep only the max peak
  [~,maxind] = max(BA(:));
  [i1,i2,i3] = ind2sub([ny nx nr],maxind);
  MAP(:,j) = [i1 i2 i3];
  
  % Process all links to the children
  for k = nodes.C{j};
    % add non-leaves to the queue
    if ~any(ismember(nodes.L,k)), Que = [Que k]; end
    
    % Perform the Gaussian convolution in the joint coordinate system:
    Alpha{j,k} = conv_gaussian_map(dims,MAP(:,j),Tmaps(j,k),...
      Gfilters{j,k},minp);
  end 
end

%% 5. Leaves
% Calculate the marginal distributions of the leaf nodes.
for j = nodes.L
  i = nodes.P(j);
  BA = Mj(:,:,:,j) .* Alpha{i,j};
  [~,maxind] = max(BA(:));
  [i1,i2,i3] = ind2sub([ny nx nr],maxind);
  MAP(:,j) = [i1 i2 i3];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOOTNOTES
%
% 26 Oct 12: moved Tji + Gconv + Tij^(-1) in a single function
% 18 Jan 13: returns the MAP estimates, not the full prob maps since now
%            we are using chaining the MAP peaks, the P are not
%             informative anymore
% 18 Jan 13: many optimizations performed, also uses some tricks from the
%            lightspeed toolbox (probably required)

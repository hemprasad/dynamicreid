function T = calc_transformations(prior,gy,gx,gr)
%
% This version create transformations that avoid clipping as much as
% possible, by assuming a larger rot space at the joint coordinates.
%
% That is:
%            R1:ny x nx x nr   --> S1:ny x nx x 2*nr
%            R2:ny x nx x 2*nr --> S2:ny x nx x nr
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
import reidaam.*;

T = struct([]);

ny = length(gy); dy = gy(2) - gy(1);
nx = length(gx); dx = gx(2) - gx(1);
nr = length(gr); dr = gr(2) - gr(1);
step = [dx; dy];
rsteps = gr / dr; % rotation steps in terms of maps

% normal and extended space dimensions
dims = [ny nx nr];
xdims = [ny nx 2*nr];
xshift = floor(nr/2); % amount of shift for extra space

% retrieve the rotation matrices for each angle
ROT = cell(nr,1);
for rr = 1:nr
  ROT{rr} = [cosd(gr(rr)) -sind(gr(rr)); sind(gr(rr))  cosd(gr(rr))];
end

% extend the rotation angles to length 2*nr
% grext = dr * floor(-(2*nr-1)/2):floor((2*nr-1)/2);
grext = dr * (2*rsteps(1):(2*nr+2*rsteps(1)-1));
ROText = cell(2*nr,1);
for rr = 1:2*nr
  ROText{rr} = [cosd(grext(rr)) -sind(grext(rr)); ...
                sind(grext(rr))  cosd(grext(rr))];
end

% process all joints
for joint = prior.edges'
  i = joint(1);
  j = joint(2);

  Xij = [prior.xij(i,j); prior.yij(i,j)];
  Xji = [prior.xij(j,i); prior.yij(j,i)];
  thij = prior.theta_ij(i,j) * 180/pi;
  R = [cosd(thij) -sind(thij); sind(thij) cosd(thij)];

  % check the rotation shift
  rshift = round(thij / dr);

  %% Tij + Tji^(-1)
  % the combined shift vector from the pov of part i is given by
  %
  %                       Sij = Xij - R*Xji
  %
  % where T(i,j).R1 --> T(i,j).S1 contain the shift of Xij
  %       T(i,j).R2 --> T(i,j).S2 contain the shift of -R*Xji
  %
  [T(i,j).R1,T(i,j).S1,T(i,j).V1] = make_mapping(dims,xdims,Xij,0,step,ROT);
  [T(i,j).R2,T(i,j).S2,T(i,j).V2] = ...
    make_mapping(xdims,dims,-R*Xji,rshift-xshift,step,ROText);
  
  %% Tji + Tij^(-1)
  % the combined shift vector from the pov of part i is given by
  %
  %                       Sji = Xji - R'*Xij
  %
  % where T(j,i).R1 --> T(j,i).S1 contain the shift of Xji
  %       T(j,i).R2 --> T(j,i).S2 contain the shift of -R'*Xij
  %
  [T(j,i).R1,T(j,i).S1,T(j,i).V1] = make_mapping(dims,xdims,Xji,0,step,ROT);
  [T(j,i).R2,T(j,i).S2,T(j,i).V2] = ...
    make_mapping(xdims,dims,-R'*Xij,-rshift-xshift,step,ROText);
end

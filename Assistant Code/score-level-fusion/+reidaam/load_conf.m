function conf = load_conf(parCPS)
% conf = load_conf(parCPS)
%
% Create a PS/CPS configuration struct loading the necessary information.
%
% Copyright (C) 2013 Cheng Dong Seon

% load the prior
conf = load(['MAT/' parCPS.BodyConf '.mat']);
conf.nParts = length(conf.prior.names);

% load the HOG parameters
load('MAT/trainpar.mat');
conf.parHOG = par;

% load  the LDA models
load('MAT/LDAmodel.mat');
if isfield(conf.prior,'PartsKept')
  model = model(conf.prior.PartsKept);
end
conf.parLDA = model;


% copy some info from the params
conf.parHOG.nRots = length(parCPS.Rots);
conf.parHOG.Rots = parCPS.Rots;
conf.parHOG.WindowStep = parCPS.WindowStep;
conf.parHOG.Scale = parCPS.Scale;

conf.PoseDir = [parCPS.Dir '/pose_estimations/'];
conf.AppearanceDir = [parCPS.Dir '/appearance_models'];

% check if user wants to reuse PS detections and estimates from a previous
% different experiment
if isfield(parCPS,'ReusePS')
  util.chkmkdir(conf.PoseDir);
  conf.DetectDir = [parCPS.ReusePS '/part_detections/'];
  copyfile([parCPS.ReusePS '/pose_estimations/MAPestimates.mat'],...
    [conf.PoseDir 'MAPestimates.mat']);
  copyfile([parCPS.ReusePS '/PSmasks.mat'],[parCPS.Dir '/PSmasks.mat']);
else
  conf.DetectDir = [parCPS.Dir '/part_detections/'];
end

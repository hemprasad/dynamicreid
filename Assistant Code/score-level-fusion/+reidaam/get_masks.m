function [maskset,pmskset] = get_masks(rgb,dset,ped,expr,par,nRun)
%
% Load pre-computed masks or create new masks of the pedestrian images
% using PS and CPS
%
% Copyright (C) 2013 Cheng Dong Seon
import reidaam.*;

[H,W,~,~] = size(rgb);

% with this flag, the user can force to re-compute everything by ignoring
% the saved files
if ~isfield(par,'IgnoreSavefiles')
  par.IgnoreSavefiles = false;
end
if ~isfield(par,'ContinueIters')
  par.ContinueIters = false;
end

% load/create the set of masks
namefile = [par.Dir '/maskset_' num2str(nRun,'%03d') '.mat'];
if exist(namefile,'file') && ~par.IgnoreSavefiles && ~par.ContinueIters
  load(namefile);
  disp('Masks loaded from file.');
else
  % generate masks with pictorial structures
  [msk,partmsk] = make_PSmasks(rgb,dset,par);
  
  if strcmp(expr.Mask,'CPS')
    % correct the PS estimates with CPS
    [msk,partmsk] = make_CPSmasks(rgb,ped,nRun,par);
  end
  
  % process the PS/CPS masks
  [pedImages,~] = size(partmsk);
  maskset = false(H,W,pedImages);
  for ii = 1:pedImages
    maskset(:,:,ii) = msk{ii};
  end
  pmskset = partmsk;
  
  save(namefile,'maskset','pmskset','expr','par');
  disp('Masks calculated.');
end



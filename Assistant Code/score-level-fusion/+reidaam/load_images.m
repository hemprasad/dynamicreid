function dataset = load_images(data,H,W,namefile)
% dataset = load_images(data,H,W,namefile)
%
% Load and resize the images from the given directory and list.
% Save the dataset in the given file and return it.
%
% Copyright (C) 2013 Cheng Dong Seon

nImages = length(data.Files);

if exist(namefile,'file')
    load(namefile);
    fprintf('Dataset loaded from file.\n');
else
    hwait = waitbar(0,'Loading images...');
    
    dataset = zeros([H W 3 nImages],'uint8');
    for ii = 1:nImages
        img = imread(strcat(data.Dir,'/',data.Files(ii).name));
        img = imresize(img,[H W]);
        dataset(:,:,:,ii) = img;
        waitbar(ii/nImages,hwait);
    end
    close(hwait);drawnow;
    save(namefile,'dataset');
    fprintf('Images loaded.\n');
end

%% Sub-dataset testing
p = 10;
gID_test = gID(idx_test);
gID_prob = gID_test(~ix_test_gallery(p,:));
gID_gal = gID_test(ix_test_gallery(p,:));
camID_test = camID(idx_test);
cam_gal = camID_test(ix_test_gallery(p,:));
cam_prob = camID_test(~ix_test_gallery(p,:));

%         showID = 256;
topidx = Qxx(61,1:end);
%         topidx([26 31]) = [];
topcamID = cam_gal(topidx);
topID = gID_gal(topidx);
%         testID = sort([showID topID]);
testID = sort(topID);
num_f = 3;
subtest = test{num_f};
gID_test = gID(idx_test);
tmptest = [];
tmptest = [subtest(ismember(gID(idx_test),testID)&camID(idx_test)==1,:);...
    subtest(ismember(gID(idx_test),testID)&camID(idx_test)==2,:)];
sub_ix_gallery = zeros(numel(testID),size(tmptest,1));
camg=2;
tmp_ix_gal = ones(numel(testID),numel(testID));
tmp_ix_prob = zeros(numel(testID),numel(testID));
for ip = 1:numel(testID)
    tmp_ix_prob(ip,ip) = -1;
end
switch camg
    case 1
        sub_ix_gallery = [tmp_ix_gal tmp_ix_prob];
    case 2
        sub_ix_gallery = [tmp_ix_prob tmp_ix_gal];
end
[subr, subdis, subQxx] = compute_rank2(algo(num_f), train(num_f), {tmptest},...
    sub_ix_gallery, gID_test(ismember(gID(idx_test),testID)));
switch num_f
    case 1
        subr_app = subr;
    case 2
        subr_dyn = subr;
    case 3 
        subr_oFV = subr;
end
%%
% show paritcualr video/images
numshow = 10;
showID = [7 22 217];
for s = 1:numel(showID)
    [subr, subdis, subQxx] = compute_rank2(algo(num_f), train(num_f), {tmptest},...
    sub_ix_gallery(testID==showID(s),:), gID_test(ismember(gID(idx_test),testID)));
%     if subr < numshow
        subtestID = testID(subQxx(1:numshow));
%     else 
%         subtestID = [testID(subQxx(1:numshow-1)) showID];
%     end
    switch camg
        case 1
            seq_prob = I{gID==showID(s) & camID==2};
        case 2
            seq_prob = I{gID==showID(s) & camID==1};
    end
    
    im_prob = seq_prob{round(numel(seq_prob)/2)};
    im_pad = zeros(128,10,3);
    im_exp = cat(2,im_prob,im_pad);
    for id = 1:numel(subtestID)
        tmpI = I{gID==subtestID(id)&camID==camg};
        im_exp = cat(2,im_exp,tmpI{round(numel(tmpI)/2)});
    end
    if ismember(showID(s),subtestID)
        switch num_f
            case 1
                imwrite(im_exp,['example_' num2str(showID(s)) '_app_rank' num2str(subr) '.png']);
            case 2 
                imwrite(im_exp,['example_' num2str(showID(s)) '_dyn_rank' num2str(subr) '.png']);
        end
    else
        num_col = 20;
        subtestID = testID(subQxx(1:subr));
        displayID = [showID(s) subtestID];
        max_row = ceil(numel(displayID)/num_col);
        im_exp = zeros(128*max_row,64*num_col,3);
        for id = 1:numel(displayID)
            tmp_row = ceil(id/num_col);
            tmp_col = mod(id,num_col);
            if tmp_col==0
                tmp_col=num_col;
            end
            if id == 1
                tmpI = seq_prob;
            else
                tmpI = I{gID==displayID(id)&camID==camg};
            end            
            tmpI = tmpI{round(numel(tmpI)/2)};
            im_exp((tmp_row-1)*128+1:tmp_row*128,(tmp_col-1)*64+1:tmp_col*64,:) = tmpI;
        end        
        switch num_f
            case 1
                imwrite(uint8(im_exp),['example_' num2str(showID(s)) '_app_rank' num2str(subr) '.png']);
            case 2 
                imwrite(uint8(im_exp),['example_' num2str(showID(s)) '_dyn_rank' num2str(subr) '.png']);
        end
    end
    
% make video
    tmpSeq = {};
    subtestID = testID(subQxx(1:numshow));
    for id = 1:numel(subtestID)
        tmpSeq{id} = I{gID==subtestID(id)&camID==camg};
        %             tmpSeq{id} = I{gID==badID(id)&camID==badCam(id)};
    end
    numcell = cellfun(@numel,[{seq_prob} tmpSeq]);
    n_frame = min(numcell);    
    switch num_f
        case 1
            writerObj = VideoWriter(['example_' num2str(showID(s)) '_app_rank' num2str(subr) '.avi']);
        case 2
            writerObj = VideoWriter(['example_' num2str(showID(s)) '_dyn_rank' num2str(subr) '.avi']);
    end
    open(writerObj);
    if ismember(showID(s),subtestID)
        for f = 1:n_frame
            tmpIm = cat(2,seq_prob{f},im_pad);
            for id = 1:numel(subtestID)
                tmpIm = cat(2,tmpIm,tmpSeq{id}{f});
            end
            writeVideo(writerObj,tmpIm);
        end
    else
        tmpSeq = {};
        subtestID = testID(subQxx(1:subr));
        displayID = [showID(s) subtestID];
        for id = 1:numel(subtestID)
            tmpSeq{id} = I{gID==subtestID(id)&camID==camg};
        %             tmpSeq{id} = I{gID==badID(id)&camID==badCam(id)};
        end
        tmpSeq = [{seq_prob},tmpSeq];        
        numcell = cellfun(@numel,[{seq_prob} tmpSeq]);
        n_frame = min(numcell); 
        for f = 1:n_frame
            num_col = 20;            
            max_row = ceil(numel(displayID)/num_col);
            im_exp = zeros(128*max_row,64*num_col,3);
            for id = 1:numel(displayID)
                tmp_row = ceil(id/num_col);
                tmp_col = mod(id,num_col);
                if tmp_col==0
                    tmp_col=num_col;
                end
                if id == 1
                    tmpI = seq_prob;
                else
                    tmpI = I{gID==displayID(id)&camID==camg};
                end            
                tmpI = tmpI{f};
                im_exp((tmp_row-1)*128+1:tmp_row*128,(tmp_col-1)*64+1:tmp_col*64,:) = tmpI;
            end   
            writeVideo(writerObj,uint8(im_exp));
        end
    close(writerObj);
    end
end
%% Plot 3 figures
subr_best = min(subr_app,subr_dyn);
% CMC curve
[a_best, b] = hist(subr_best',1:max(size(subr_best,1),100));
[a_app, b] = hist(subr_app',1:max(size(subr_app,1),100));
[a_dyn, b] = hist(subr_dyn',1:max(size(subr_dyn,1),100));
cum_best = cumsum(a_best)./numel(a_best);
cum_app = cumsum(a_app)./numel(a_app);
cum_dyn = cumsum(a_dyn)./numel(a_dyn);
figure,hold on;
hb = plot(cum_best,'k');
ha = plot(cum_app);
hd = plot(cum_dyn,'r');
plot(1,cum_dyn(1),'r*')
plot(1,cum_app(1),'*')
plot(1,cum_best(1),'k*')
legend([hb,ha,hd],{'Best Selection','Appearance','Dynamic'});
title('CMC curve from iLIDS-VID dataset');
xlabel('Rank');
ylabel('Percentage');
ylim([0,1.01]);
% Correlation figure
figure,plot(subr_app,subr_dyn,'*')
hold on
plot(0:0.01:150,0:0.01:150,'r')
title('Correlation between dynamic and appearance')
xlabel('Appearance Rank')
ylabel('Dynamic Rank')
% Rank figure
figure,plot(subr_app)
hold on
plot(subr_dyn,'g')
plot(subr_best,'r')
legend({'Appearance','Dynamic','Best Selection'})
title('Rank results for each sample')
xlabel('Sample Index')
ylabel('Rank')
%% Plot top 10 gallery samples
subtest = test{1};
p = 10;
gID_test = gID(idx_test);
gID_prob = gID_test(~ix_test_gallery(p,:));
gID_gal = gID_test(ix_test_gallery(p,:));
camID_test = camID(idx_test);
cam_gal = camID_test(ix_test_gallery(p,:));
cam_prob = camID_test(~ix_test_gallery(p,:));
showID = 256;
topidx = Qxx((gID_prob==showID),1:10);
topidx = [topidx Qxx((gID_prob==showID),r(p,gID_prob==showID))];
topcamID = cam_gal(topidx);
topID = gID_gal(topidx);

subtest = test{2};
expDis = [];
expidx = [];
figure,hold on;
count = 1;
for showID = [233   285   173   256   253   227    65   311   306   270   238]
    testID = [showID topID];
    tmptest = [];
    tmptest = [tmptest;subtest(~ix_test_gallery(p,:)&gID_test==showID,:)];
    for id = 1:numel(topID)
        tmptest = [tmptest;subtest(ix_test_gallery(p,:)&gID_test==topID(id),:)];
    end
    sub_ix_gallery = logical([0 ones(1,11)]);
    [subr, subdis, subQxx] = compute_rank2(algo(2), train(2), {tmptest}, sub_ix_gallery, testID);
    expgalID = testID(sub_ix_gallery);
    expDis = [expDis;subdis{1}(subQxx)];
    expidx = [expidx;expgalID(subQxx)];
    plot(subdis{1}(subQxx),'Color',CM(count,:));
    text(count,expDis(count,count),num2str(showID));
    count = count + 1;
    gID_prob = testID(2:end);
    topID = gID_prob(subQxx);
    topcamID = topcamID(subQxx);
    I_test = I(idx_test);
    seq_prob = I_test{gID_test==showID & ~ix_test_gallery(p,:)};
    im_prob = seq_prob{round(numel(seq_prob)/2)};
    im_pad = zeros(128,10,3);
    im_exp = cat(2,im_prob,im_pad);
    for id = 1:11
        tmpI = I{gID==topID(id)&camID==topcamID(id)};
        im_exp = cat(2,im_exp,tmpI{round(numel(tmpI)/2)});
    end
    if ismember(showID,topID)
        imwrite(im_exp,['example_' num2str(showID) '_app_rank' num2str(subr) '.png']);
    else
        correct_rank = r(10,gID_prob==showID);
        correct_cam = cam_gal(gID_gal == showID);
        correctSeq = I{gID==showID&camID==correct_cam};
        im_exp = cat(2,im_exp,im_pad,correctSeq{round(numel(correctSeq)/2)});
        imwrite(im_exp,['example_' num2str(showID) '_rank_' num2str(correct_rank) '.png'])
    end
    tmpSeq = {};
    for id = 1:11
        tmpSeq{id} = I{gID==topID(id)&camID==topcamID(id)};
        %             tmpSeq{id} = I{gID==badID(id)&camID==badCam(id)};
    end
    numcell = cellfun(@numel,[{seq_prob} tmpSeq]);
    n_frame = min(numcell);
    writerObj = VideoWriter(['example_' num2str(showID) '_app_rank' num2str(subr) '.avi']);
    open(writerObj);
    for f = 1:n_frame
        tmpIm = cat(2,seq_prob{f},im_pad);
        for id = 1:11
            tmpIm = cat(2,tmpIm,tmpSeq{id}{f});
        end
        writeVideo(writerObj,tmpIm);
    end
    close(writerObj);
end

%% misc
subtest = test{1};
gID_test = gID(idx_test);
tmptest = [];
tmptest = [subtest(ismember(gID(idx_test),testID)&camID(idx_test)==1,:);...
    subtest(ismember(gID(idx_test),testID)&camID(idx_test)==2,:)];
sub_ix_gallery = zeros(numel(testID),size(tmptest,1));
camg=2;
tmp_ix_gal = ones(numel(testID),numel(testID));
tmp_ix_prob = zeros(numel(testID),numel(testID));
for ip = 1:numel(testID)
    tmp_ix_prob(ip,ip) = -1;
end
switch camg
    case 1
        sub_ix_gallery = [tmp_ix_gal tmp_ix_prob];
    case 2
        sub_ix_gallery = [tmp_ix_prob tmp_ix_gal];
end
[subr, subdis, subQxx] = compute_rank2(algo(1), train(1), {tmptest},...
    sub_ix_gallery, gID_test(ismember(gID(idx_test),testID)));

%% Generate dynamic visual video
load('slideTracklet_256.mat');
load('center_iLIDSVID_p1.mat');
for c = 1:numel(unique(camID))
    writerObj = VideoWriter(['dynVisual_256_cam' num2str(c) '.avi']);
    writerObj.FrameRate = 10;
    open(writerObj);
    camTrj = denseTrj(camID==c);
    for d = 1:numel(camTrj)
        [ dynMat ] = dynamicVisual_wrap(camTrj{d},trainCenter,8);
        hf = figure(1);
        imagesc(dynMat);
        axis equal
        xlim([0,size(dynMat,2)]);
        drawnow;
        %         saveas(hf,['Saved_fig\256_cam' num2str(camID(d)) '_' sprintf('%03d',d) '.png']);
        frame = getframe(hf);
        writeVideo(writerObj,frame.cdata);
    end
    close(writerObj);
end

%%

clc
clear
%% Finding all black persons for machine
l1 = load('Assistant Code\label_1.mat');
l2 = load('Assistant Code\label_2.mat');
l3 = load('Assistant Code\label_3.mat');

bk_human = l1.label_re==1 & l2.label_re==1 & l3.label_re==1;
load('dataset/iLIDSVID_Images_Tracklets_l15.mat');
load('Feature/iLIDSVID_Hist75Patch.mat');

%% Parameter setting and pre processing
tmpFeat = normc_safe(FeatureAppearence');
Feature = tmpFeat';
AlgoOption.beta =0.01;
AlgoOption.d =40;
AlgoOption.LocalScalingNeighbor =6; % local scaling affinity matrix parameter.
AlgoOption.kernel = 'linear';
AlgoOption.epsilon =1e-4;
AlgoOption.dataname ='iLIDSVID';

gID_allbk = gID(bk_human);
uID_allbk = unique(gID_allbk);

order_allbk = zeros(numel(uID_allbk),sum(camID==1));
dis_allbk = zeros(numel(uID_allbk),sum(camID==1));
for n = 1:numel(uID_allbk)
%% Generate partition
    uID = unique(gID);
    testID = uID_allbk(n);
    trainID = uID(~ismember(uID,testID));
    train = Feature(ismember(gID,trainID),:);
    test_cam1 = [Feature(gID==testID & camID == 1,:);
                 Feature(camID == 2,:)];
    test_cam2 = [Feature(gID==testID & camID == 2,:);
                 Feature(camID == 1,:)];
    idx_gallery_cam1 = logical([0,ones(1,sum(camID==2))]);
    idx_gallery_cam2 = logical([0,ones(1,sum(camID==1))]);
    %% training
    [algo, V]= LFDA(double(train),gID(ismember(gID,trainID))' ,AlgoOption);

    %% distance calculation
    K_test = ComputeKernelTest(train,test_cam1,algo);
    K_prob = K_test(:,~idx_gallery_cam1);
    K_gal = K_test(:,idx_gallery_cam1);
    dis1 = bsxfun(@minus, K_gal, K_prob);
    dis1 = algo.P*dis1;
    dis1 = sqrt(sum(dis1.^2,1));

    K_test = ComputeKernelTest(train,test_cam2,algo);
    K_prob = K_test(:,~idx_gallery_cam2);
    K_gal = K_test(:,idx_gallery_cam2);
    dis2 = bsxfun(@minus, K_gal, K_prob);
    dis2 = algo.P*dis2;
    dis2 = sqrt(sum(dis2.^2,1));
    
    dis = dis1 + dis2;
    [~,order] = sort(dis);
    order_allbk(n,:) = order;
    dis_allbk(n,:) = dis;
end

%% sub dataset extraction
num_keep = 150;

dismax = max(dis_allbk);
[~,ordermax] = sort(dismax);
id_sub = ordermax(1:num_keep);
idx_sub = ismember([1:300 1:300],id_sub);

gID = gID(idx_sub);
I = I(idx_sub);
camID = camID(idx_sub);
denseTrj = denseTrj(idx_sub);

FeatureAppearence = FeatureAppearence(idx_sub,:);
save(sprintf('dataset/iLIDSVID_Images_Tracklets_l15_allbk_%d.mat',num_keep),'gID','I','camID','denseTrj');
save(sprintf('Feature/iLIDSVID_Hist75Patch_allbk_%d.mat',num_keep),'FeatureAppearence');

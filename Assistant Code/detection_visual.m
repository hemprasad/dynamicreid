function im_exp = detection_visual(I,num_col,max_disp)
% max_disp = 300;
if numel(I) > max_disp
    I = I(1:max_disp);
end    
% num_col = 30;
% subtestID = testID(subQxx(1:subr));
% displayID = [showID(s) subtestID];
max_row = ceil(numel(numel(I))/num_col);
im_exp = zeros(128*max_row,64*num_col,3);
for id = 1:numel(I)
    tmp_row = ceil(id/num_col);
    tmp_col = mod(id,num_col);
    if tmp_col==0
        tmp_col=num_col;
    end
    tmpI = imresize(I{id}{5},[128,64]);
%     if id == 1
%         tmpI = seq_prob;
%     else
%         tmpI = I{gID==displayID(id)&camID==camg};
%     end            
%     tmpI = tmpI{round(numel(tmpI)/2)};
    im_exp((tmp_row-1)*128+1:tmp_row*128,(tmp_col-1)*64+1:tmp_col*64,:) = tmpI;
end  
im_exp = uint8(im_exp);
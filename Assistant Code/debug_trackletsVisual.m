% function debug_trackletsVisual(showID,label)
load('dataset\iLIDSVID_Images_Tracklets.mat');
% load('dataset\PRID_Images_Tracklets_l15.mat');
% load('dataset\CLE_Images_Tracklets_l15.mat');
if nargin < 2
    label = ones(1,size(denseTrj{showID},1));
end
seqs = I{showID};
trjs = denseTrj{showID};
trjX = trjs(:,2:2:end);
trjY = trjs(:,3:2:end);

imsiz = [343,170];
numf = trjs(:,1);
writerObj = VideoWriter(['tracklets_person' num2str(gID(showID)),...
                        '_cam' num2str(camID(showID))]);
open(writerObj);
for f = 1:numel(seqs)%min(numf):max(numf)
    tmpI = seqs{f};
    tmpX = trjs(numf==f,2:2:end);
    tmpY = trjs(numf==f,3:2:end);
    
    figure(1);
    imagesc(tmpI);    
    axis equal;
    xlim([min(min(trjX)),max(max(trjX))]);
    ylim([min(min(trjY)),max(max(trjY))]);    
% set(gcf,'YDir','reverse');
    hold on;
    plot(tmpX',tmpY','r');  
    
%     xlim([1,64]);
%     ylim([1,128]);
    drawnow;
    frame = getframe();
%     tmpframe = imresize(frame.cdata,imsiz);
    writeVideo(writerObj,frame.cdata);
    hold off
end
close(writerObj);


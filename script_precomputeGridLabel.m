clc
clear 
%% 
dataname = 'iLIDSVID_Images_Tracklets_l15_allbk_97.mat';
load(['Dataset/' dataname]);
gridlabel = cell(1,numel(denseTrj));
labelmatCell = cell(numel(denseTrj),1);
nr = 5;
nc = 3;
for i = 1:numel(denseTrj)
    if mod(i,round(numel(denseTrj)/10))==1
        fprintf('.')
    end
    [denseTrj{i},gridlabel{i},labelmatCell{i}] = assignGridLabel(denseTrj{i}, [1 1 64 128], nr, nc);
end

labelmat = cell2mat(labelmatCell);
[numlabelPerI,~] = cellfun(@size, labelmatCell,'UniformOutput',1);
[numTrjPerI,~] = cellfun(@size, denseTrj,'UniformOutput',1);
plot(numlabelPerI-numTrjPerI');
%% 
save(dataname,'I','gID','camID','denseTrj','labelmatCell');
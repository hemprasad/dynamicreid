function endP = EndPtNormalBBox(oriendP, oriSz, normSz)
% oriSz - WxH (XxY)
% oriendP - Nx2 [x,y]
oriendP = ceil(oriendP);
XtranTable = linspace(1,normSz(2),oriSz(2))';
YtranTable = linspace(1,normSz(1),oriSz(1))';

endP(:,1) = XtranTable(oriendP(:,1));
endP(:,2) = YtranTable(oriendP(:,2));
endP = round(endP);
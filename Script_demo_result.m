% Script Showing Result
% clc
% clear
% close all;
dropbox_folder =  pwd;%'C:\Users\Mengran\Dropbox\Fei_Mengran\Matlab Code\ReID';
patition_folder = [dropbox_folder '/Feature/'];
result_folder = pwd;
% result_folder = 'C:\Users\Mengran\Research\code\ReID\Backup\ECCV14\result';
clear rrr rrrr
%%
% figure
% clf;
num_patch = [75];% 341 6 14 75
flag_preFilter = 0;
feat_cha = 'LDFV'; %'HistPD' %'HistMoment' Hist
partition_name = 'DVR';  %'Random'; %'SDALF'; %
algoname = {'LFDA'}; % 'PCCA','oLFDA','svmml','KISSME','rPCCA','LFDA','MFA'
kname={'linear'}; %  'linear','chi2' 'chi2-rbf'
dynamic = 'woDynamic';%'wDynamic';%'wDynamic_vel';
color={ 'c','g', 'b','r','k','m','y'};
linesty ={'o-.','d--','*-'};
% cnt=1;
datasetname = {'iLIDSVID'}; %'VIPeR', 'iLIDS' 'RPIReID';
datasetProfix = [];%'_allbk_150';%'_allbk_89';%'_allbk_89';%;%'_allbk_150';
rrrr=[];
rrrr_std = [];
cmc = [];
h(1) = figure();
% %--------parameters tuning--------%
for p = 0.2
% %---------------------------------%
    
clear M Method
clear result_name
for kkk=1: length(datasetname)
    dataset_name = datasetname{kkk};
    load([patition_folder dataset_name '_Partition_' partition_name datasetProfix '.mat']);
    num_ref = sum(Partition(1).ix_test_gallery(1,:));
%     h(kkk) = figure();
    cnt=1;
    
    for ixp =1:length(num_patch)
        %     figure;
        np =num_patch(ixp);
        
        for m = 1: length(algoname)
            for k =1:length(kname)
                kname{k};
                if np == 0
                    Featurename = [dataset_name '_UIUC'];
                else
%                     Featurename = [dataset_name '_' feat_cha num2str(num_patch) 'Patch' datasetProfix '_' dynamic '_'];
%                     Featurename = [dataset_name '_ColorLBP' datasetProfix '_' dynamic '_'];
%                     Featurename = [dataset_name '_HOG3D' datasetProfix '_' dynamic '_'];
%                     Featurename = [dataset_name '_dyn3sl' datasetProfix '_'];
                    featuse = {};
                    featuse = [featuse, [dataset_name '_dyn3sl' datasetProfix]];
                    featuse = [featuse, [dataset_name '_LDFV75Patch64' datasetProfix]];
                    Featurename = [cell2mat(featuse) '_wdyn' num2str(p) '_'];
                end
                parafix = 'ncenter12_s6_wdyn0.1_';%['mode1_stdnorm_ncenter16_w200_'];%['*_ncenter' num2str(p) '_*'];%['*_w' num2str(p) '_*'];
                if ~strcmp(dynamic,'woDynamic')
                    Featurename = [Featurename parafix];
                end
                Featurename = ['Result_' dataset_name '_' algoname{m} '_' Featurename partition_name '_' kname{k} '_'];
                fname = dir([result_folder '/' Featurename '*']);
                if size(fname,1) >1 
                    fname = fname(2);
                end
                if isempty(fname)
                    continue;
                end
                display(fname.name);
                fname = [result_folder '/' fname.name];
                load(fname);
%                 fname(strfind(fname,' ')) =[];
%                 temp = strfind(fname,'.mat');
%                 load(fname(1:temp+3));
                M(ixp,m, k ,:,:) = Method;
                prob = zeros(size(Method,1), 10000);
                stat_std = zeros(size(Method,1), 10000);
                
                Method = [Method Method]; 
                for i =1: size(Method,1)
                    r = Method{i,2}.Ranking(1,:); % use cam2 as reference
%                     r = Method{i,2}.Ranking(1:10,:);
                    [a, b] = hist(r' ,1: size(r,2));
                    a = a./repmat(ones(1,size(a,2))*size(r,2), size(a,1),1);
                    a = a';
%                     if size(a,1) == size(Partition(1).ix_test_gallery,1)
%                         a = a';
%                     end
                    prob(i, 1:size(a,1)) =mean(a,2); 
                        
                end
                prob_cum = cumsum(prob,2);
                prob = mean(prob_cum,1);
                stat_std = std(prob_cum,1);
                temp = [color{m} linesty{k}];
                
                if strcmp(algoname{m},'oLFDA')
                    result_name{cnt} = 'LFDA';
                elseif strcmp(algoname{m},'svmml')
                    result_name{cnt} = 'svmml';
                else
                    switch kname{k}
                        case {'linear'}
                            result_name{cnt} = [algoname{m} '-' 'L'];
                            if strcmp(algoname{m},'LFDA')
                                result_name{cnt} = ['k' algoname{m} '-' 'L'];
                            else
                                result_name{cnt} = [algoname{m} '-' 'L'];
                            end
                        case {'chi2'}
                            if strcmp(algoname{m},'LFDA')
                                result_name{cnt} = ['k' algoname{m} '-' '\chi^2'];
                            else
                                result_name{cnt} = [algoname{m} '-' '\chi^2'];
                            end
                        case {'chi2-rbf'}
                            if strcmp(algoname{m},'LFDA')
                                result_name{cnt} = ['k' algoname{m} '-' 'R{\chi^2}'];
                            else
                                result_name{cnt} = [algoname{m} '-' 'R{\chi^2}'];
                            end
                    end
                end
                if length(num_patch)>1
                    result_name{cnt} = [result_name{cnt} '-' num2str(np)];
                end
                plot(prob(1:25)*100,temp,'linewidth',1.2,'markersize',10); hold on;
%                 boxplot(prob_cum(:,1:25)*100,'plotstyle','compact','color',temp(1),'symbol','');hold on;
                cmc(cnt,:)=prob(1:50);
                prob= ([ prob([1 5 10 20])  CalculatePUR(prob(1:size(Method{1,2}.Ranking,2)),num_ref)] )*100;                
                stat_std = stat_std([1 5 10 20]);
                rrr(cnt,:)=prob;
                rrr_std(cnt,:) = stat_std;
                
                display([dataset_name '_HistMoment' num2str(np) 'Patch' algoname{m} ' Rank 1 5 10 20 accuracy ===>']);
                display(num2str(prob,'%.1f  '));
                cnt = cnt +1;
            end
        end
    end
 
    legend(result_name);
    grid on;
    xlabel('Rank Score','Fontsize', 18, 'fontweight', 'bold')
    ylabel('Matching Rate (%)','Fontsize', 18, 'fontweight', 'bold')
    title(['CMC curve on ' dataset_name ' with ' num2str(np) ' patch'],'Fontsize', 18, 'fontweight', 'bold');
%     rrrr =[rrrr; rrr([1:3 10 4:9], :)]
    rrrr =[rrrr; rrr];
    rrrr_std = [rrrr_std; rrr_std];
end
% M=squeeze(M);
% matrix2latex_tab([rrrr]','%.1f  ');
end
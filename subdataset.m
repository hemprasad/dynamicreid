clc
clear 
%%
load('Dataset/PRID_Images_Tracklets_l15.mat');

testID = bk38;
ix_test = ismember(gID, testID);
trainID = setdiff(gID,testID);
% use the same number of identities in training
trainID = trainID(randsample(numel(trainID),round(numel(unique(gID))/2)));
ix_train = ismember(gID,trainID);

gID = [gID(ix_train), gID(ix_test)];
camID = [camID(ix_train), camID(ix_test)];
I = [I(ix_train), I(ix_test)];
denseTrj = [denseTrj(ix_train), denseTrj(ix_test)];

save('PRID_Images_Tracklets_l15_allbk_38.mat','I','gID','camID','denseTrj');
%%
Partition.idx_train = 1:sum(ix_train);
Partition.idx_test = sum(ix_train)+1:numel(gID);
Partition.ix_train_gallery = logical([zeros(1,numel(unique(trainID))),...
    ones(1,numel(unique(trainID))); ones(1,numel(unique(trainID))),...
    zeros(1,numel(unique(trainID)))]);
Partition.ix_test_gallery = logical([zeros(1,numel(unique(testID))),...
    ones(1,numel(unique(testID))); ones(1,numel(unique(testID))),...
    zeros(1,numel(unique(testID)))]);
save('PRID_Partiton_Random_allbk_38.mat','Partition');

%% subsample feature
load('Dataset/iLIDSVID_Images_Tracklets_l15.mat','gID','camID');
oriGID = gID;
oricamID = camID;
load('Dataset/iLIDSVID_Images_Tracklets_l15_allbk_97.mat','gID','camID');
load('Feature/iLIDSVID_HOG3D.mat');

Feat = feat_hog3d;

bk_feat = zeros(numel(gID),size(Feat,2),'single');
for i = 1:numel(gID)
    tmpID = gID(i);
    tmpCID = camID(i);
    idxori = find(oriGID==tmpID & oricamID==tmpCID);
    bk_feat(i,:) = Feat(idxori,:);
end

% FeatureAppearence = bk_feat;
feat_hog3d = bk_feat;
save('Feature/iLIDSVID_HOG3D_allbk_97.mat','feat_hog3d');

function dynMat = dynamicVisual(data,label,figsiz,subcolor)
% visulization of dynamic words
% input: figsiz--[r,c];
patchSiz = 4; % print 4X4 pixel patch
dynMat = zeros([figsiz/patchSiz,3]);
dynHist = cell(figsiz/patchSiz);
% colorS = colormap('HSV');
% subcolor = linspace(1,size(colorS,1),maxl);
% subcolor = colorS(round(subcolor),:);
% dynMat = zeros(figsiz);
labelCell = cell(figsiz);
for i = 1:size(data,2)
    tmpdata = data(:,i);
    tmpX = round(tmpdata(end-1,:));
    tmpY = round(tmpdata(end,:));
    tmpX(tmpX<1) = 1;
    tmpY(tmpY<1) = 1;
    tmpX(tmpX>figsiz(2)) = figsiz(2);
    tmpY(tmpY>figsiz(1)) = figsiz(1);
    labelCell{tmpY,tmpX} = [labelCell{tmpY,tmpX} label(i)];
end

for r = 1:figsiz(1)/patchSiz
    for c = 1:figsiz(2)/patchSiz
        subCell = labelCell((r-1)*patchSiz+1:r*patchSiz,...
                            (c-1)*patchSiz+1:c*patchSiz);
        tmpCount = cell2mat(subCell(:)');
        if isempty(tmpCount)
            dynHist{r,c} = zeros(1,size(subcolor,1));
            continue;
        end
        dynMat(r,c,:) = subcolor(mode(tmpCount),:);
        dynHist{r,c} = hist(tmpCount,1:size(subcolor,1));
    end
end
                        

% labelMat = cellfun(@mode,labelCell); % most frequent one
% 
% ul = unique(labelMat(~isnan(labelMat)));
% for i = 1:numel(ul)
%     l = ul(i);
%     tmpInd = find(labelMat==l);
%     for in = 1:numel(tmpInd)
%         [tmpR, tmpC] = ind2sub(figsiz,tmpInd(in));
%         dynMat(tmpR,tmpC,:) = subcolor(l,:);
%     end
% end


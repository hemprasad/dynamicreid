function DF = pairwiseDynamicsDistance(data)
% data: dXN
[cluster.XHHp,cluster.XHHpFrob,m] = calHHp(data);
w = [ones(1, 2 * m) 2 * ones(1, length(2*m+1 : size(cluster.XHHp, 1)))];
DF = relativeDynamicsDistance_HHp(cluster.XHHp, cluster.XHHpFrob,...
    1:size(data,2),w);